SHELL=/bin/sh

# deal with operating system choice
OSFLAG:=
ifeq ($(OS),Windows_NT)
	OSFLAG += -D WIN32
	ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
		OSFLAG += -D AMD64
	endif
	ifeq ($(PROCESSOR_ARCHITECTURE),x86)
		OSFLAG += -D IA32
	endif
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		OSFLAG:=LINUX
	endif
	ifeq ($(UNAME_S),Darwin)
		OSFLAG:=OSX
	endif
endif

# In case of debugging, it's very important to at least leave out two threads at least for browsing/developing.
SPARE_NPROCS:=1
NPROCS:=
ifeq ($(OSFLAG),LINUX)
	NPROCS:=$(shell grep -c ^processor /proc/cpuinfo)
	ifeq ($(shell expr $(NPROCS) \> ${SPARE_NPROCS}),1)
		NPROCS:=$(shell echo ${NPROCS}-${SPARE_NPROCS} | bc)
	endif
	ifeq ($(shell expr $(NPROCS) \<\= ${SPARE_NPROCS}),1)
		NPROCS:=1
	endif
else
	NPROCS:=$(shell system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
endif

# Test the whole application using cargo
test:
	@cargo test -- --test-threads=1

# Run the application in a debugging sense
run:
	@echo "Running cargo with parallel job with $(NPROCS) processes."
	@cargo run -j $(NPROCS)

clean:
	@cargo clean

release:
	@cargo run --release

watch:
	@cargo watch -x 'run'

doc:
	@cargo doc --open --document-private-items

grpcui:
	@grpcui -plaintext 'localhost:50051'