use crate::app::App;
use crate::common::crypto::Secret;
use crate::common::proto::util::*;
use crate::common::proto::writer_heaven_server::WriterHeaven;
use crate::usecase::account::{
    GetAccountParam, InsertAccountParam as UInsertAccountParam,
    LoginNativeAccountParam as ULoginNativeAccountParam,
};
use crate::usecase::book::{UpdateBookParam, UpdateSectionParam};
use crate::usecase::common::{Header, Pagination};
use crate::{common, usecase, ServerConfig};
use tonic::{Request, Response, Status};

pub struct WriterHeavenHandler {
    pub app: App,
}

impl WriterHeavenHandler {
    pub async fn new(config: &ServerConfig) -> Self {
        Self {
            app: App::new(config).await,
        }
    }

    #[cfg(test)]
    pub fn mock(config: &ServerConfig) -> Self {
        Self {
            app: App::mock(config),
        }
    }
}

#[tonic::async_trait]
impl WriterHeaven for WriterHeavenHandler {
    #[tracing::instrument(skip_all)]
    async fn say_hello(
        &self,
        request: Request<common::proto::HelloRequest>,
    ) -> Result<Response<common::proto::HelloReply>, Status> {
        tracing::debug!("received request");

        let reply = common::proto::HelloReply {
            message: format!("Hello {}!", request.into_inner().name),
        };

        tracing::debug!("sending response");
        Ok(Response::new(reply))
    }

    #[tracing::instrument(skip_all)]
    async fn get_account(
        &self,
        request: tonic::Request<common::proto::GetAccountParam>,
    ) -> Result<Response<common::proto::GetAccountReply>, Status> {
        let r_param = request.into_inner();

        // Get JWT Claim
        let claim = self
            .app
            .extract_jwt_claim(r_param.header)?
            .ok_or_else(|| Status::not_found("Need header in GRPC Request."))?;

        let param = GetAccountParam {
            id: r_param.id,
            header: Header { claim: Some(claim) },
        };

        // Begin the business logic
        let result = self.app.account.get_account(&param).await?;

        // Final mapping to protobuf type
        match result {
            Some(v) => Ok(Response::new(common::proto::GetAccountReply {
                id: v.id,
                email: v.email.to_owned(),
                display_name: v.display_name,
                permission: v.permission.into_inner().into(),
            })),
            None => Err(Status::not_found(format!(
                "Account with id: \"{}\" is not found.",
                param.id
            ))),
        }
    }

    #[tracing::instrument(skip_all)]
    async fn insert_account(
        &self,
        request: Request<common::proto::InsertAccountParam>,
    ) -> Result<Response<common::proto::InsertAccountReply>, Status> {
        let r_param = request.into_inner();

        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = UInsertAccountParam {
            email: r_param.email,
            display_name: r_param.display_name,
            password: r_param.password,
            header: Header { claim },
        };

        let _ = self.app.account.insert_account(&param).await?;

        Ok(Response::new(common::proto::InsertAccountReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn login_native_account(
        &self,
        request: tonic::Request<common::proto::LoginNativeAccountParam>,
    ) -> Result<tonic::Response<common::proto::LoginNativeAccountReply>, tonic::Status> {
        let r_param = request.into_inner();
        let param = ULoginNativeAccountParam {
            email: r_param.email,
            password: Secret(r_param.password),
            server_secret: Secret(self.app.config.secret_key.clone()),
        };

        let jwt = self.app.account.login_native_account(&param).await?;

        Ok(Response::new(common::proto::LoginNativeAccountReply {
            jwt,
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn insert_book(
        &self,
        request: tonic::Request<common::proto::InsertBookParam>,
    ) -> Result<tonic::Response<common::proto::InsertBookReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = crate::usecase::book::InsertBookParam {
            header: Header { claim },
            title: r_param.title,
            description: r_param.description,
        };

        let result = self.app.book.insert_book(&param).await?;

        Ok(Response::new(common::proto::InsertBookReply { id: result }))
    }

    #[tracing::instrument(skip_all)]
    async fn get_book(
        &self,
        request: tonic::Request<common::proto::GetBookParam>,
    ) -> Result<tonic::Response<common::proto::GetBookReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let size = r_param
            .pagination
            .iter()
            .map(|f| f.size)
            .next()
            .unwrap_or_default();
        let number = r_param
            .pagination
            .iter()
            .map(|f| f.number)
            .next()
            .unwrap_or_default();

        let param = crate::usecase::book::GetBookListParam {
            header: Header { claim },
            pagination: Pagination { size, number },
            id: convert_i64_option(r_param.id),
            owner_id: convert_i64_option(r_param.owner_id),
            title: convert_string_option(r_param.title),
        };

        let result = self.app.book.get_book_list(&param).await?;

        Ok(Response::new(common::proto::GetBookReply {
            pagination: Some(common::proto::PaginationReply {
                total: result.total,
            }),
            book_list: result
                .data
                .into_iter()
                .map(|f| common::proto::get_book_reply::Book {
                    id: f.id,
                    title: f.title,
                    description: f.description,
                    owner_name: f.owner_name,
                    owner_id: f.owner_id,
                })
                .collect(),
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn insert_section(
        &self,
        request: tonic::Request<common::proto::InsertSectionParam>,
    ) -> Result<tonic::Response<common::proto::InsertSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = crate::usecase::book::InsertSectionToBookParam {
            header: Header { claim },
            book_id: r_param.book_id,
            title: r_param.title,
            content: r_param.content,
        };
        let result = self.app.book.insert_section_to_book(&param).await?;

        Ok(Response::new(common::proto::InsertSectionReply {
            id: result.id,
            owner_book_id: result.owner_book_id,
            title: result.title,
            content: result.content,
            created_at: result.created_at.timestamp(),
            updated_at: result.updated_at.timestamp(),
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn get_section(
        &self,
        request: tonic::Request<common::proto::GetSectionParam>,
    ) -> Result<tonic::Response<common::proto::GetSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let size = r_param
            .pagination
            .iter()
            .map(|f| f.size)
            .next()
            .unwrap_or_default();
        let number = r_param
            .pagination
            .iter()
            .map(|f| f.number)
            .next()
            .unwrap_or_default();

        let param = crate::usecase::book::GetSectionListParam {
            header: Header { claim },
            pagination: Pagination { size, number },
            book_id: r_param.book_id,
            section_id: convert_i64_option(r_param.section_id),
            title: convert_string_option(r_param.title),
        };

        let result = self.app.book.get_section_list(&param).await?;

        Ok(Response::new(common::proto::GetSectionReply {
            pagination: Some(common::proto::PaginationReply {
                total: result.total,
            }),
            section_list: result
                .data
                .into_iter()
                .map(|f| common::proto::get_section_reply::Section {
                    id: f.id,
                    title: f.title,
                    owner_book_id: f.owner_book_id,
                    content: f.content,
                    is_draft: f.is_draft,
                    created_at: f.created_at.timestamp(),
                    updated_at: f.updated_at.timestamp(),
                    like_count: f.like_count,
                })
                .collect(),
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn update_book(
        &self,
        request: tonic::Request<common::proto::UpdateBookParam>,
    ) -> Result<tonic::Response<common::proto::UpdateBookReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = UpdateBookParam {
            header: Header { claim },
            book_id: r_param.id,
            title: convert_string_option(r_param.title),
            description: convert_string_option(r_param.description),
        };

        let _ = self.app.book.update_book(&param).await?;

        Ok(Response::new(common::proto::UpdateBookReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn update_section(
        &self,
        request: tonic::Request<common::proto::UpdateSectionParam>,
    ) -> Result<tonic::Response<common::proto::UpdateSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = UpdateSectionParam {
            header: Header { claim },
            book_id: r_param.book_id,
            title: convert_string_option(r_param.title),
            content: convert_string_option(r_param.content),
            section_id: r_param.section_id,
            is_draft: r_param.is_draft,
        };

        let _ = self.app.book.update_section(&param).await?;

        Ok(Response::new(common::proto::UpdateSectionReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn delete_section(
        &self,
        request: tonic::Request<common::proto::DeleteSectionParam>,
    ) -> Result<tonic::Response<common::proto::DeleteSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::book::DeleteSectionParam {
            header: Header { claim },
            section_id: r_param.section_id,
        };

        let _ = self.app.book.delete_section(&param).await?;

        Ok(Response::new(common::proto::DeleteSectionReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn get_note(
        &self,
        request: tonic::Request<common::proto::GetNoteParam>,
    ) -> Result<tonic::Response<common::proto::GetNoteReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let size = r_param
            .pagination
            .iter()
            .map(|f| f.size)
            .next()
            .unwrap_or_default();
        let number = r_param
            .pagination
            .iter()
            .map(|f| f.number)
            .next()
            .unwrap_or_default();

        let param = usecase::note::GetNoteListParam {
            header: Header { claim },
            pagination: Pagination { size, number },
            id: convert_i64_option(r_param.id),
            keyword: convert_string_option(r_param.keyword),
            tag_list: r_param.tag_list,
        };
        let result = self.app.note.get_note_list(&param).await?;

        Ok(Response::new(common::proto::GetNoteReply {
            pagination: Some(common::proto::PaginationReply {
                total: result.total,
            }),
            note_list: result
                .data
                .into_iter()
                .map(|f| common::proto::get_note_reply::Note {
                    id: f.id,
                    content: f.content,
                    tag_list: f.tag_list,
                    created_at: f.created_at.timestamp(),
                    updated_at: f.updated_at.timestamp(),
                })
                .collect(),
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn insert_note(
        &self,
        request: tonic::Request<common::proto::InsertNoteParam>,
    ) -> Result<tonic::Response<common::proto::InsertNoteReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::note::InsertNoteParam {
            header: Header { claim },
            content: r_param.content,
            tag_list: r_param.tag_list,
        };
        let result = self.app.note.insert_note(&param).await?;

        Ok(Response::new(common::proto::InsertNoteReply {
            id: result.id,
            content: result.content,
            tag_list: result.tag_list,
            created_at: result.created_at.timestamp(),
            updated_at: result.updated_at.timestamp(),
        }))
    }

    #[tracing::instrument(skip_all)]
    async fn update_note(
        &self,
        request: tonic::Request<common::proto::UpdateNoteParam>,
    ) -> Result<tonic::Response<common::proto::UpdateNoteReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::note::UpdateNoteParam {
            header: Header { claim },
            id: r_param.id,
            content: r_param.content,
            tag_list: r_param.tag_list,
        };
        let _ = self.app.note.update_note(&param).await?;

        Ok(Response::new(common::proto::UpdateNoteReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn delete_note(
        &self,
        request: tonic::Request<common::proto::DeleteNoteParam>,
    ) -> Result<tonic::Response<common::proto::DeleteNoteReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::note::DeleteNoteParam {
            header: Header { claim },
            id: r_param.id,
        };
        let _ = self.app.note.delete_note(&param).await?;

        Ok(Response::new(common::proto::DeleteNoteReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn like_section(
        &self,
        request: tonic::Request<common::proto::LikeSectionParam>,
    ) -> Result<tonic::Response<common::proto::LikeSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::book::LikeSectionParam {
            header: Header { claim },
            section_id: r_param.section_id,
            is_like: r_param.is_like,
        };
        let _ = self.app.book.like_section(&param).await?;

        Ok(Response::new(common::proto::LikeSectionReply {}))
    }

    #[tracing::instrument(skip_all)]
    async fn get_like_section(
        &self,
        request: tonic::Request<common::proto::GetLikeSectionParam>,
    ) -> Result<tonic::Response<common::proto::GetLikeSectionReply>, tonic::Status> {
        let r_param = request.into_inner();
        let claim = self.app.extract_jwt_claim(r_param.header)?;

        let param = usecase::book::GetLikeSectionParam {
            header: Header { claim },
            section_id: r_param.section_id,
        };
        let result = self.app.book.get_like_section(&param).await?;

        Ok(Response::new(common::proto::GetLikeSectionReply {
            is_like: result.is_like,
        }))
    }
}

#[cfg(test)]
mod test {
    use crate::common::proto::writer_heaven_server::WriterHeaven;
    use crate::common::proto::{HelloReply, HelloRequest};
    use crate::handler::WriterHeavenHandler;
    use crate::ServerConfig;
    use pretty_assertions::assert_eq;
    use tonic::Request;

    #[tokio::test(flavor = "multi_thread")]
    async fn test_writer_heaven() {
        let config = ServerConfig::mock();
        let handler = WriterHeavenHandler::mock(&config);

        struct Param {
            request: HelloRequest,
        }
        struct TestCase {
            param: Param,
            want: HelloReply,
            want_err: bool,
        }
        let test_cases = [
            TestCase {
                param: Param {
                    request: HelloRequest {
                        name: "Izdihar".to_string(),
                    },
                },
                want: HelloReply {
                    message: "Hello Izdihar!".to_string(),
                },
                want_err: false,
            },
            TestCase {
                param: Param {
                    request: HelloRequest {
                        name: "Dear User".to_string(),
                    },
                },
                want: HelloReply {
                    message: "Hello Dear User!".to_string(),
                },
                want_err: false,
            },
        ];

        for t in test_cases {
            let response = handler.say_hello(Request::new(t.param.request)).await;

            if t.want_err {
                assert!(response.is_err());
            } else {
                let hello_reply = response.unwrap().into_inner();
                assert_eq!(t.want, hello_reply);
            }
        }
    }

    #[test]
    fn test_match_integer() {
        let pattern_int = |x: i64| match x {
            0 => None,
            v => Some(v),
        };
        let patter_string = |x: String| match x.as_str() {
            "" => None,
            _ => Some(x),
        };

        assert_eq!(pattern_int(0), None);
        assert_eq!(pattern_int(1), Some(1));
        assert_eq!(pattern_int(-1), Some(-1));
        assert_eq!(patter_string("".to_string()), None);
        assert_eq!(patter_string("Halo".to_string()), Some("Halo".to_string()));
    }
}
