mod implement;
#[cfg(test)]
mod testing;

use tonic::async_trait;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
    usecase::common::{Paginated, Pagination},
};

#[async_trait]
#[cfg_attr(test, automock)]
pub trait LikeAccountRepository {
    async fn get_like_account(
        &self,
        resource: &Tx,
        param: &GetLikeAccountParam,
    ) -> AppResult<Paginated<GetLikeAccountRepoEntity>>;

    async fn insert_like_account(
        &self,
        resource: &Tx,
        param: &InsertLikeAccountParam,
    ) -> AppResult<InsertLikeAccountRepoEntity>;

    async fn delete_like_account(
        &self,
        resource: &Tx,
        param: &DeleteLikeAccountParam,
    ) -> AppResult<()>;
}

#[derive(Debug)]
pub struct LikeAccountRepositoryImpl;

#[derive(Debug, PartialEq)]
pub struct GetLikeAccountParam {
    pub pagination: Pagination,
    pub account_id: Option<BigSerial>,
    pub section_id: Option<BigSerial>,
}

#[derive(Debug, PartialEq)]
pub struct GetLikeAccountRepoEntity {
    pub account_id: BigSerial,
    pub section_id: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct InsertLikeAccountParam {
    pub account_id: BigSerial,
    pub section_id: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct InsertLikeAccountRepoEntity {
    pub account_id: BigSerial,
    pub section_id: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct DeleteLikeAccountParam {
    pub account_id: BigSerial,
    pub section_id: BigSerial,
}
