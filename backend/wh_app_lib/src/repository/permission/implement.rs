use super::{
    GetPermissionResponse, InsertPermissionResponse, PermissionRepository, PermissionRepositoryImpl,
};
use crate::common::permission::try_deserialize_permission;
use migration::entity::{permission, prelude::Permission};
use sea_orm::{prelude::*, Set, TransactionTrait};
use wh_app_migration as migration;

#[tonic::async_trait]
impl PermissionRepository for PermissionRepositoryImpl {
    #[tracing::instrument(skip_all)]
    async fn get_permission(
        &self,
        resource: &crate::resource::Tx,
        param: &super::GetPermissionRequest,
    ) -> crate::common::error::AppResult<super::GetPermissionResponse> {
        let found: Option<permission::Model> = Permission::find()
            .filter(permission::Column::AccountId.eq(param.account_id))
            .one(resource.tx())
            .await?;

        let found = found
            .map(|f| f.permission_flag)
            .map(|f| try_deserialize_permission(f).unwrap_or_default());

        Ok(GetPermissionResponse {
            permission_container: found,
        })
    }

    #[tracing::instrument(skip_all)]
    async fn insert_permission(
        &self,
        resource: &crate::resource::Tx,
        param: &super::InsertPermissionRequest,
    ) -> crate::common::error::AppResult<InsertPermissionResponse> {
        let txn = resource.tx().begin().await?;

        let _ = permission::ActiveModel {
            account_id: Set(param.account_id),
            permission_flag: Set(param.permission_container.as_raw_slice().to_vec()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;

        Ok(InsertPermissionResponse {})
    }

    #[tracing::instrument(skip_all)]
    async fn upsert_permission(
        &self,
        resource: &crate::resource::Tx,
        param: &super::UpsertPermissionRequest,
    ) -> crate::common::error::AppResult<super::UpsertPermissionResponse> {
        let txn = resource.tx().begin().await?;
        let found: Option<permission::Model> = Permission::find()
            .filter(permission::Column::AccountId.eq(param.account_id))
            .one(&txn)
            .await?;

        let _ = match found {
            Some(permission) => {
                let mut active: permission::ActiveModel = permission.into();
                active.permission_flag = Set(param.permission_container.as_raw_slice().to_vec());
                active.update(&txn).await?;
            }
            None => {
                permission::ActiveModel {
                    account_id: Set(param.account_id),
                    permission_flag: Set(param.permission_container.as_raw_slice().to_vec()),
                    ..Default::default()
                }
                .insert(&txn)
                .await?;
            }
        };

        txn.commit().await?;
        Ok(super::UpsertPermissionResponse {})
    }
}
