mod implement;
#[cfg(test)]
mod testing;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
};
use tonic::async_trait;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait TagRepository {
    async fn insert_tag(&self, resource: &Tx, param: &InsertTagParam) -> AppResult<TagModel>;

    async fn get_tag_list(
        &self,
        resource: &Tx,
        param: &GetTagListParam,
    ) -> AppResult<Vec<TagModel>>;

    async fn delete_tag(&self, resource: &Tx, param: &DeleteTagParam) -> AppResult<()>;
}

#[derive(Debug, Default)]
pub struct TagRepositoryImpl;

#[derive(Debug)]
pub struct InsertTagParam {
    pub owner_note_id: BigSerial,
    pub title: String,
}

#[derive(Debug)]
pub struct GetTagListParam {
    pub owner_note_id: Option<i64>,
    pub keyword: Option<String>,
}

#[derive(Debug)]
pub struct TagModel {
    pub owner_note_id: BigSerial,
    pub title: String,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct DeleteTagParam {
    pub owner_note_id: BigSerial,
    pub title: String,
}
