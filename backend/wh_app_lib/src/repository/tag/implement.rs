use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, EntityTrait, QueryFilter, Set, TransactionTrait,
};
use tonic::async_trait;
use wh_app_migration::entity::{prelude::Tag, tag};

use crate::common::error::AppError;

use super::{TagRepository, TagRepositoryImpl};

#[async_trait]
impl TagRepository for TagRepositoryImpl {
    #[tracing::instrument(skip_all)]
    async fn insert_tag(
        &self,
        resource: &crate::resource::Tx,
        param: &super::InsertTagParam,
    ) -> crate::common::error::AppResult<super::TagModel> {
        let txn = resource.tx().begin().await?;

        let new: tag::Model = tag::ActiveModel {
            owner_note_id: Set(param.owner_note_id),
            title: Set(param.title.clone()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;

        Ok(super::TagModel {
            owner_note_id: new.owner_note_id,
            title: new.title,
            created_at: new.created_at,
            updated_at: new.updated_at,
        })
    }

    #[tracing::instrument(skip_all)]
    async fn get_tag_list(
        &self,
        resource: &crate::resource::Tx,
        param: &super::GetTagListParam,
    ) -> crate::common::error::AppResult<Vec<super::TagModel>> {
        let mut condition = Condition::all();

        if let Some(owner_note_id) = param.owner_note_id {
            condition = condition.add(tag::Column::OwnerNoteId.eq(owner_note_id))
        }

        if let Some(keyword) = &param.keyword {
            condition = condition.add(tag::Column::Title.eq(keyword.as_str()))
        }

        let model: Vec<tag::Model> = Tag::find().filter(condition).all(resource.tx()).await?;

        Ok(model
            .into_iter()
            .map(|m| super::TagModel {
                owner_note_id: m.owner_note_id,
                title: m.title,
                created_at: m.created_at,
                updated_at: m.updated_at,
            })
            .collect())
    }

    #[tracing::instrument(skip_all)]
    async fn delete_tag(
        &self,
        resource: &crate::resource::Tx,
        param: &super::DeleteTagParam,
    ) -> crate::common::error::AppResult<()> {
        let tx = resource.tx().begin().await?;
        let model: Option<tag::Model> = tag::Entity::find()
            .filter(tag::Column::OwnerNoteId.eq(param.owner_note_id))
            .filter(tag::Column::Title.eq(param.title.clone()))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let active: tag::ActiveModel = m.into();
                let _ = active.delete(&tx).await?;
                tx.commit().await?;

                Ok(())
            }
            None => Err(AppError::Generic(
                "Tag with that id is not found and isn't deleted".to_string(),
            )),
        }
    }
}
