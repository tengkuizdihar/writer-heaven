mod implement;
#[cfg(test)]
mod testing;

use tonic::async_trait;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
    usecase::common::{Paginated, Pagination},
};

#[async_trait]
#[cfg_attr(test, automock)]
pub trait SectionLikeRepository {
    async fn get_section_like(
        &self,
        resource: &Tx,
        param: &GetSectionLikeParam,
    ) -> AppResult<Paginated<GetSectionLikeRepoEntity>>;

    async fn insert_section_like(
        &self,
        resource: &Tx,
        param: &InsertSectionLikeParam,
    ) -> AppResult<InsertSectionLikeRepoEntity>;

    async fn update_section_like(
        &self,
        resource: &Tx,
        param: &UpdateSectionLikeParam,
    ) -> AppResult<()>;
}

#[derive(Debug)]
pub struct SectionLikeRepositoryImpl;

#[derive(Debug, PartialEq)]
pub struct GetSectionLikeParam {
    pub pagination: Pagination,
    pub section_id: Option<BigSerial>,
}

#[derive(Debug, PartialEq)]
pub struct GetSectionLikeRepoEntity {
    pub section_id: BigSerial,
    pub like_count: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct InsertSectionLikeParam {
    pub section_id: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct InsertSectionLikeRepoEntity {
    pub section_id: BigSerial,
    pub like_count: BigSerial,
}

#[derive(Debug, PartialEq)]
pub struct UpdateSectionLikeParam {
    pub section_id: BigSerial,
    pub like_count: BigSerial,
}
