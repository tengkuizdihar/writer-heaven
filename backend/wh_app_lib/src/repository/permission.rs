mod implement;
#[cfg(test)]
mod testing;

use crate::{
    common::{alias::BigSerial, error::AppResult, permission::PermissionContainer},
    resource::Tx,
};
use tonic::async_trait;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait PermissionRepository {
    async fn get_permission(
        &self,
        resource: &Tx,
        param: &GetPermissionRequest,
    ) -> AppResult<GetPermissionResponse>;

    async fn insert_permission(
        &self,
        resource: &Tx,
        param: &InsertPermissionRequest,
    ) -> AppResult<InsertPermissionResponse>;

    async fn upsert_permission(
        &self,
        resource: &Tx,
        param: &UpsertPermissionRequest,
    ) -> AppResult<UpsertPermissionResponse>;
}

#[derive(Debug)]
pub struct PermissionRepositoryImpl;

#[derive(Debug)]
pub struct GetPermissionRequest {
    pub account_id: BigSerial,
}

#[derive(Debug)]
pub struct GetPermissionResponse {
    pub permission_container: Option<PermissionContainer>,
}

#[derive(Debug)]
pub struct InsertPermissionRequest {
    pub account_id: BigSerial,
    pub permission_container: PermissionContainer,
}

#[derive(Debug)]
pub struct InsertPermissionResponse {}

#[derive(Debug)]
pub struct UpsertPermissionRequest {
    pub account_id: BigSerial,
    pub permission_container: PermissionContainer,
}

#[derive(Debug)]
pub struct UpsertPermissionResponse {}
