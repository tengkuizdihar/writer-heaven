use std::collections::BTreeMap;

use crate::{
    repository::like_account::{
        DeleteLikeAccountParam, GetLikeAccountParam, GetLikeAccountRepoEntity,
        InsertLikeAccountParam, LikeAccountRepository, LikeAccountRepositoryImpl,
    },
    resource::{Connection, Tx},
    usecase::common::{Paginated, Pagination},
};
use pretty_assertions::assert_eq;
use sea_orm::{
    entity::prelude::*, DatabaseBackend, MockDatabase, MockExecResult, Statement, Transaction,
    Values,
};
use wh_app_migration::entity::like_account;

#[tokio::test]
async fn test_insert_like_account_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![like_account::Model {
            owner_section_id: 1,
            owner_account_id: 3,
        }]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "INSERT INTO \"like_account\" (\"owner_section_id\", \"owner_account_id\") VALUES ($1, $2) RETURNING \"owner_section_id\", \"owner_account_id\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(1)),
                        Value::BigInt(Some(3)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "RELEASE SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = LikeAccountRepositoryImpl {};
    let param = InsertLikeAccountParam {
        section_id: 1,
        account_id: 3,
    };

    // ACT
    let result = repo.insert_like_account(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!(
        super::InsertLikeAccountRepoEntity {
            section_id: 1,
            account_id: 3
        },
        result
    );

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}

#[tokio::test]
async fn test_delete_like_account_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_exec_results(vec![MockExecResult {
            last_insert_id: 1,
            rows_affected: 1,
        }])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "DELETE FROM \"like_account\" WHERE \"like_account\".\"owner_section_id\" = $1 AND \"like_account\".\"owner_account_id\" = $2"
                .into(),
            values: Some(Values(vec![Value::BigInt(Some(1)), Value::BigInt(Some(3))])),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "RELEASE SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = LikeAccountRepositoryImpl {};
    let param = DeleteLikeAccountParam {
        section_id: 1,
        account_id: 3,
    };

    // ACT
    let result = repo.delete_like_account(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!((), result);

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}

#[tokio::test]
async fn test_get_like_account_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![
            (BTreeMap::from([("num_items", Value::BigInt(Some(1)))])), // Need to be num_items and bigint for paginated related query
        ]])
        .append_query_results(vec![vec![like_account::Model {
            owner_section_id: 123,
            owner_account_id: 321,
        }]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT COUNT(*) AS num_items FROM (SELECT \"like_account\".\"owner_section_id\", \"like_account\".\"owner_account_id\" FROM \"like_account\" WHERE \"like_account\".\"owner_account_id\" = $1 AND \"like_account\".\"owner_section_id\" = $2) AS \"sub_query\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(321)),
                        Value::BigInt(Some(123)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT \"like_account\".\"owner_section_id\", \"like_account\".\"owner_account_id\" FROM \"like_account\" WHERE \"like_account\".\"owner_account_id\" = $1 AND \"like_account\".\"owner_section_id\" = $2 LIMIT $3 OFFSET $4".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(321)),
                        Value::BigInt(Some(123)),
                        Value::BigUnsigned(Some(1)),
                        Value::BigUnsigned(Some(0)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = LikeAccountRepositoryImpl {};
    let param = GetLikeAccountParam {
        section_id: Some(123),
        pagination: Pagination { size: 1, number: 0 },
        account_id: Some(321),
    };

    // ACT
    let result = repo.get_like_account(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    let assert_value = Paginated {
        total: 1,
        data: vec![GetLikeAccountRepoEntity {
            section_id: 123,
            account_id: 321,
        }],
    };
    assert_eq!(assert_value, result);

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}
