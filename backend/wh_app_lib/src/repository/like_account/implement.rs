use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, EntityTrait, PaginatorTrait, QueryFilter,
    QuerySelect, Set, TransactionTrait,
};
use tonic::async_trait;
use wh_app_migration::entity::like_account;

use crate::{common::error::AppResult, resource::Tx, usecase::common::Paginated};

use super::{
    DeleteLikeAccountParam, GetLikeAccountParam, GetLikeAccountRepoEntity, InsertLikeAccountParam,
    InsertLikeAccountRepoEntity, LikeAccountRepository, LikeAccountRepositoryImpl,
};

#[async_trait]
impl LikeAccountRepository for LikeAccountRepositoryImpl {
    async fn get_like_account(
        &self,
        resource: &Tx,
        param: &GetLikeAccountParam,
    ) -> AppResult<Paginated<GetLikeAccountRepoEntity>> {
        let tx = resource.tx();

        let mut condition = Condition::all();
        if let Some(v) = param.account_id {
            condition = condition.add(like_account::Column::OwnerAccountId.eq(v))
        }
        if let Some(v) = param.section_id {
            condition = condition.add(like_account::Column::OwnerSectionId.eq(v))
        }

        let query = like_account::Entity::find().filter(condition);
        let total = PaginatorTrait::count(query.clone(), tx).await? as u64;
        let list = query
            .limit(param.pagination.size())
            .offset(param.pagination.size() * param.pagination.number)
            .all(tx)
            .await?;

        Ok(Paginated {
            total,
            data: list
                .into_iter()
                .map(|v| GetLikeAccountRepoEntity {
                    account_id: v.owner_account_id,
                    section_id: v.owner_section_id,
                })
                .collect(),
        })
    }

    async fn insert_like_account(
        &self,
        resource: &Tx,
        param: &InsertLikeAccountParam,
    ) -> AppResult<InsertLikeAccountRepoEntity> {
        let tx = resource.tx().begin().await?;

        let model = like_account::ActiveModel {
            owner_section_id: Set(param.section_id),
            owner_account_id: Set(param.account_id),
        }
        .insert(&tx)
        .await?;

        tx.commit().await?;
        Ok(InsertLikeAccountRepoEntity {
            account_id: model.owner_account_id,
            section_id: model.owner_section_id,
        })
    }

    async fn delete_like_account(
        &self,
        resource: &Tx,
        param: &DeleteLikeAccountParam,
    ) -> AppResult<()> {
        let tx = resource.tx().begin().await?;

        let _ = like_account::Entity::delete_by_id((param.section_id, param.account_id))
            .exec(&tx)
            .await?;

        tx.commit().await?;
        Ok(())
    }
}
