use chrono::Utc;
use sea_orm::{DatabaseBackend, MockDatabase, Statement, Transaction, Value, Values};
use wh_app_migration::entity::account;

use super::{AccountRepository, AccountRepositoryImpl, InsertAccountParam};
use crate::resource::{Connection, Tx};
use pretty_assertions::assert_eq;

#[tokio::test]
async fn test_insert_account_normal() {
    // ARRANGE - DATABASE
    let created_at = Utc::now();
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![account::Model {
            id: 1,
            email: "testemail@gmail.com".to_string(),
            display_name: "test name".to_string(),
            pass_hash: "testhash".to_string(),
            created_at: created_at.into(),
            updated_at: created_at.into(),
        }]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "INSERT INTO \"account\" (\"email\", \"display_name\", \"pass_hash\") VALUES ($1, $2, $3) RETURNING \"id\", \"email\", \"display_name\", \"pass_hash\", \"created_at\", \"updated_at\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::String(Some(Box::new("testemail@gmail.com".into()))),
                        Value::String(Some(Box::new("test name".into()))),
                        Value::String(Some(Box::new("testhash".into()))),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "RELEASE SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = AccountRepositoryImpl {};
    let param = InsertAccountParam {
        email: "testemail@gmail.com".to_string(),
        display_name: "test name".to_string(),
        pass_hash: "testhash".to_string(),
    };

    // ACT
    let result = repo.insert_account(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!(
        super::Account {
            id: 1,
            email: "testemail@gmail.com".to_string(),
            display_name: "test name".to_string(),
            pass_hash: "testhash".to_string(),
            created_at: created_at.into(),
            updated_at: created_at.into(),
        },
        result
    );

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}
