use super::{AccountRepository, AccountRepositoryImpl};
use crate::resource::Tx;
use migration::entity::{account, prelude::Account};
use sea_orm::{prelude::*, Condition, Set, TransactionTrait};
use wh_app_migration as migration;

#[tonic::async_trait]
impl AccountRepository for AccountRepositoryImpl {
    #[tracing::instrument(skip_all)]
    async fn insert_account(
        &self,
        resource: &Tx,
        param: &super::InsertAccountParam,
    ) -> crate::common::error::AppResult<super::Account> {
        let txn = resource.tx().begin().await?;

        let new: account::Model = account::ActiveModel {
            email: Set(param.email.clone()),
            display_name: Set(param.display_name.clone()),
            pass_hash: Set(param.pass_hash.clone()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;
        Ok(super::Account {
            id: new.id,
            email: new.email,
            display_name: new.display_name,
            pass_hash: new.pass_hash,
            created_at: new.created_at,
            updated_at: new.updated_at,
        })
    }

    #[tracing::instrument(skip_all)]
    async fn get_account_list(
        &self,
        resource: &Tx,
        param: &super::GetAccountListParam,
    ) -> crate::common::error::AppResult<Vec<super::Account>> {
        let mut condition = Condition::all();

        if let Some(id) = param.id {
            condition = condition.add(account::Column::Id.eq(id))
        }

        let models: Vec<account::Model> =
            Account::find().filter(condition).all(resource.tx()).await?;

        Ok(models
            .into_iter()
            .map(|m| super::Account {
                id: m.id,
                email: m.email,
                display_name: m.display_name,
                pass_hash: m.pass_hash,
                created_at: m.created_at,
                updated_at: m.updated_at,
            })
            .collect())
    }

    async fn get_account(
        &self,
        resource: &Tx,
        param: &super::GetAccountParam,
    ) -> crate::common::error::AppResult<Option<super::Account>> {
        let mut condition = Condition::all();

        if let Some(id) = param.id {
            condition = condition.add(account::Column::Id.eq(id))
        }

        if let Some(email) = &param.email {
            condition = condition.add(account::Column::Email.eq(email.as_str()))
        }

        let model: Option<account::Model> =
            Account::find().filter(condition).one(resource.tx()).await?;

        Ok(model.map(|m| super::Account {
            id: m.id,
            email: m.email,
            display_name: m.display_name,
            pass_hash: m.pass_hash,
            created_at: m.created_at,
            updated_at: m.updated_at,
        }))
    }

    async fn upsert_account(
        &self,
        resource: &Tx,
        param: &super::UpsertAccountParam,
    ) -> crate::common::error::AppResult<super::Account> {
        let mut condition = Condition::all();
        let txn = resource.tx().begin().await?;

        condition = condition.add(account::Column::Email.eq(param.email.as_str()));
        let model: Option<account::Model> = Account::find().filter(condition).one(&txn).await?;

        let upsert_account: account::Model = match model {
            Some(account) => {
                let mut existing: account::ActiveModel = account.into();
                existing.pass_hash = Set(param.pass_hash.clone());
                existing.update(&txn).await?
            }
            None => {
                account::ActiveModel {
                    email: Set(param.email.clone()),
                    display_name: Set(param.display_name.clone()),
                    pass_hash: Set(param.pass_hash.clone()),
                    ..Default::default()
                }
                .insert(&txn)
                .await?
            }
        };

        txn.commit().await?;
        Ok(super::Account {
            id: upsert_account.id,
            email: upsert_account.email,
            display_name: upsert_account.display_name,
            pass_hash: upsert_account.pass_hash,
            created_at: upsert_account.created_at,
            updated_at: upsert_account.updated_at,
        })
    }
}
