mod implement;
#[cfg(test)]
mod testing;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
    usecase::common::{Paginated, Pagination},
};
use tonic::async_trait;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait NoteRepository {
    async fn insert_note(&self, resource: &Tx, param: &InsertNoteParam) -> AppResult<NoteModel>;

    async fn get_note_list(
        &self,
        resource: &Tx,
        param: &GetNoteListParam,
    ) -> AppResult<Paginated<NoteWithTagListModel>>;

    async fn update_note(&self, resource: &Tx, param: &UpdateNoteParam) -> AppResult<NoteModel>;

    async fn delete_note(&self, resource: &Tx, param: &DeleteNoteParam) -> AppResult<()>;
}

#[derive(Debug, Default)]
pub struct NoteRepositoryImpl;

#[derive(Debug, PartialEq)]
pub struct InsertNoteParam {
    pub content: String,
    pub owner_account_id: BigSerial,
}

#[derive(Debug)]
pub struct GetNoteListParam {
    pub pagination: Pagination,
    pub owner_account_id: Option<BigSerial>,
    pub id: Option<BigSerial>,
    pub keyword: Option<String>,
}

#[derive(Debug)]
pub struct NoteModel {
    pub id: BigSerial,
    pub owner_account_id: BigSerial,
    pub content: String,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct NoteWithTagListModel {
    pub id: BigSerial,
    pub owner_account_id: BigSerial,
    pub content: String,
    pub tag_list: Vec<String>,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct UpdateNoteParam {
    pub id: BigSerial,
    pub owner_account_id: BigSerial,
    pub content: Option<String>,
}

#[derive(Debug)]
pub struct DeleteNoteParam {
    pub id: BigSerial,
}
