use std::collections::BTreeMap;

use crate::{
    repository::section_like::{
        GetSectionLikeParam, GetSectionLikeRepoEntity, InsertSectionLikeParam,
        SectionLikeRepository, SectionLikeRepositoryImpl, UpdateSectionLikeParam,
    },
    resource::{Connection, Tx},
    usecase::common::{Paginated, Pagination},
};
use pretty_assertions::assert_eq;
use sea_orm::{
    entity::prelude::*, DatabaseBackend, MockDatabase, MockExecResult, Statement, Transaction,
    Values,
};
use wh_app_migration::entity::section_like;

#[tokio::test]
async fn test_insert_section_like_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![section_like::Model {
            owner_section_id: 1,
            like_count: 0,
        }]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "INSERT INTO \"section_like\" (\"owner_section_id\", \"like_count\") VALUES ($1, $2) RETURNING \"owner_section_id\", \"like_count\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(1)),
                        Value::BigInt(Some(0)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "RELEASE SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = SectionLikeRepositoryImpl {};
    let param = InsertSectionLikeParam { section_id: 1 };

    // ACT
    let result = repo.insert_section_like(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!(
        super::InsertSectionLikeRepoEntity {
            section_id: 1,
            like_count: 0
        },
        result
    );

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}

#[tokio::test]
async fn test_update_section_like_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![
            vec![section_like::Model {
                owner_section_id: 1,
                like_count: 0,
            }],
            vec![section_like::Model {
                owner_section_id: 1,
                like_count: 1,
            }],
        ])
        .append_exec_results(vec![MockExecResult {
            last_insert_id: 1,
            rows_affected: 1,
        }])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT \"section_like\".\"owner_section_id\", \"section_like\".\"like_count\" FROM \"section_like\" WHERE \"section_like\".\"owner_section_id\" = $1 LIMIT $2"
                .into(),
            values: Some(Values(vec![Value::BigInt(Some(1)), Value::BigUnsigned(Some(1))])),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "UPDATE \"section_like\" SET \"like_count\" = $1 WHERE \"section_like\".\"owner_section_id\" = $2 RETURNING \"owner_section_id\", \"like_count\""
                .into(),
            values: Some(Values(vec![Value::BigInt(Some(1)), Value::BigInt(Some(1))])),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "RELEASE SAVEPOINT savepoint_1".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = SectionLikeRepositoryImpl {};
    let param = UpdateSectionLikeParam {
        section_id: 1,
        like_count: 1,
    };

    // ACT
    let result = repo.update_section_like(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!((), result);

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}

#[tokio::test]
async fn test_get_section_like_normal() {
    // ARRANGE - DATABASE
    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![
            (BTreeMap::from([("num_items", Value::BigInt(Some(1)))])), // Need to be num_items and bigint for paginated related query
        ]])
        .append_query_results(vec![vec![section_like::Model {
            owner_section_id: 123,
            like_count: 0,
        }]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT COUNT(*) AS num_items FROM (SELECT \"section_like\".\"owner_section_id\", \"section_like\".\"like_count\" FROM \"section_like\" WHERE \"section_like\".\"owner_section_id\" = $1) AS \"sub_query\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(123)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT \"section_like\".\"owner_section_id\", \"section_like\".\"like_count\" FROM \"section_like\" WHERE \"section_like\".\"owner_section_id\" = $1 LIMIT $2 OFFSET $3".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(123)),
                        Value::BigUnsigned(Some(1)),
                        Value::BigUnsigned(Some(0)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = SectionLikeRepositoryImpl {};
    let param = GetSectionLikeParam {
        section_id: Some(123),
        pagination: Pagination { size: 1, number: 0 },
    };

    // ACT
    let result = repo.get_section_like(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    let assert_value = Paginated {
        total: 1,
        data: vec![GetSectionLikeRepoEntity {
            section_id: 123,
            like_count: 0,
        }],
    };
    assert_eq!(assert_value, result);

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}
