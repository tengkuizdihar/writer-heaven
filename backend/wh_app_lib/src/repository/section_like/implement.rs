use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, EntityTrait, PaginatorTrait, QueryFilter,
    QuerySelect, Set, TransactionTrait,
};
use tonic::async_trait;
use wh_app_migration::entity::section_like;

use crate::{
    common::error::{AppError, AppResult},
    resource::Tx,
    usecase::common::Paginated,
};

use super::{
    GetSectionLikeParam, GetSectionLikeRepoEntity, InsertSectionLikeParam,
    InsertSectionLikeRepoEntity, SectionLikeRepository, SectionLikeRepositoryImpl,
    UpdateSectionLikeParam,
};

#[async_trait]
impl SectionLikeRepository for SectionLikeRepositoryImpl {
    #[tracing::instrument(skip_all)]
    async fn get_section_like(
        &self,
        resource: &Tx,
        param: &GetSectionLikeParam,
    ) -> AppResult<Paginated<GetSectionLikeRepoEntity>> {
        let tx = resource.tx();

        let mut condition = Condition::all();
        if let Some(v) = param.section_id {
            condition = condition.add(section_like::Column::OwnerSectionId.eq(v))
        }

        let query = section_like::Entity::find().filter(condition);
        let total = PaginatorTrait::count(query.clone(), tx).await? as u64;
        let list = query
            .limit(param.pagination.size())
            .offset(param.pagination.size() * param.pagination.number)
            .all(tx)
            .await?;

        Ok(Paginated {
            total,
            data: list
                .into_iter()
                .map(|v| GetSectionLikeRepoEntity {
                    section_id: v.owner_section_id,
                    like_count: v.like_count,
                })
                .collect(),
        })
    }

    #[tracing::instrument(skip_all)]
    async fn insert_section_like(
        &self,
        resource: &Tx,
        param: &InsertSectionLikeParam,
    ) -> AppResult<InsertSectionLikeRepoEntity> {
        let tx = resource.tx().begin().await?;

        let model = section_like::ActiveModel {
            owner_section_id: Set(param.section_id),
            like_count: Set(0),
        }
        .insert(&tx)
        .await?;

        tx.commit().await?;
        Ok(InsertSectionLikeRepoEntity {
            section_id: model.owner_section_id,
            like_count: model.like_count,
        })
    }

    #[tracing::instrument(skip_all)]
    async fn update_section_like(
        &self,
        resource: &Tx,
        param: &UpdateSectionLikeParam,
    ) -> AppResult<()> {
        let tx = resource.tx().begin().await?;
        let model: Option<section_like::Model> = section_like::Entity::find_by_id(param.section_id)
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let mut active_model: section_like::ActiveModel = m.into();
                active_model.like_count = Set(param.like_count);
                active_model.update(&tx).await?;

                tx.commit().await?;
                Ok(())
            }
            None => Err(AppError::Generic(
                "Update section fails because section_like might be broken".into(),
            )),
        }
    }
}
