use std::collections::HashMap;

use chrono::Utc;
use sea_orm::{prelude::*, Condition, QueryOrder, QuerySelect, Set, TransactionTrait};
use tonic::async_trait;
use wh_app_migration::{
    entity::{note, tag},
    Expr, Func,
};

use crate::{
    common::{alias::BigSerial, error::AppError},
    usecase::common::Paginated,
};

use super::{NoteRepository, NoteRepositoryImpl};

#[async_trait]
impl NoteRepository for NoteRepositoryImpl {
    #[tracing::instrument(skip_all)]
    async fn insert_note(
        &self,
        resource: &crate::resource::Tx,
        param: &super::InsertNoteParam,
    ) -> crate::common::error::AppResult<super::NoteModel> {
        let txn = resource.tx().begin().await?;

        let new: note::Model = note::ActiveModel {
            owner_account_id: Set(param.owner_account_id),
            content: Set(param.content.clone()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;

        Ok(super::NoteModel {
            id: new.id,
            owner_account_id: new.owner_account_id,
            content: new.content,
            created_at: new.created_at,
            updated_at: new.updated_at,
        })
    }

    #[tracing::instrument(skip_all)]
    async fn get_note_list(
        &self,
        resource: &crate::resource::Tx,
        param: &super::GetNoteListParam,
    ) -> crate::common::error::AppResult<
        crate::usecase::common::Paginated<super::NoteWithTagListModel>,
    > {
        let tx = resource.tx();
        let mut condition = Condition::all();

        if let Some(v) = param.id {
            condition = condition.add(note::Column::Id.eq(v));
        }

        if let Some(v) = param.owner_account_id {
            condition = condition.add(note::Column::OwnerAccountId.eq(v));
        }

        if let Some(v) = param.keyword.clone() {
            condition = condition.add(
                Expr::expr(Func::lower(Expr::col(note::Column::Content)))
                    .like(format!("%{}%", v.to_lowercase()).as_str()),
            );
        }

        // Internal struct used only for fetching purposes
        #[derive(sea_orm::FromQueryResult)]
        struct NoteResult {
            pub id: BigSerial,
            pub owner_account_id: BigSerial,
            pub content: String,
            pub created_at: chrono::DateTime<chrono::FixedOffset>,
            pub updated_at: chrono::DateTime<chrono::FixedOffset>,
        }

        let query = note::Entity::find()
            .select_only()
            .column(note::Column::Id)
            .column(note::Column::OwnerAccountId)
            .column(note::Column::Content)
            .column(note::Column::CreatedAt)
            .column(note::Column::UpdatedAt)
            .filter(condition)
            .order_by_desc(note::Column::Id);

        let total = PaginatorTrait::count(query.clone(), tx).await? as u64;
        let result_list: Vec<NoteResult> = query
            .into_model()
            .paginate(tx, param.pagination.size() as usize)
            .fetch_page(param.pagination.number as usize)
            .await?;

        let note_id_array: Vec<_> = result_list.iter().map(|f| f.id).collect();

        // get tags based on notes id
        let query_tags = tag::Entity::find()
            .select_only()
            .column(tag::Column::OwnerNoteId)
            .column(tag::Column::Title)
            .column(tag::Column::CreatedAt)
            .column(tag::Column::UpdatedAt)
            .filter(tag::Column::OwnerNoteId.is_in(note_id_array))
            .order_by_desc(tag::Column::OwnerNoteId);
        let result_tag_list: Vec<tag::Model> = query_tags.all(tx).await?;

        let mut tag_id_map: HashMap<BigSerial, Vec<String>> = HashMap::new();
        for tag in result_tag_list.into_iter() {
            let mut tags_by_note_id = tag_id_map
                .get(&tag.owner_note_id)
                .map(|f| f.to_owned())
                .unwrap_or_default();

            tags_by_note_id.push(tag.title);
            let _ = tag_id_map.insert(tag.owner_note_id, tags_by_note_id);
        }

        Ok(Paginated {
            total,
            data: result_list
                .into_iter()
                .map(|f| super::NoteWithTagListModel {
                    id: f.id,
                    owner_account_id: f.owner_account_id,
                    content: f.content,
                    created_at: f.created_at,
                    updated_at: f.updated_at,
                    tag_list: tag_id_map
                        .get(&f.id)
                        .map(|f| f.to_owned())
                        .unwrap_or_default(),
                })
                .collect(),
        })
    }

    #[tracing::instrument(skip_all)]
    async fn update_note(
        &self,
        resource: &crate::resource::Tx,
        param: &super::UpdateNoteParam,
    ) -> crate::common::error::AppResult<super::NoteModel> {
        let tx = resource.tx().begin().await?;
        let model: Option<note::Model> = note::Entity::find()
            .filter(note::Column::Id.eq(param.id))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let mut active: note::ActiveModel = m.into();
                if let Some(content) = param.content.clone() {
                    active.content = Set(content)
                }

                active.updated_at = Set(Utc::now().into());

                let model: note::Model = active.update(&tx).await?;

                tx.commit().await?;

                Ok(super::NoteModel {
                    id: model.id,
                    content: model.content,
                    owner_account_id: model.owner_account_id,
                    created_at: model.created_at,
                    updated_at: model.updated_at,
                })
            }
            None => Err(AppError::Generic(
                "Note with that id is not found and isn't updated".to_string(),
            )),
        }
    }

    #[tracing::instrument(skip_all)]
    async fn delete_note(
        &self,
        resource: &crate::resource::Tx,
        param: &super::DeleteNoteParam,
    ) -> crate::common::error::AppResult<()> {
        let tx = resource.tx().begin().await?;
        let model: Option<note::Model> = note::Entity::find()
            .filter(note::Column::Id.eq(param.id))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let active: note::ActiveModel = m.into();
                let _ = active.delete(&tx).await?;
                tx.commit().await?;

                Ok(())
            }
            None => Err(AppError::Generic(
                "Note with that id is not found and isn't deleted".to_string(),
            )),
        }
    }
}
