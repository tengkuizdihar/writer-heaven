mod implement;
#[cfg(test)]
mod testing;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
};
use tonic::async_trait;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait AccountRepository {
    async fn insert_account(&self, resource: &Tx, param: &InsertAccountParam)
        -> AppResult<Account>;

    async fn get_account_list(
        &self,
        resource: &Tx,
        param: &GetAccountListParam,
    ) -> AppResult<Vec<Account>>;

    async fn get_account(
        &self,
        resource: &Tx,
        param: &GetAccountParam,
    ) -> AppResult<Option<Account>>;

    async fn upsert_account(&self, resource: &Tx, param: &UpsertAccountParam)
        -> AppResult<Account>;
}

#[derive(Debug, Default)]
pub struct AccountRepositoryImpl;

#[derive(Debug, PartialEq)]
pub struct Account {
    pub id: BigSerial,
    pub email: String,
    pub display_name: String,
    pub pass_hash: String,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct InsertAccountParam {
    pub email: String,
    pub display_name: String,
    pub pass_hash: String,
}

#[derive(Debug)]
pub struct UpsertAccountParam {
    pub email: String,
    pub display_name: String,
    pub pass_hash: String,
}

#[derive(Debug)]
pub struct GetAccountListParam {
    pub id: Option<BigSerial>,
}

/// The consequences would be that there will be a `select * from account` without where condition
#[derive(Debug)]
pub struct GetAccountParam {
    pub id: Option<BigSerial>,
    pub email: Option<String>,
}
