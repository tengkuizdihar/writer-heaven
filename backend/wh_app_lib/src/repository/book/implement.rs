use super::{
    BookModel, BookRepository, BookRepositoryImpl, InsertedSectionModel, UpdatedSectionModel,
};
use crate::common::error::AppError;
use crate::{common::alias::BigSerial, usecase::common::Paginated};
use chrono::Utc;
use sea_orm::{prelude::*, Condition, QuerySelect, Set, TransactionTrait};
use sea_orm::{JoinType, QueryOrder};
use tonic::async_trait;
use tracing::instrument;
use wh_app_migration::entity::{section, section_like};
use wh_app_migration::{
    entity::{account, book},
    Expr, Func,
};

#[async_trait]
impl BookRepository for BookRepositoryImpl {
    #[instrument(skip_all)]
    async fn insert_book(
        &self,
        resource: &crate::resource::Tx,
        param: &super::InsertBookParam,
    ) -> crate::common::error::AppResult<super::BookModel> {
        let txn = resource.tx().begin().await?;

        let new: book::Model = book::ActiveModel {
            owner_account_id: Set(param.owner_account_id),
            title: Set(param.title.clone()),
            description: Set(param.description.clone()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;

        Ok(BookModel {
            id: new.id,
            title: new.title,
            description: new.description,
            owner_account_id: new.owner_account_id,
            created_at: new.created_at,
            updated_at: new.updated_at,
        })
    }

    #[instrument(skip_all)]
    async fn insert_section_to_book(
        &self,
        resource: &crate::resource::Tx,
        param: &super::InsertSectionToBookParam,
    ) -> crate::common::error::AppResult<InsertedSectionModel> {
        let txn = resource.tx().begin().await?;

        let new: section::Model = section::ActiveModel {
            owner_book_id: Set(param.owner_book_id),
            title: Set(param.title.clone()),
            content: Set(param.content.clone()),
            ..Default::default()
        }
        .insert(&txn)
        .await?;

        txn.commit().await?;

        Ok(InsertedSectionModel {
            id: new.id,
            owner_book_id: new.owner_book_id,
            title: new.title,
            content: new.content,
            is_draft: new.is_draft,
            created_at: new.created_at,
            updated_at: new.updated_at,
        })
    }

    #[instrument(skip_all)]
    async fn get_book_list(
        &self,
        resource: &crate::resource::Tx,
        param: &super::GetBookListParam,
    ) -> crate::common::error::AppResult<Paginated<super::BookWithOwnerModel>> {
        let mut condition = Condition::all();

        if let Some(id) = param.id {
            condition = condition.add(book::Column::Id.eq(id));
        }

        if let Some(id) = param.owner_account_id {
            condition = condition.add(book::Column::OwnerAccountId.eq(id));
        }

        if let Some(title) = param.title.clone() {
            let lowered = title.to_lowercase();

            condition = condition.add(
                Expr::expr(Func::lower(Expr::col(book::Column::Title)))
                    .like(format!("%{}%", lowered).as_str()),
            );
        }

        // Internal struct used only for fetching purposes
        #[derive(sea_orm::FromQueryResult)]
        struct BookResult {
            pub id: BigSerial,
            pub title: String,
            pub description: String,
            pub owner_account_id: BigSerial,
            pub owner_account_name: String,
            pub created_at: chrono::DateTime<chrono::FixedOffset>,
            pub updated_at: chrono::DateTime<chrono::FixedOffset>,
        }

        let query = book::Entity::find()
            .select_only()
            .column(book::Column::Id)
            .column(book::Column::Title)
            .column(book::Column::Description)
            .column_as(account::Column::Id, "owner_account_id")
            .column_as(account::Column::DisplayName, "owner_account_name")
            .column(book::Column::CreatedAt)
            .column(book::Column::UpdatedAt)
            .left_join(account::Entity)
            .filter(condition)
            .order_by_desc(book::Column::CreatedAt)
            .group_by(book::Column::Id)
            .group_by(account::Column::Id)
            .group_by(account::Column::DisplayName);

        let total = PaginatorTrait::count(query.clone(), resource.tx()).await? as u64;
        let result_list: Vec<BookResult> = query
            .into_model()
            .paginate(resource.tx(), param.pagination.size() as usize)
            .fetch_page(param.pagination.number as usize)
            .await?;

        Ok(Paginated {
            total,
            data: result_list
                .into_iter()
                .map(|f| super::BookWithOwnerModel {
                    id: f.id,
                    title: f.title,
                    description: f.description,
                    owner_account_id: f.owner_account_id,
                    owner_account_name: f.owner_account_name,
                    created_at: f.created_at,
                    updated_at: f.updated_at,
                })
                .collect(),
        })
    }

    #[instrument(skip_all)]
    async fn get_section_list(
        &self,
        resource: &crate::resource::Tx,
        param: &super::GetSectionListParam,
    ) -> crate::common::error::AppResult<Paginated<super::SectionModel>> {
        let mut condition = Condition::all();

        if let Some(id) = param.section_id {
            condition = condition.add(section::Column::Id.eq(id));
        }

        if let Some(book_id) = param.book_id {
            condition = condition.add(section::Column::OwnerBookId.eq(book_id));
        }

        if let Some(is_draft) = param.draft {
            condition = condition.add(section::Column::IsDraft.eq(is_draft));
        }

        if let Some(title) = param.title.clone() {
            let lowered = title.to_lowercase();

            condition = condition.add(
                Expr::expr(Func::lower(Expr::col(section::Column::Title)))
                    .like(format!("%{}%", lowered).as_str()),
            );
        }

        // Internal struct used only for fetching purposes
        #[derive(sea_orm::FromQueryResult)]
        struct SectionResult {
            pub id: BigSerial,
            pub owner_book_id: BigSerial,
            pub title: String,
            pub content: String,
            pub is_draft: bool,
            pub created_at: chrono::DateTime<chrono::FixedOffset>,
            pub updated_at: chrono::DateTime<chrono::FixedOffset>,
            pub like_count: BigSerial,
        }

        let query = section::Entity::find()
            .select_only()
            .column(section::Column::Id)
            .column(section::Column::OwnerBookId)
            .column(section::Column::Title)
            .column(section::Column::Content)
            .column(section::Column::IsDraft)
            .column(section::Column::CreatedAt)
            .column(section::Column::UpdatedAt)
            .column(section_like::Column::LikeCount)
            .filter(condition)
            .join_rev(JoinType::LeftJoin, section_like::Relation::Section.def())
            .group_by(section_like::Column::LikeCount)
            .group_by(section::Column::Id)
            .order_by_desc(section::Column::CreatedAt);

        let total = PaginatorTrait::count(query.clone(), resource.tx()).await? as u64;
        let result_list: Vec<SectionResult> = query
            .into_model()
            .paginate(resource.tx(), param.pagination.size() as usize)
            .fetch_page(param.pagination.number as usize)
            .await?;

        Ok(Paginated {
            total,
            data: result_list
                .into_iter()
                .map(|f| super::SectionModel {
                    id: f.id,
                    title: f.title,
                    content: f.content,
                    owner_book_id: f.owner_book_id,
                    is_draft: f.is_draft,
                    created_at: f.created_at,
                    updated_at: f.updated_at,
                    like_count: f.like_count,
                })
                .collect(),
        })
    }

    #[instrument(skip_all)]
    async fn update_book(
        &self,
        resource: &crate::resource::Tx,
        param: &super::UpdateBookParam,
    ) -> crate::common::error::AppResult<super::BookModel> {
        let tx = resource.tx().begin().await?;
        let model: Option<book::Model> = book::Entity::find()
            .filter(book::Column::Id.eq(param.book_id))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let mut active: book::ActiveModel = m.into();
                if let Some(title) = param.title.clone() {
                    active.title = Set(title)
                }

                if let Some(description) = param.description.clone() {
                    active.description = Set(description)
                }

                active.updated_at = Set(Utc::now().into());

                let model: book::Model = active.update(&tx).await?;

                tx.commit().await?;

                Ok(BookModel {
                    id: model.id,
                    title: model.title,
                    description: model.description,
                    owner_account_id: model.owner_account_id,
                    created_at: model.created_at,
                    updated_at: model.updated_at,
                })
            }
            None => Err(AppError::Generic(
                "Book with that id is not found and isn't updated".to_string(),
            )),
        }
    }

    #[instrument(skip_all)]
    async fn update_section(
        &self,
        resource: &crate::resource::Tx,
        param: &super::UpdateSectionParam,
    ) -> crate::common::error::AppResult<UpdatedSectionModel> {
        let tx = resource.tx().begin().await?;
        let model: Option<section::Model> = section::Entity::find()
            .filter(section::Column::Id.eq(param.section_id))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let mut active: section::ActiveModel = m.into();
                if let Some(title) = param.title.clone() {
                    active.title = Set(title)
                }

                if let Some(content) = param.content.clone() {
                    active.content = Set(content)
                }

                active.is_draft = Set(param.is_draft);
                active.updated_at = Set(Utc::now().into());

                let model: section::Model = active.update(&tx).await?;

                tx.commit().await?;

                Ok(UpdatedSectionModel {
                    id: model.id,
                    title: model.title,
                    content: model.content,
                    owner_book_id: model.owner_book_id,
                    is_draft: model.is_draft,
                    created_at: model.created_at,
                    updated_at: model.updated_at,
                })
            }
            None => Err(AppError::Generic(
                "Section with that id is not found and isn't updated".to_string(),
            )),
        }
    }

    #[instrument(skip_all)]
    async fn delete_section(
        &self,
        resource: &crate::resource::Tx,
        param: &super::DeleteSectionParam,
    ) -> crate::common::error::AppResult<()> {
        let tx = resource.tx().begin().await?;
        let model: Option<section::Model> = section::Entity::find()
            .filter(section::Column::Id.eq(param.section_id))
            .one(&tx)
            .await?;

        match model {
            Some(m) => {
                let active: section::ActiveModel = m.into();
                let _ = active.delete(&tx).await?;
                tx.commit().await?;

                Ok(())
            }
            None => Err(AppError::Generic(
                "Section with that id is not found and isn't deleted".to_string(),
            )),
        }
    }
}
