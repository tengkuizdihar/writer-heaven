use std::collections::BTreeMap;

use chrono::Utc;
use sea_orm::{DatabaseBackend, MockDatabase, Statement, Transaction, Value, Values};

use crate::repository::book::{BookRepository, GetSectionListParam};
use crate::{
    repository::book::BookRepositoryImpl,
    resource::{Connection, Tx},
};

use pretty_assertions::assert_eq;

#[tokio::test]
async fn test_get_section_list() {
    // ARRANGE - DATABASE
    let now: chrono::DateTime<chrono::FixedOffset> = Utc::now().into(); // VERY IMPORTANT! IT NEEDS TO BE FIXED OFFSET! BECAUSE THE DATABASE DEFINITION IS LIKE SO.

    let mut row1 = BTreeMap::<&str, Value>::new();
    row1.insert("id", 1i64.into());
    row1.insert("owner_book_id", 2i64.into());
    row1.insert("title", "title".into());
    row1.insert("content", "content".into());
    row1.insert("is_draft", true.into());
    row1.insert("created_at", now.into());
    row1.insert("updated_at", now.into());
    row1.insert("like_count", Some(0i64).into());

    let conn = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results(vec![vec![
            (BTreeMap::from([("num_items", Value::BigInt(Some(1)))])), // Need to be num_items and bigint for paginated related query
        ]])
        .append_query_results(vec![vec![row1]])
        .into_connection();

    let conn = Connection { conn };
    let tx = Tx::begin_transaction(&conn).await.unwrap();

    // ARRANGE - EXPECTATION
    let transaction = vec![
        Statement {
            sql: "BEGIN".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT COUNT(*) AS num_items FROM (SELECT \"section\".\"id\", \"section\".\"owner_book_id\", \"section\".\"title\", \"section\".\"content\", \"section\".\"is_draft\", \"section\".\"created_at\", \"section\".\"updated_at\", \"section_like\".\"like_count\" FROM \"section\" LEFT JOIN \"section_like\" ON \"section_like\".\"owner_section_id\" = \"section\".\"id\" WHERE \"section\".\"id\" = $1 AND \"section\".\"owner_book_id\" = $2 AND \"section\".\"is_draft\" = $3 AND LOWER(\"title\") LIKE $4 GROUP BY \"section_like\".\"like_count\", \"section\".\"id\" ORDER BY \"section\".\"created_at\" DESC) AS \"sub_query\"".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(1)),
                        Value::BigInt(Some(2)),
                        Value::Bool(Some(true)),
                        Value::String(Some(Box::new("%title%".into()))),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "SELECT \"section\".\"id\", \"section\".\"owner_book_id\", \"section\".\"title\", \"section\".\"content\", \"section\".\"is_draft\", \"section\".\"created_at\", \"section\".\"updated_at\", \"section_like\".\"like_count\" FROM \"section\" LEFT JOIN \"section_like\" ON \"section_like\".\"owner_section_id\" = \"section\".\"id\" WHERE \"section\".\"id\" = $1 AND \"section\".\"owner_book_id\" = $2 AND \"section\".\"is_draft\" = $3 AND LOWER(\"title\") LIKE $4 GROUP BY \"section_like\".\"like_count\", \"section\".\"id\" ORDER BY \"section\".\"created_at\" DESC LIMIT $5 OFFSET $6".into(),
            values: Some(
                Values(
                    vec![
                        Value::BigInt(Some(1)),
                        Value::BigInt(Some(2)),
                        Value::Bool(Some(true)),
                        Value::String(Some(Box::new("%title%".into()))),
                        Value::BigUnsigned(Some(1)),
                        Value::BigUnsigned(Some(0)),
                    ],
                ),
            ),
            db_backend: DatabaseBackend::Postgres,
        },
        Statement {
            sql: "COMMIT".into(),
            values: None,
            db_backend: DatabaseBackend::Postgres,
        },
    ];

    // ARRANGE - PARAM AND REPO
    let repo = BookRepositoryImpl {};
    let param = GetSectionListParam {
        pagination: crate::usecase::common::Pagination { size: 1, number: 0 },
        section_id: Some(1),
        book_id: Some(2),
        title: Some("title".into()),
        draft: Some(true),
    };

    // ACT
    let result = repo.get_section_list(&tx, &param).await.unwrap();
    tx.commit().await.unwrap();

    // ASSERT
    assert_eq!(
        super::SectionModel {
            id: 1,
            created_at: now,
            updated_at: now,
            owner_book_id: 2,
            title: "title".to_string(),
            content: "content".to_string(),
            is_draft: true,
            like_count: 0
        },
        result.data[0]
    );

    assert_eq!(
        vec![Transaction::many(transaction)],
        conn.conn.into_transaction_log()
    );
}
