mod implement;
#[cfg(test)]
mod testing;

use crate::{
    common::{alias::BigSerial, error::AppResult},
    resource::Tx,
    usecase::common::{Paginated, Pagination},
};
use tonic::async_trait;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait BookRepository {
    async fn insert_book(&self, resource: &Tx, param: &InsertBookParam) -> AppResult<BookModel>;

    async fn insert_section_to_book(
        &self,
        resource: &Tx,
        param: &InsertSectionToBookParam,
    ) -> AppResult<InsertedSectionModel>;

    async fn get_book_list(
        &self,
        resource: &Tx,
        param: &GetBookListParam,
    ) -> AppResult<Paginated<BookWithOwnerModel>>;

    async fn get_section_list(
        &self,
        resource: &Tx,
        param: &GetSectionListParam,
    ) -> AppResult<Paginated<SectionModel>>;

    async fn update_book(&self, resource: &Tx, param: &UpdateBookParam) -> AppResult<BookModel>;

    async fn update_section(
        &self,
        resource: &Tx,
        param: &UpdateSectionParam,
    ) -> AppResult<UpdatedSectionModel>;

    async fn delete_section(&self, resource: &Tx, param: &DeleteSectionParam) -> AppResult<()>;
}

#[derive(Debug, Default)]
pub struct BookRepositoryImpl;

#[derive(Debug)]
pub struct BookModel {
    pub id: BigSerial,
    pub title: String,
    pub description: String,
    pub owner_account_id: BigSerial,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct BookWithOwnerModel {
    pub id: BigSerial,
    pub title: String,
    pub description: String,
    pub owner_account_id: BigSerial,
    pub owner_account_name: String,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct InsertBookParam {
    pub title: String,
    pub description: String,
    pub owner_account_id: BigSerial,
}

#[derive(Debug)]
pub struct GetBookListParam {
    pub pagination: Pagination,
    pub id: Option<BigSerial>,
    pub owner_account_id: Option<BigSerial>,
    pub title: Option<String>,
}

#[derive(Debug)]
pub struct InsertSectionToBookParam {
    pub owner_book_id: BigSerial,
    pub title: String,
    pub content: String,
}

#[derive(Debug)]
pub struct GetSectionListParam {
    pub pagination: Pagination,
    pub book_id: Option<BigSerial>,
    pub section_id: Option<BigSerial>,
    pub title: Option<String>,
    pub draft: Option<bool>,
}

#[derive(Debug, PartialEq)]
pub struct SectionModel {
    pub id: BigSerial,
    pub owner_book_id: BigSerial,
    pub title: String,
    pub content: String,
    pub is_draft: bool,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
    pub like_count: BigSerial,
}

#[derive(Debug)]
pub struct InsertedSectionModel {
    pub id: BigSerial,
    pub owner_book_id: BigSerial,
    pub title: String,
    pub content: String,
    pub is_draft: bool,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

pub struct UpdatedSectionModel {
    pub id: BigSerial,
    pub owner_book_id: BigSerial,
    pub title: String,
    pub content: String,
    pub is_draft: bool,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug)]
pub struct UpdateSectionParam {
    pub section_id: BigSerial,
    pub is_draft: bool,
    pub title: Option<String>,
    pub content: Option<String>,
}

#[derive(Debug)]
pub struct UpdateBookParam {
    pub book_id: BigSerial,
    pub title: Option<String>,
    pub description: Option<String>,
}

#[derive(Debug)]
pub struct DeleteSectionParam {
    pub section_id: BigSerial,
}
