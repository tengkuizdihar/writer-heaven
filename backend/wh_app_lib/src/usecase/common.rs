use crate::common::crypto::JwtClaims;

#[derive(Debug, Clone)]
pub struct Header {
    pub claim: Option<JwtClaims>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Pagination {
    pub size: u64,
    pub number: u64,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Paginated<T> {
    pub total: u64,
    pub data: Vec<T>,
}

impl Pagination {
    pub fn size(&self) -> u64 {
        self.size.clamp(0, 25)
    }
}
