mod implement;
#[cfg(test)]
mod testing;

use super::common::Header;
use crate::{
    common::{alias::BigSerial, crypto::Secret, error::AppResult, permission::PermissionContainer},
    repository::{account::AccountRepository, permission::PermissionRepository},
    resource::Connection,
};
use tonic::async_trait;
use validator::Validate;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait AccountUsecase {
    async fn insert_account(&self, param: &InsertAccountParam) -> AppResult<BigSerial>;

    async fn get_account(&self, param: &GetAccountParam) -> AppResult<Option<GetAccountReply>>;

    async fn login_native_account(&self, param: &LoginNativeAccountParam) -> AppResult<String>;

    /// This method should not be connected to any kinds of handler that's accessible from the open internet.
    /// It is strictly used by the internal of this program.
    async fn set_admin_account(&self, param: &InsertAdminAccountParam) -> AppResult<()>;
}

pub struct AccountUsecaseImpl {
    pub account: Box<dyn AccountRepository + Send + Sync>,
    pub permission: Box<dyn PermissionRepository + Send + Sync>,
    pub connection: Connection,
}

#[derive(Debug, Validate)]
pub struct InsertAccountParam {
    pub header: Header,

    #[validate(email, length(min = 3, max = 100))]
    pub email: String,

    #[validate(length(min = 3, max = 100))]
    pub display_name: String,

    #[validate(length(min = 6, max = 100))]
    pub password: String,
}

#[derive(Debug)]
pub struct GetAccountParam {
    pub header: Header,
    pub id: BigSerial,
}

#[derive(Debug)]
pub struct LoginNativeAccountParam {
    pub email: String,
    pub password: Secret<String>,
    pub server_secret: Secret<String>,
}

#[derive(Debug, Validate)]
pub struct InsertAdminAccountParam {
    #[validate(email, length(min = 3, max = 100))]
    pub email: String,

    #[validate(length(min = 3, max = 100))]
    pub display_name: String,

    #[validate(length(min = 6, max = 100))]
    pub password: String,
}

#[derive(Debug)]
pub struct GetAccountReply {
    pub id: BigSerial,
    pub email: String,
    pub display_name: String,
    pub pass_hash: String,
    pub permission: PermissionContainer,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}
