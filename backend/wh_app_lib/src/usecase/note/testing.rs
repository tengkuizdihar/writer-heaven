use std::future;

use chrono::Utc;
use pretty_assertions as pa;

use super::{InsertNoteParam, NoteUsecase, NoteUsecaseImpl};
use crate::{
    common::crypto::JwtClaims, repository::note::MockNoteRepository, usecase::common::Header,
};

#[tokio::test]
async fn test_insert_note_normal() {
    // ARRANGE
    let created_at = Utc::now();

    // ARRANGE - REPOSITORY
    let mut mock_note = MockNoteRepository::default();
    mock_note
        .expect_insert_note()
        .withf(move |_, b| {
            pa::assert_eq!(
                crate::repository::note::InsertNoteParam {
                    content: "content baby".to_string(),
                    owner_account_id: 123,
                },
                *b
            );

            true
        })
        .return_once(move |_, _| {
            Box::pin(future::ready(Ok(crate::repository::note::NoteModel {
                id: 1,
                owner_account_id: 123,
                content: "content baby".to_string(),
                created_at: created_at.into(),
                updated_at: created_at.into(),
            })))
        });

    // ARRANGE - USECASE
    let mut uc = NoteUsecaseImpl::mock();
    uc.note = Box::new(mock_note);

    // ARRANGE - PARAM
    let param = InsertNoteParam {
        header: Header {
            claim: Some(JwtClaims {
                exp: 0,
                sub: 123,
                slt: "fakesalt".to_string(),
            }),
        },
        content: "content baby".to_string(),
        tag_list: vec![],
    };

    // ARRANGE - EXPECTED OUTPUT
    let expect = super::NoteModel {
        id: 1,
        owner_account_id: 123,
        content: "content baby".to_string(),
        tag_list: vec![],
        created_at: created_at.into(),
        updated_at: created_at.into(),
    };

    // ACT
    let result = uc.insert_note(&param).await.unwrap();

    // ASSERT
    pa::assert_eq!(expect, result);
}
