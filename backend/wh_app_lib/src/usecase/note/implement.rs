use super::{NoteUsecase, NoteUsecaseImpl};
use crate::{common::error::AppError, resource::Tx};
use tonic::async_trait;
use validator::Validate;

#[async_trait]
impl NoteUsecase for NoteUsecaseImpl {
    /// TODO-IMPROVEMENT There is so many clone in this code
    #[tracing::instrument(skip(self))]
    async fn insert_note(
        &self,
        param: &super::InsertNoteParam,
    ) -> crate::common::error::AppResult<super::NoteModel> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let new_note_param = crate::repository::note::InsertNoteParam {
            content: param.content.clone(),
            owner_account_id: claim.sub,
        };
        let new_note = self.note.insert_note(&resource, &new_note_param).await?;

        for title in param.tag_list.iter() {
            let insert_tag_param = crate::repository::tag::InsertTagParam {
                title: title.clone(),
                owner_note_id: new_note.id,
            };
            let _ = self.tag.insert_tag(&resource, &insert_tag_param).await?;
        }

        resource.commit().await?;
        Ok(super::NoteModel {
            id: new_note.id,
            owner_account_id: new_note.owner_account_id,
            content: new_note.content,
            tag_list: param.tag_list.clone(),
            created_at: new_note.created_at,
            updated_at: new_note.updated_at,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn get_note_list(
        &self,
        param: &super::GetNoteListParam,
    ) -> crate::common::error::AppResult<crate::usecase::common::Paginated<super::NoteModel>> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let param = crate::repository::note::GetNoteListParam {
            pagination: param.pagination,
            id: param.id,
            keyword: param.keyword.clone(),
            owner_account_id: Some(claim.sub),
        };
        let result = self.note.get_note_list(&resource, &param).await?;

        resource.commit().await?;
        Ok(crate::usecase::common::Paginated {
            total: result.total,
            data: result
                .data
                .into_iter()
                .map(|n| super::NoteModel {
                    id: n.id,
                    owner_account_id: n.owner_account_id,
                    content: n.content,
                    tag_list: n.tag_list,
                    created_at: n.created_at,
                    updated_at: n.updated_at,
                })
                .collect(),
        })
    }

    #[tracing::instrument(skip(self))]
    async fn update_note(
        &self,
        param: &super::UpdateNoteParam,
    ) -> crate::common::error::AppResult<super::NoteModel> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let note_result = self
            .note
            .update_note(
                &resource,
                &crate::repository::note::UpdateNoteParam {
                    id: param.id,
                    content: Some(param.content.clone()),
                    owner_account_id: claim.sub,
                },
            )
            .await?;

        let tag_result = self
            .tag
            .get_tag_list(
                &resource,
                &crate::repository::tag::GetTagListParam {
                    owner_note_id: Some(note_result.id),
                    keyword: None,
                },
            )
            .await?
            .into_iter()
            .map(|f| f.title)
            .collect::<Vec<_>>();

        let tag_to_insert = param
            .tag_list
            .clone()
            .into_iter()
            .filter(|p| !tag_result.contains(p))
            .collect::<Vec<_>>();

        let tag_to_delete = tag_result
            .into_iter()
            .filter(|p| !param.tag_list.contains(p))
            .collect::<Vec<_>>();

        for tag in tag_to_insert.into_iter() {
            let _ = self
                .tag
                .insert_tag(
                    &resource,
                    &crate::repository::tag::InsertTagParam {
                        owner_note_id: note_result.id,
                        title: tag,
                    },
                )
                .await?;
        }

        for tag in tag_to_delete.into_iter() {
            let _ = self
                .tag
                .delete_tag(
                    &resource,
                    &crate::repository::tag::DeleteTagParam {
                        owner_note_id: note_result.id,
                        title: tag,
                    },
                )
                .await?;
        }

        let param = crate::repository::note::GetNoteListParam {
            pagination: crate::usecase::common::Pagination { size: 1, number: 0 },
            id: Some(note_result.id),
            owner_account_id: Some(claim.sub),
            keyword: None,
        };
        let result = self
            .note
            .get_note_list(&resource, &param)
            .await?
            .data
            .into_iter()
            .next()
            .ok_or_else(|| {
                AppError::Generic("Note doesn't exist after updating, how?".to_string())
            })?;

        resource.commit().await?;
        Ok(super::NoteModel {
            id: result.id,
            owner_account_id: result.owner_account_id,
            content: result.content,
            tag_list: result.tag_list,
            created_at: result.created_at,
            updated_at: result.updated_at,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn delete_note(
        &self,
        param: &super::DeleteNoteParam,
    ) -> crate::common::error::AppResult<()> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let r_get_param = crate::repository::note::GetNoteListParam {
            pagination: crate::usecase::common::Pagination { size: 1, number: 0 },
            owner_account_id: Some(claim.sub),
            id: Some(param.id),
            keyword: None,
        };
        let notes = self
            .note
            .get_note_list(&resource, &r_get_param)
            .await?
            .data
            .into_iter()
            .next()
            .ok_or_else(|| {
                AppError::Generic(
                    "There's no notes found that's attached to this user.".to_string(),
                )
            })?;

        let r_param = crate::repository::note::DeleteNoteParam { id: notes.id };
        self.note.delete_note(&resource, &r_param).await?;

        resource.commit().await?;
        Ok(())
    }
}

impl NoteUsecaseImpl {
    #[cfg(test)]
    pub fn mock() -> Self {
        use crate::{
            repository::{note::MockNoteRepository, tag::MockTagRepository},
            resource::Connection,
        };

        NoteUsecaseImpl {
            connection: Connection::mock(),
            note: Box::new(MockNoteRepository::default()),
            tag: Box::new(MockTagRepository::default()),
        }
    }
}
