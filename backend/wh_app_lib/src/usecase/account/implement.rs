use super::{
    AccountUsecase, AccountUsecaseImpl, GetAccountParam, GetAccountReply, InsertAccountParam,
    LoginNativeAccountParam,
};
use crate::{
    common::{
        alias::BigSerial,
        crypto::{jwt_encode, salted_hash, verify_password, AuthenticationResult},
        error::{AppError, AppResult},
        permission::{check_permission, role_to_permission, Permission, Role},
    },
    repository::{
        account::GetAccountParam as RGetAccountParam,
        permission::{GetPermissionRequest, InsertPermissionRequest, UpsertPermissionRequest},
    },
    resource::Tx,
};
use validator::{validate_email, validate_length, Validate, ValidationError, ValidationErrors};

#[tonic::async_trait]
impl AccountUsecase for AccountUsecaseImpl {
    #[tracing::instrument(skip_all)]
    async fn insert_account(&self, param: &InsertAccountParam) -> AppResult<BigSerial> {
        param.validate()?;

        let password = param.password.clone();
        let pass_hash = tokio::task::spawn_blocking(move || salted_hash(password)).await??;
        let resource = Tx::begin_transaction(&self.connection).await?;

        let insert_param = crate::repository::account::InsertAccountParam {
            email: param.email.clone(),
            display_name: param.display_name.clone(),
            pass_hash,
        };

        let new_account = self
            .account
            .insert_account(&resource, &insert_param)
            .await?;

        // The permission is different between admin and regular.
        let permission_container = match param.header.claim.as_ref() {
            Some(claim) => {
                // Get permission
                let permission_param = GetPermissionRequest {
                    account_id: claim.sub,
                };
                let permission_flags = self
                    .permission
                    .get_permission(&resource, &permission_param)
                    .await?
                    .permission_container
                    .ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

                match check_permission(Permission::CreateUserWithAnyRole, &permission_flags) {
                    true => role_to_permission(Role::Admin),
                    false => role_to_permission(Role::Regular),
                }
            }
            None => role_to_permission(Role::Regular),
        };

        let insert_permission = InsertPermissionRequest {
            account_id: new_account.id,
            permission_container,
        };

        let _ = self
            .permission
            .insert_permission(&resource, &insert_permission)
            .await?;

        resource.commit().await?;

        Ok(new_account.id)
    }

    #[tracing::instrument(skip_all)]
    async fn get_account(&self, param: &GetAccountParam) -> AppResult<Option<GetAccountReply>> {
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        // Get permission
        let permission_param = GetPermissionRequest {
            account_id: claim.sub,
        };
        let permission = self
            .permission
            .get_permission(&resource, &permission_param)
            .await?;

        match permission.permission_container {
            Some(perm) => {
                let mut p = RGetAccountParam {
                    id: Some(param.id),
                    email: None,
                };

                if param.id == 0 {
                    p.id = Some(claim.sub);
                }

                let result = if check_permission(Permission::GetAccountOtherMember, &perm) {
                    self.account.get_account(&resource, &p).await?
                } else {
                    p.id = Some(claim.sub);
                    self.account.get_account(&resource, &p).await?
                };

                let reply = match result {
                    Some(account) => {
                        let permission_param = GetPermissionRequest {
                            account_id: account.id,
                        };
                        let permission = self
                            .permission
                            .get_permission(&resource, &permission_param)
                            .await?.permission_container
                            .ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

                        Ok(Some(GetAccountReply {
                            id: account.id,
                            email: account.email,
                            display_name: account.display_name,
                            pass_hash: account.pass_hash,
                            permission,
                            created_at: account.created_at,
                            updated_at: account.updated_at,
                        }))
                    },
                    None => Ok(None),
                };

                resource.commit().await?;
                reply
            }
            None => Err(AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string())),
        }
    }

    #[tracing::instrument(skip_all)]
    async fn login_native_account(&self, param: &LoginNativeAccountParam) -> AppResult<String> {
        let p = RGetAccountParam {
            id: None,
            email: Some(param.email.clone()),
        };

        let resource = Tx::begin_transaction(&self.connection).await?;

        let result = self.account.get_account(&resource, &p).await?;

        match result {
            // Check to see if the user with that email exist
            Some(account) => {
                // Check whether the password hash is valid or not
                let auth_result = verify_password(&param.password.0, &account.pass_hash)?;

                match auth_result {
                    AuthenticationResult::Allow => {
                        let result = jwt_encode(account.id, param.server_secret.0.as_str())?;
                        resource.commit().await?;
                        Ok(result)
                    }
                    AuthenticationResult::Disallow => {
                        Err(AppError::Generic("Your password is invalid!".to_string()))
                    }
                }
            }
            None => Err(AppError::Generic(
                "Account with that email does not exist!".to_string(),
            )),
        }
    }

    #[tracing::instrument(skip_all)]
    async fn set_admin_account(&self, param: &super::InsertAdminAccountParam) -> AppResult<()> {
        param.validate()?;

        let password = param.password.clone();
        let pass_hash = tokio::task::spawn_blocking(move || salted_hash(password)).await??;
        let resource = Tx::begin_transaction(&self.connection).await?;

        let upsert_account = crate::repository::account::UpsertAccountParam {
            email: param.email.clone(),
            display_name: param.display_name.clone(),
            pass_hash,
        };

        let account = self
            .account
            .upsert_account(&resource, &upsert_account)
            .await?;

        // The permission for admin
        let permission_container = role_to_permission(Role::Admin);
        let upsert_permission = UpsertPermissionRequest {
            account_id: account.id,
            permission_container,
        };

        let _ = self
            .permission
            .upsert_permission(&resource, &upsert_permission)
            .await?;

        resource.commit().await?;

        Ok(())
    }
}

impl Validate for LoginNativeAccountParam {
    fn validate(&self) -> Result<(), validator::ValidationErrors> {
        let mut errors = ValidationErrors::new();

        if validate_email(&self.email) && validate_length(&self.email, Some(3), Some(100), None) {
            errors.add("email", ValidationError::new("min"));
        }
        if validate_length(&self.password.0, Some(6), Some(100), None) {
            errors.add("password", ValidationError::new("min"));
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}
