mod implement;
#[cfg(test)]
mod testing;

use super::common::{Header, Paginated, Pagination};
use crate::{
    common::{alias::BigSerial, error::AppResult},
    repository::{note::NoteRepository, tag::TagRepository},
    resource::Connection,
};
use tonic::async_trait;
use validator::Validate;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait NoteUsecase {
    async fn insert_note(&self, param: &InsertNoteParam) -> AppResult<NoteModel>;

    async fn get_note_list(&self, param: &GetNoteListParam) -> AppResult<Paginated<NoteModel>>;

    async fn update_note(&self, param: &UpdateNoteParam) -> AppResult<NoteModel>;

    async fn delete_note(&self, param: &DeleteNoteParam) -> AppResult<()>;
}

pub struct NoteUsecaseImpl {
    pub connection: Connection,
    pub note: Box<dyn NoteRepository + Send + Sync>,
    pub tag: Box<dyn TagRepository + Send + Sync>,
}

#[derive(Debug, Validate)]
pub struct InsertNoteParam {
    pub header: Header,

    #[validate(length(min = 3, max = 25000))]
    pub content: String,

    #[validate(length(min = 0, max = 10))]
    pub tag_list: Vec<String>,
}

#[derive(Debug, Validate)]
pub struct GetNoteListParam {
    pub header: Header,
    pub pagination: Pagination,

    pub id: Option<i64>,

    #[validate(length(min = 3, max = 100))]
    pub keyword: Option<String>,

    pub tag_list: Vec<String>,
}

#[derive(Debug, PartialEq)]
pub struct NoteModel {
    pub id: BigSerial,
    pub owner_account_id: BigSerial,
    pub content: String,
    pub tag_list: Vec<String>,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Debug, Validate)]
pub struct UpdateNoteParam {
    pub header: Header,
    pub id: BigSerial,

    #[validate(length(min = 3, max = 25000))]
    pub content: String,

    #[validate(length(min = 0, max = 10))]
    pub tag_list: Vec<String>,
}

#[derive(Debug, Validate)]
pub struct DeleteNoteParam {
    pub header: Header,
    pub id: BigSerial,
}
