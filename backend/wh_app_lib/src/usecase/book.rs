mod implement;
#[cfg(test)]
mod testing;

use super::common::{Header, Paginated, Pagination};
use crate::{
    common::{alias::BigSerial, error::AppResult},
    repository::{
        book::BookRepository, like_account::LikeAccountRepository,
        section_like::SectionLikeRepository,
    },
    resource::Connection,
};
use tonic::async_trait;
use validator::Validate;

#[async_trait]
#[cfg_attr(test, automock)]
pub trait BookUsecase {
    async fn insert_book(&self, param: &InsertBookParam) -> AppResult<BigSerial>;

    async fn get_book_list(
        &self,

        param: &GetBookListParam,
    ) -> AppResult<Paginated<BookWithOwnerNameModel>>;

    async fn insert_section_to_book(
        &self,
        param: &InsertSectionToBookParam,
    ) -> AppResult<SectionModel>;

    async fn get_section_list(
        &self,
        param: &GetSectionListParam,
    ) -> AppResult<Paginated<SectionModel>>;

    async fn update_book(&self, param: &UpdateBookParam) -> AppResult<BookModel>;

    async fn update_section(&self, param: &UpdateSectionParam) -> AppResult<SectionModel>;

    async fn delete_section(&self, param: &DeleteSectionParam) -> AppResult<()>;

    async fn like_section(&self, param: &LikeSectionParam) -> AppResult<()>;

    async fn get_like_section(
        &self,
        param: &GetLikeSectionParam,
    ) -> AppResult<GetLikeSectionResult>;
}

pub struct BookUsecaseImpl {
    pub book: Box<dyn BookRepository + Send + Sync>,
    pub section_like: Box<dyn SectionLikeRepository + Send + Sync>,
    pub like_account: Box<dyn LikeAccountRepository + Send + Sync>,
    pub connection: Connection,
}

#[derive(Debug, Validate)]
pub struct InsertBookParam {
    pub header: Header,

    #[validate(length(min = 3, max = 255))]
    pub title: String,

    #[validate(length(min = 3, max = 500))]
    pub description: String,
}

#[derive(Debug)]
pub struct GetBookListParam {
    pub header: Header,
    pub pagination: Pagination,
    pub id: Option<BigSerial>,
    pub owner_id: Option<BigSerial>,
    pub title: Option<String>,
}

#[derive(Debug)]
pub struct BookWithOwnerNameModel {
    pub id: BigSerial,
    pub title: String,
    pub description: String,
    pub owner_name: String,
    pub owner_id: BigSerial,
}

#[derive(Debug)]
pub struct BookModel {
    pub id: BigSerial,
    pub title: String,
    pub description: String,
    pub owner_account_id: BigSerial,
}

#[derive(Debug)]
pub struct GetSectionListParam {
    pub header: Header,
    pub pagination: Pagination,
    pub book_id: BigSerial,
    pub section_id: Option<BigSerial>,
    pub title: Option<String>,
}

#[derive(Debug)]
pub struct SectionModel {
    pub id: BigSerial,
    pub owner_book_id: BigSerial,
    pub title: String,
    pub content: String,
    pub is_draft: bool,
    pub created_at: chrono::DateTime<chrono::FixedOffset>,
    pub updated_at: chrono::DateTime<chrono::FixedOffset>,
    pub like_count: BigSerial,
}

#[derive(Debug, Validate)]
pub struct InsertSectionToBookParam {
    pub header: Header,
    pub book_id: BigSerial,

    #[validate(length(min = 3, max = 255))]
    pub title: String,

    #[validate(length(min = 3, max = 25000))]
    pub content: String,
}

#[derive(Debug, Validate)]
pub struct UpdateSectionParam {
    pub header: Header,
    pub book_id: BigSerial,
    pub section_id: BigSerial,
    pub is_draft: bool,

    #[validate(length(min = 3, max = 255))]
    pub title: Option<String>,

    #[validate(length(min = 3, max = 25000))]
    pub content: Option<String>,
}

#[derive(Debug, Validate)]
pub struct DeleteSectionParam {
    pub header: Header,
    pub section_id: BigSerial,
}

#[derive(Debug, Validate)]
pub struct UpdateBookParam {
    pub header: Header,
    pub book_id: BigSerial,

    #[validate(length(min = 3, max = 255))]
    pub title: Option<String>,

    #[validate(length(min = 3, max = 500))]
    pub description: Option<String>,
}

#[derive(Debug)]
pub struct LikeSectionParam {
    pub header: Header,
    pub section_id: BigSerial,
    pub is_like: bool,
}

#[derive(Debug)]
pub struct GetLikeSectionParam {
    pub header: Header,
    pub section_id: BigSerial,
}

#[derive(Debug)]
pub struct GetLikeSectionResult {
    pub is_like: bool,
}
