use super::{BookUsecase, BookUsecaseImpl};

use crate::{
    common::error::AppError,
    repository::{
        book::{
            DeleteSectionParam, GetBookListParam, GetSectionListParam, InsertSectionToBookParam,
            UpdateBookParam, UpdateSectionParam,
        },
        like_account::{DeleteLikeAccountParam, GetLikeAccountParam, InsertLikeAccountParam},
        section_like::{GetSectionLikeParam, InsertSectionLikeParam, UpdateSectionLikeParam},
    },
    resource::Tx,
    usecase::common::{Paginated, Pagination},
};

use tonic::async_trait;
use validator::Validate;

#[async_trait]
impl BookUsecase for BookUsecaseImpl {
    #[tracing::instrument(skip(self))]
    async fn insert_book(
        &self,
        param: &super::InsertBookParam,
    ) -> crate::common::error::AppResult<crate::common::alias::BigSerial> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let param = crate::repository::book::InsertBookParam {
            title: param.title.clone(),
            description: param.description.clone(),
            owner_account_id: claim.sub,
        };

        let result = self.book.insert_book(&resource, &param).await?;
        resource.commit().await?;

        Ok(result.id)
    }

    #[tracing::instrument(skip(self))]
    async fn get_book_list(
        &self,
        param: &super::GetBookListParam,
    ) -> crate::common::error::AppResult<Paginated<super::BookWithOwnerNameModel>> {
        let resource = Tx::begin_transaction(&self.connection).await?;

        let param = &GetBookListParam {
            pagination: param.pagination,
            id: param.id,
            owner_account_id: param.owner_id,
            title: param.title.clone(),
        };
        let book_list = self.book.get_book_list(&resource, param).await?;

        Ok(Paginated {
            total: book_list.total,
            data: book_list
                .data
                .into_iter()
                .map(|f| super::BookWithOwnerNameModel {
                    id: f.id,
                    title: f.title,
                    description: f.description,
                    owner_name: f.owner_account_name,
                    owner_id: f.owner_account_id,
                })
                .collect(),
        })
    }

    #[tracing::instrument(skip(self))]
    async fn insert_section_to_book(
        &self,
        param: &super::InsertSectionToBookParam,
    ) -> crate::common::error::AppResult<super::SectionModel> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let get_param = GetBookListParam {
            pagination: Pagination { size: 1, number: 0 },
            id: Some(param.book_id),
            owner_account_id: Some(claim.sub),
            title: None,
        };
        let book = self.book.get_book_list(&resource, &get_param).await?;

        if book.data.len().eq(&0) {
            Err(AppError::Generic(
                "You are not allowed to insert a section to that book.".to_string(),
            ))
        } else {
            let request_param = InsertSectionToBookParam {
                owner_book_id: param.book_id,
                title: param.title.clone(),
                content: param.content.clone(),
            };
            let result = self
                .book
                .insert_section_to_book(&resource, &request_param)
                .await?;

            let request_param = InsertSectionLikeParam {
                section_id: result.id,
            };
            let _ = self
                .section_like
                .insert_section_like(&resource, &request_param)
                .await?;

            resource.commit().await?;

            Ok(super::SectionModel {
                id: result.id,
                owner_book_id: result.owner_book_id,
                title: result.title,
                content: result.content,
                is_draft: result.is_draft,
                created_at: result.created_at,
                updated_at: result.updated_at,
                like_count: 0,
            })
        }
    }

    #[tracing::instrument(skip(self))]
    async fn get_section_list(
        &self,
        param: &super::GetSectionListParam,
    ) -> crate::common::error::AppResult<Paginated<super::SectionModel>> {
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref();

        let get_param = GetBookListParam {
            pagination: Pagination { size: 1, number: 0 },
            id: Some(param.book_id),
            owner_account_id: None,
            title: None,
        };
        let book = self.book.get_book_list(&resource, &get_param).await?;
        let book = match book.data.iter().take(1).next() {
            Some(b) => b,
            None => {
                return Err(AppError::Generic(
                    "The book you're searching for doesn't exist.".to_string(),
                ))
            }
        };

        let repo_param = crate::repository::book::GetSectionListParam {
            pagination: param.pagination,
            book_id: Some(param.book_id),
            section_id: param.section_id,
            title: param.title.clone(),
            draft: match book.owner_account_id.eq(&claim.map(|f| f.sub).unwrap_or(0)) {
                true => None,
                false => Some(false),
            },
        };

        let sections = self.book.get_section_list(&resource, &repo_param).await?;

        Ok(Paginated {
            total: sections.total,
            data: sections
                .data
                .into_iter()
                .map(|f| super::SectionModel {
                    id: f.id,
                    owner_book_id: f.owner_book_id,
                    title: f.title,
                    content: f.content,
                    is_draft: f.is_draft,
                    created_at: f.created_at,
                    updated_at: f.updated_at,
                    like_count: f.like_count,
                })
                .collect(),
        })
    }

    #[tracing::instrument(skip(self))]
    async fn update_book(
        &self,
        param: &super::UpdateBookParam,
    ) -> crate::common::error::AppResult<super::BookModel> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let get_param = GetBookListParam {
            pagination: Pagination { size: 1, number: 0 },
            id: Some(param.book_id),
            owner_account_id: Some(claim.sub),
            title: None,
        };
        let book = self
            .book
            .get_book_list(&resource, &get_param)
            .await?
            .data
            .into_iter()
            .take(1)
            .next();

        match book {
            Some(b) => {
                let update_param = UpdateBookParam {
                    book_id: b.id,
                    title: param.title.clone(),
                    description: param.description.clone(),
                };
                let result = self.book.update_book(&resource, &update_param).await?;

                resource.commit().await?;

                Ok(super::BookModel {
                    id: result.id,
                    title: result.title,
                    description: result.description,
                    owner_account_id: result.owner_account_id,
                })
            }
            None => Err(AppError::Generic(
                "You are not allowed to insert a section to that book.".to_string(),
            )),
        }
    }

    #[tracing::instrument(skip(self))]
    async fn update_section(
        &self,
        param: &super::UpdateSectionParam,
    ) -> crate::common::error::AppResult<super::SectionModel> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let get_param = GetBookListParam {
            pagination: Pagination { size: 1, number: 0 },
            id: Some(param.book_id),
            owner_account_id: Some(claim.sub),
            title: None,
        };
        let book = self.book.get_book_list(&resource, &get_param).await?;

        if book.data.len().eq(&0) {
            Err(AppError::Generic(
                "You are not allowed to insert a section to that book.".to_string(),
            ))
        } else {
            let get_section_param = crate::repository::book::GetSectionListParam {
                pagination: Pagination { size: 1, number: 0 },
                book_id: Some(param.book_id),
                section_id: Some(param.section_id),
                title: None,
                draft: None,
            };
            let section = self
                .book
                .get_section_list(&resource, &get_section_param)
                .await?
                .data
                .into_iter()
                .take(1)
                .next();

            match section {
                Some(s) => {
                    let update_param = UpdateSectionParam {
                        section_id: s.id,
                        title: param.title.clone(),
                        content: param.content.clone(),
                        is_draft: param.is_draft,
                    };
                    let result = self.book.update_section(&resource, &update_param).await?;

                    resource.commit().await?;

                    Ok(super::SectionModel {
                        id: result.id,
                        owner_book_id: result.owner_book_id,
                        title: result.title,
                        content: result.content,
                        is_draft: result.is_draft,
                        created_at: result.created_at,
                        updated_at: result.updated_at,
                        like_count: s.like_count,
                    })
                }
                None => Err(AppError::Generic(
                    "Section with that id doesn't exist.".to_string(),
                )),
            }
        }
    }

    #[tracing::instrument(skip(self))]
    async fn delete_section(
        &self,
        param: &super::DeleteSectionParam,
    ) -> crate::common::error::AppResult<()> {
        param.validate()?;
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let get_section_param = GetSectionListParam {
            pagination: Pagination { size: 1, number: 0 },
            book_id: None,
            section_id: Some(param.section_id),
            title: None,
            draft: None,
        };
        let section = self
            .book
            .get_section_list(&resource, &get_section_param)
            .await?
            .data
            .into_iter()
            .next()
            .ok_or_else(|| {
                AppError::Generic("Section that you want to delete is not found.".to_string())
            })?;

        // Get Book from section
        // Search by owner_account_id to make sure we're looking at the same
        // Validate that this book is owned by this user
        let get_book_param = GetBookListParam {
            pagination: Pagination { size: 1, number: 0 },
            id: Some(section.owner_book_id),
            owner_account_id: Some(claim.sub),
            title: None,
        };
        let _ = self
            .book
            .get_book_list(&resource, &get_book_param)
            .await?
            .data
            .into_iter()
            .next()
            .ok_or_else(|| {
                AppError::Generic(
                    "You don't have a permission to delete the section for this book.".to_string(),
                )
            })?;

        // Delete section
        let delete_section_param = DeleteSectionParam {
            section_id: param.section_id,
        };
        let _ = self
            .book
            .delete_section(&resource, &delete_section_param)
            .await?;

        resource.commit().await?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn like_section(
        &self,
        param: &super::LikeSectionParam,
    ) -> crate::common::error::AppResult<()> {
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;
        let tx = Tx::begin_transaction(&self.connection).await?;

        // check whether the section exist

        // check whether a section is being liked with like_account
        let repo_param = GetLikeAccountParam {
            pagination: Pagination { size: 1, number: 0 },
            account_id: Some(claim.sub),
            section_id: Some(param.section_id),
        };
        let like_account = self.like_account.get_like_account(&tx, &repo_param).await?;
        let is_liked = like_account.total > 0;

        match (param.is_like, is_liked) {
            // CASE: if user want to like but there's no record that user have liked this section before, then,
            // - Add a "marker" that this user like this post (insert to like_account)
            // - Increment the like count
            (true, false) => {
                let repo_param = InsertLikeAccountParam {
                    account_id: claim.sub,
                    section_id: param.section_id,
                };
                let _ = self
                    .like_account
                    .insert_like_account(&tx, &repo_param)
                    .await?;

                // increment section_like
                // get the current and then set it to n+1
                let repo_param = GetSectionLikeParam {
                    pagination: Pagination { size: 1, number: 0 },
                    section_id: Some(param.section_id),
                };
                let section_like = self
                    .section_like
                    .get_section_like(&tx, &repo_param)
                    .await?
                    .data
                    .into_iter()
                    .next()
                    .ok_or_else(|| {
                        AppError::Generic(
                            "Shouldn't happen because section should always have one section_like"
                                .into(),
                        )
                    })?;

                let repo_param = UpdateSectionLikeParam {
                    section_id: param.section_id,
                    like_count: section_like.like_count + 1,
                };
                let _ = self
                    .section_like
                    .update_section_like(&tx, &repo_param)
                    .await?;
            }

            // CASE: if user want to unlike, yet currently it's being liked, then,
            // - remove the "marker" that this user like this section
            // - decrement the like count
            (false, true) => {
                let repo_param = DeleteLikeAccountParam {
                    account_id: claim.sub,
                    section_id: param.section_id,
                };
                let _ = self
                    .like_account
                    .delete_like_account(&tx, &repo_param)
                    .await?;

                // decrement section_like
                // get the current and then set it to n-1
                let repo_param = GetSectionLikeParam {
                    pagination: Pagination { size: 1, number: 0 },
                    section_id: Some(param.section_id),
                };
                let section_like = self
                    .section_like
                    .get_section_like(&tx, &repo_param)
                    .await?
                    .data
                    .into_iter()
                    .next()
                    .ok_or_else(|| {
                        AppError::Generic(
                            "Shouldn't happen because section should always have one section_like"
                                .into(),
                        )
                    })?;

                let repo_param = UpdateSectionLikeParam {
                    section_id: param.section_id,
                    like_count: section_like.like_count - 1,
                };
                let _ = self
                    .section_like
                    .update_section_like(&tx, &repo_param)
                    .await?;
            }

            // If the operation is already being done, do nothing
            // E.G. If a user wants to like an already liked section, then it should do nothing.
            _ => (),
        }

        tx.commit().await?;
        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn get_like_section(
        &self,
        param: &super::GetLikeSectionParam,
    ) -> crate::common::error::AppResult<super::GetLikeSectionResult> {
        let resource = Tx::begin_transaction(&self.connection).await?;
        let claim = param.header.claim.as_ref().ok_or_else(|| AppError::Generic("This user doesn't have a permission, contact the administrator for further assistance.".to_string()))?;

        let r_param = GetLikeAccountParam {
            pagination: Pagination { size: 1, number: 0 },
            account_id: Some(claim.sub),
            section_id: Some(param.section_id),
        };
        let result = self
            .like_account
            .get_like_account(&resource, &r_param)
            .await?;

        let is_like = result.total > 0;

        Ok(super::GetLikeSectionResult { is_like })
    }
}
