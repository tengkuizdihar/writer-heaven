use crate::common::error::AppResult;
use sea_orm::{Database, DatabaseConnection, DatabaseTransaction, TransactionTrait};

/// This is a long living struct used to store connection to services such as databases.
#[derive(Debug)]
pub struct Connection {
    pub conn: DatabaseConnection,
}

impl Connection {
    pub fn custom_clone(&self) -> Self {
        match &self.conn {
            DatabaseConnection::SqlxPostgresPoolConnection(c) => Connection {
                conn: DatabaseConnection::SqlxPostgresPoolConnection(c.clone()),
            },
            DatabaseConnection::MockDatabaseConnection(c) => Connection {
                conn: DatabaseConnection::MockDatabaseConnection(c.clone()),
            },
            DatabaseConnection::Disconnected => {
                panic!("It's disconnected, thankfully this happens on initialization, usually.")
            }
        }
    }

    #[cfg(test)]
    pub fn mock() -> Self {
        Connection {
            conn: sea_orm::MockDatabase::new(sea_orm::DatabaseBackend::Postgres).into_connection(),
        }
    }
}

/// This is a short living struct only used to interact with repository because they are very close
/// to the implementation of the other services. This struct may have reference to connection in Connection in the future.
#[derive(Debug)]
pub struct Tx {
    pub tx: DatabaseTransaction,
}

impl Connection {
    pub async fn new(database_uri: &str) -> Self {
        Self {
            conn: Database::connect(database_uri)
                .await
                .expect("Unable to connect to database"),
        }
    }

    pub fn conn(&self) -> &DatabaseConnection {
        &self.conn
    }
}

/// Tx is short for transaction, why does it need to be its own struct?
///  ¯\_(ツ)_/¯
impl Tx {
    pub async fn new(txn: DatabaseTransaction) -> Self {
        Self { tx: txn }
    }

    pub async fn begin_transaction(connection: &Connection) -> AppResult<Self> {
        Ok(Tx::new(connection.conn().begin().await?).await)
    }

    pub fn tx(&self) -> &DatabaseTransaction {
        &self.tx
    }

    pub async fn commit(self) -> AppResult<()> {
        Ok(self.tx.commit().await?)
    }
}
