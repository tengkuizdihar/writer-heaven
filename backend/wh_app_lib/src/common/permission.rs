use std::cmp::Ordering;

use crate::common::error::AppError;

use super::error::AppResult;
use bitvec::prelude::*;

pub const PERM_CONT_BIT_SIZE: usize = 1024;
pub const PERM_CONT_BYTE_SIZE: usize = PERM_CONT_BIT_SIZE / 8;

/// The container used as permission flag.
/// It's possible to store up to 1024 kinds of permission.
/// If that's not scalable enough, you could go to the BitVec route.
pub type PermissionContainer = BitArr!(for PERM_CONT_BIT_SIZE, in u8);

/// This is the permission enum used for authorizing an account's action.
/// Must always remember to not exceed (2**32)-1 because it's based on usize and most computers could still use 32 bit arch.
/// Developer must be always aware that the value of 0 is a huge no-no. Begin it with one please.
/// Please update `role_to_permission` after changing this enum.
#[derive(Debug, Clone, Copy)]
pub enum Permission {
    /// Used for usecases that has the ability to get Personally Identified Information for an account.
    /// For example, an admin will have this permission but regular user will not, and could only get their own account.
    GetAccountOtherMember = 1,

    /// Granting the ability for the user to create a user with other roles such as Admin.
    CreateUserWithAnyRole = 2,
}

#[tracing::instrument(skip_all)]
pub fn toggle_permission(permission: Permission, permission_flags: &mut PermissionContainer) {
    let p = permission as usize;
    let reversed_value = permission_flags.get(p).map(|f| *f.as_ref()).map(|f| !f);

    if let Some(v) = reversed_value {
        permission_flags.set(p, v);
    }
}

#[tracing::instrument(skip_all)]
pub fn set_permission(
    permission: Permission,
    value: bool,
    permission_flags: &mut PermissionContainer,
) {
    permission_flags.set(permission as usize, value)
}

#[tracing::instrument(skip_all)]
pub fn set_permissions(
    permission: &[Permission],
    value: bool,
    permission_flags: &mut PermissionContainer,
) {
    for p in permission {
        permission_flags.set(*p as usize, value)
    }
}

#[tracing::instrument(skip_all)]
pub fn check_permission(permission: Permission, permission_flags: &PermissionContainer) -> bool {
    permission_flags
        .get(permission as usize)
        .map(|f| *f.as_ref())
        .unwrap_or_default()
}

#[tracing::instrument(skip_all)]
pub fn serialize_permission(permission_flags: &PermissionContainer) -> String {
    permission_flags
        .as_raw_slice()
        .iter()
        .map(|f| *f as char)
        .collect()
}

#[tracing::instrument(skip_all)]
pub fn try_deserialize_permission<T>(raw_permission: T) -> AppResult<PermissionContainer>
where
    T: std::fmt::Debug + AsRef<[u8]>,
{
    let mut new = PermissionContainer::new([0; PERM_CONT_BYTE_SIZE]);
    let raw_slice = raw_permission.as_bits::<Lsb0>();

    match raw_slice.len().cmp(&new.len()) {
        Ordering::Less => {
            // TODO-PERFORMANCE: is this slow? Possibly. Do I care? Not yet.
            for (index, br) in raw_slice.iter().enumerate() {
                new.set(index, *br);
            }
            Ok(new)
        }
        Ordering::Equal => {
            new.copy_from_bitslice(raw_slice);
            Ok(new)
        }
        Ordering::Greater => Err(AppError::DeserializingPermission(
            "raw_permission have greater length than the current permission's length".to_string(),
        )),
    }
}

/// This is the Role enum only used internally to set what kind of a permission a user could do.
/// The account might have permission that's not tied to any Role.
#[derive(Debug, Clone, Copy)]
pub enum Role {
    Admin,
    Regular,
}

pub fn role_to_permission(role: Role) -> PermissionContainer {
    let mut container = PermissionContainer::new([0; PERM_CONT_BYTE_SIZE]);
    match role {
        Role::Admin => {
            set_permissions(
                &[
                    Permission::GetAccountOtherMember,
                    Permission::CreateUserWithAnyRole,
                ],
                true,
                &mut container,
            );
            container
        }
        Role::Regular => container,
    }
}

#[cfg(test)]
mod testing {
    use crate::common::permission::{try_deserialize_permission, PERM_CONT_BYTE_SIZE};

    use super::{set_permission, Permission, PermissionContainer};

    use pretty_assertions::assert_eq;

    #[test]
    pub fn permission_sanity_check() {
        let mut good_permission = PermissionContainer::new([0; PERM_CONT_BYTE_SIZE]);
        set_permission(
            Permission::GetAccountOtherMember,
            true,
            &mut good_permission,
        );

        // Although, semantically speaking, zeroth element is not used because I want to make sure nothing is null.
        // Just to be safe, even though this is rust.
        let mut bad_permission = PermissionContainer::new([0; PERM_CONT_BYTE_SIZE]);
        bad_permission.set(0, true);

        let mut want_permission = [0; PERM_CONT_BYTE_SIZE];
        want_permission[0] = 0b00000010;
        let want_permission = PermissionContainer::new(want_permission);

        let mut short_permission = [0; PERM_CONT_BYTE_SIZE];
        short_permission[0] = 0b01000001_u8; // The letter "A"
        let short_permission = PermissionContainer::new(short_permission);

        // Init the strings
        let want_permission_string: String = want_permission
            .as_raw_slice()
            .iter()
            .map(|f| *f as char)
            .collect();
        let good_permission_string: String = good_permission
            .as_raw_slice()
            .iter()
            .map(|f| *f as char)
            .collect();
        let bad_permission_string: String = bad_permission
            .as_raw_slice()
            .iter()
            .map(|f| *f as char)
            .collect();

        // Try deserialize
        let good_deserialize = try_deserialize_permission(good_permission_string.clone()).unwrap();
        let short_deserialize = try_deserialize_permission("A".to_string()).unwrap();

        // Assert it all
        assert_eq!(want_permission, good_permission);
        assert_eq!(want_permission, good_deserialize);
        assert_eq!(short_deserialize, short_permission);
        assert_ne!(want_permission, bad_permission);
        assert_eq!(want_permission_string, good_permission_string);
        assert_ne!(want_permission_string, bad_permission_string);
        assert_ne!(want_permission_string, bad_permission_string);
    }
}
