tonic::include_proto!("wh");

pub const FILE_DESCRIPTOR_SET: &[u8] =
    tonic::include_file_descriptor_set!("writer_heaven_descriptor");

pub mod util {
    use paste::paste;
    macro_rules! convert_to_option {
        ($x:tt) => {
            paste! {
                /// A middleware kind of function to act as a "null" checker for $x in protobuf
                #[allow(dead_code)]
                pub fn [<convert_ $x _option>](i: $x) -> Option<$x> {
                    match i {
                        0 => None,
                        v => Some(v),
                    }
                }
            }
        };
    }

    convert_to_option!(i64);
    convert_to_option!(i32);
    convert_to_option!(u64);
    convert_to_option!(u32);

    /// A middleware kind of function to act as a "null" checker for string in protobuf
    pub fn convert_string_option(i: String) -> Option<String> {
        match i.as_str() {
            "" => None,
            _ => Some(i),
        }
    }
}
