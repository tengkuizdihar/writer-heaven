use std::fmt::Debug;

use super::alias::BigSerial;
use super::error::AppResult;
use crate::common::error::AppError;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, TokenData, Validation};
use pbkdf2::password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString};
use pbkdf2::Pbkdf2;
use rand_core::OsRng;
use serde::{Deserialize, Serialize};

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
pub enum AuthenticationResult {
    Allow,
    Disallow,
}

pub struct Secret<T>(pub T);

impl<T> Debug for Secret<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Secret")
            .field(&"Secret Value Not Meant For Prying Eyes")
            .finish()
    }
}

/// WARNING! This is an expensive function and should be used with a tokio::task::spawn_blocking
/// Verify a password against a password hash, typically from database.
#[tracing::instrument(skip_all)]
pub fn salted_hash(plaintext: impl AsRef<[u8]> + Debug) -> AppResult<String> {
    let salt = SaltString::generate(&mut OsRng);
    match Pbkdf2.hash_password(plaintext.as_ref(), &salt) {
        Ok(p) => Ok(p.to_string()),
        Err(e) => {
            tracing::error!("Password hashing error! {:?}", e);
            Err(AppError::Generic("Password hashing error!".to_string()))
        }
    }
}

/// WARNING! This is an expensive function and should be used with a tokio::task::spawn_blocking
/// Verify a password against a password hash, typically from database.
pub fn verify_password(
    plaintext: impl AsRef<[u8]>,
    password_hash: impl AsRef<str>,
) -> AppResult<AuthenticationResult> {
    let hashed_pass = match PasswordHash::new(password_hash.as_ref()) {
        Ok(it) => it,
        Err(_) => {
            tracing::error!("Parsing oassword hash failed!");
            return Err(AppError::Generic(
                "Parsing oassword hash failed!".to_string(),
            ));
        }
    };

    match Pbkdf2.verify_password(plaintext.as_ref(), &hashed_pass) {
        Ok(_) => Ok(AuthenticationResult::Allow),
        Err(e) => match e {
            pbkdf2::password_hash::Error::Password => Ok(AuthenticationResult::Disallow),
            _ => {
                tracing::error!("Password hashing error! {}", e);
                Err(AppError::Generic("Failed processing password!".to_string()))
            }
        },
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct JwtClaims {
    /// Required (validate_exp defaults to true in validation). Expiration time (as UTC timestamp)
    pub exp: i64,
    /// Optional. Subject (whom token refers to)
    pub sub: BigSerial,
    /// Optional. Salt that's usually filled with UUID and should usually be skipped when validating claims
    pub slt: String,
}

#[tracing::instrument(skip_all)]
pub fn jwt_encode(sub: BigSerial, secret: impl AsRef<[u8]>) -> AppResult<String> {
    let exp = (Utc::now() + Duration::weeks(53)).timestamp();
    let claims = JwtClaims {
        exp,
        sub,
        slt: uuid::Uuid::new_v4().to_string(),
    };

    Ok(encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_ref()),
    )?)
}

#[tracing::instrument(skip_all)]
pub fn jwt_decode(
    token: impl AsRef<str>,
    secret: impl AsRef<[u8]>,
) -> AppResult<TokenData<JwtClaims>> {
    Ok(decode(
        token.as_ref(),
        &DecodingKey::from_secret(secret.as_ref()),
        &Validation::default(),
    )?)
}

#[cfg(test)]
mod testing {
    use super::{jwt_encode, salted_hash, verify_password};
    use crate::common::crypto::{jwt_decode, AuthenticationResult};
    use pretty_assertions::assert_eq;

    #[test]
    pub fn hashing_and_verifying() {
        let good_password = "goodpass$@$@12351".to_owned();
        let bad_password = "I'm a bad guy".to_owned();

        let hashed_pass =
            salted_hash(&good_password).expect("Salted hash should've hash this without problem");

        let verify_good = verify_password(&good_password, &hashed_pass)
            .expect("Verify hash should've verified this without problem");
        let verify_bad = verify_password(&bad_password, &hashed_pass)
            .expect("Verify hash should've verified this without problem");

        assert_eq!(AuthenticationResult::Allow, verify_good);
        assert_eq!(AuthenticationResult::Disallow, verify_bad);
    }

    #[test]
    pub fn jwt_test() {
        let good_subject = 123;
        let good_secret = "secret".to_string();
        let bad_secret = "not secret".to_string();

        let encoded = jwt_encode(good_subject, &good_secret).unwrap();
        let second_encoded = jwt_encode(good_subject, &good_secret).unwrap();

        let _ = jwt_decode(&encoded, good_secret).unwrap();
        let _ = jwt_decode(&encoded, bad_secret).unwrap_err();

        assert_ne!(encoded, second_encoded);
    }
}
