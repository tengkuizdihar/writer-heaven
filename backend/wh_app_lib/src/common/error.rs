use sea_orm::DbErr;
use thiserror::Error;
use tokio::task::JoinError;
use tonic::Status;

#[allow(dead_code)]
pub type AppResult<T> = Result<T, AppError>;

#[derive(Error, Debug)]
#[allow(dead_code)]
pub enum AppError {
    #[error("Internal Database Error")]
    Database(#[from] DbErr),
    #[error("Error: {0}")]
    Join(#[from] JoinError),
    #[error("JWT Error")]
    Jwt(#[from] jsonwebtoken::errors::Error),
    #[error("Error: {0}")]
    Generic(String),
    #[error("Failed Deserializing Permission: {0}")]
    DeserializingPermission(String),
    #[error("{0}")]
    Validation(#[from] validator::ValidationErrors),
}

impl From<AppError> for Status {
    #[tracing::instrument]
    fn from(val: AppError) -> Self {
        match val {
            AppError::Database(err) => match err {
                DbErr::Conn(s) => {
                    tracing::error!("DThere was a problem with the database connection: {}", s);
                    Status::internal("DThere was a problem with the database connection")
                }
                DbErr::Exec(s) => {
                    tracing::error!("DB Operation Did Not Execute Properly: {}", s);
                    Status::internal("DB Operation Did Not Execute Properly")
                }
                DbErr::Query(s) => {
                    tracing::error!("DB Error Query: {}", s);
                    Status::internal("DB Error Query")
                }
                DbErr::RecordNotFound(_) => Status::not_found("Entity not found in database."),
                DbErr::Custom(s) => {
                    tracing::error!("DB Error Custom: {}", s);
                    Status::internal("DB Error Custom")
                }
                DbErr::Type(s) => {
                    tracing::error!("Error occurred while parsing value as target type: {}", s);
                    Status::internal("Error occurred while parsing value as target type.")
                }
                DbErr::Json(s) => {
                    tracing::error!("Error parsing JSON to database: {}", s);
                    Status::internal("Error parsing JSON to database.")
                }
                DbErr::Migration(s) => {
                    tracing::error!("A Migration Error In Runtime, How: {}", s);
                    Status::internal("A Migration Error In Runtime, How.")
                }
            },
            AppError::Generic(s) => Status::cancelled(s),
            AppError::Join(_) => Status::aborted("Tokio Task Join Error"),
            AppError::Jwt(e) => {
                tracing::error!("Failed to process JWT {}", e);
                Status::aborted("Failed to process JWT")
            }
            AppError::DeserializingPermission(e) => {
                tracing::error!("Failed Deserializing Permission {}", e);
                Status::aborted("Failed Deserializing Permission")
            }
            AppError::Validation(e) => {
                tracing::error!("Failed Validating {}", e);
                match serde_json::to_string(&e) {
                    Ok(e) => Status::invalid_argument(e),
                    Err(_) => Status::aborted("Invalid serde to json, how the hell."),
                }
            }
        }
    }
}
