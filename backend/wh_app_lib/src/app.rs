use tonic::Status;

use crate::{
    common::crypto::{jwt_decode, JwtClaims},
    repository::{
        account::AccountRepositoryImpl, book::BookRepositoryImpl,
        like_account::LikeAccountRepositoryImpl, note::NoteRepositoryImpl,
        permission::PermissionRepositoryImpl, section_like::SectionLikeRepositoryImpl,
        tag::TagRepositoryImpl,
    },
    resource::Connection,
    usecase::{
        account::{AccountUsecase, AccountUsecaseImpl},
        book::{BookUsecase, BookUsecaseImpl},
        note::{NoteUsecase, NoteUsecaseImpl},
    },
    ServerConfig,
};
use std::sync::Arc;

pub struct App {
    pub account: Box<dyn AccountUsecase + Send + Sync>,
    pub book: Box<dyn BookUsecase + Send + Sync>,
    pub note: Box<dyn NoteUsecase + Send + Sync>,
    pub config: Arc<ServerConfig>,
}

impl App {
    pub async fn new(config: &ServerConfig) -> Self {
        let connection = Connection::new(config.database_uri.as_ref()).await;
        Self {
            account: Box::new(AccountUsecaseImpl {
                connection: connection.custom_clone(),
                account: Box::new(AccountRepositoryImpl {}),
                permission: Box::new(PermissionRepositoryImpl {}),
            }),
            book: Box::new(BookUsecaseImpl {
                connection: connection.custom_clone(),
                book: Box::new(BookRepositoryImpl {}),
                section_like: Box::new(SectionLikeRepositoryImpl {}),
                like_account: Box::new(LikeAccountRepositoryImpl {}),
            }),
            note: Box::new(NoteUsecaseImpl {
                connection: connection.custom_clone(),
                note: Box::new(NoteRepositoryImpl {}),
                tag: Box::new(TagRepositoryImpl {}),
            }),
            config: Arc::new(config.clone()),
        }
    }

    #[cfg(test)]
    pub fn mock(config: &ServerConfig) -> Self {
        Self {
            account: Box::new(crate::usecase::account::MockAccountUsecase::new()),
            book: Box::new(crate::usecase::book::MockBookUsecase::new()),
            note: Box::new(crate::usecase::note::MockNoteUsecase::new()),
            config: Arc::new(config.clone()),
        }
    }

    pub fn extract_jwt_claim(
        &self,
        some_header: Option<crate::common::proto::Header>,
    ) -> Result<Option<JwtClaims>, Status> {
        some_header
            .map(|f| {
                jwt_decode(f.jwt, self.config.secret_key.as_str())
                    .ok()
                    .map(|f| f.claims)
            })
            .ok_or_else(|| Status::not_found("Need header in GRPC Request."))
    }
}
