use crate::common::proto::FILE_DESCRIPTOR_SET;
use crate::handler::WriterHeavenHandler;
use crate::{
    common::proto::writer_heaven_server::WriterHeavenServer,
    usecase::account::InsertAdminAccountParam,
};
use std::{future::Future, net::SocketAddr};
use tonic::transport::Server;

#[cfg(test)]
#[macro_use]
extern crate mockall;

pub(crate) mod app;
pub(crate) mod common;
pub(crate) mod handler;
pub(crate) mod repository;
pub(crate) mod resource;
pub(crate) mod usecase;

#[derive(Clone, Debug)]
pub struct ServerConfig {
    pub address: SocketAddr,
    pub tracing_level: tracing::Level,
    pub database_uri: String,
    pub secret_key: String,
}

impl ServerConfig {
    #[cfg(test)]
    pub fn mock() -> Self {
        use std::net::{IpAddr, Ipv4Addr};

        Self {
            address: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 50051),
            tracing_level: tracing::Level::DEBUG,
            database_uri: "mocking shouldn't use this".to_string(),
            secret_key: "supersecretmockingkey".to_string(),
        }
    }
}

pub async fn create_server(
    config: ServerConfig,
    admin_password: String,
) -> impl Future<Output = Result<(), tonic::transport::Error>> {
    // Setup Services
    let handler = WriterHeavenHandler::new(&config).await;
    let _ = handler
        .app
        .account
        .set_admin_account(&InsertAdminAccountParam {
            email: "admin@admin.com".to_string(),
            display_name: "admin".to_string(),
            password: admin_password,
        })
        .await
        .unwrap();

    let writer_heaven_service = WriterHeavenServer::new(handler);
    let grpc_reflection_service = tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(FILE_DESCRIPTOR_SET)
        .build()
        .unwrap();

    tracing::info!(message = "Starting server.", %config.address);
    Server::builder()
        .accept_http1(true)
        .trace_fn(|_| tracing::info_span!("Writer Heaven Server"))
        .add_service(tonic_web::enable(writer_heaven_service))
        .add_service(grpc_reflection_service)
        .serve(config.address)
}

pub mod reexport {
    pub use tracing;
}
