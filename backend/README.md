# WriterHeaven - Backend

- [WriterHeaven - Backend](#writerheaven---backend)
  - [Environment](#environment)
  - [Commands](#commands)

## Environment

For those who are using visual studio code, below are extension that's recommended for the development of this application.
* Nix Environment Selector by arrterian
* rust-analyzer by matklad
* Markdown All in One by yzhang
* vscode-proto3 by zxh404
* Error Lens by usernamehw
* crates by serayuzgur
* Code Spell Checker by streetsidesoftware


## Installing
Everytime you're on your first run or changes the nix files, please run this command.

```shell
nix-build shell.nix -A inputDerivation -o .nix-shell-inputs -vv && nix-shell shell.nix
```

This will create a .nix-shell-inputs which will persist the dependency of the project between GC. It will also make sure that when you delete the project, your dependency will also be GC because .nix-shell-inputs is deleted.
Subsequent nix-shell command shouldn't do this build phase as long as the nix files isn't changed. More information could be found here https://web.archive.org/web/20220708201311/https://ianthehenry.com/posts/how-to-learn-nix/saving-your-shell/.

## Commands
Below are some useful commands usually used for local development.
* Migrate fresh the database
  * Go to the root directory for backend (the folder this README is in right now).
  * `DATABASE_URL="postgres://postgres:postgres@localhost/writer_heaven" sea-orm-cli migrate fresh -d wh_app_migration`.
* Generate Entities From Database
  * Do this everytime migration changes.
  * `sea-orm-cli generate entity -o ./wh_app_migration/src/entity -u postgres://postgres:postgres@localhost/writer_heaven --expanded-format`

Below are some makefiles related commands
* Test the whole application.
  * `make test`
* This will clean all build artifact, basically deleting `./target`.
  * `make clean`
* Compile the documentation and open it in the browser.
  * `make doc`
* Open the browser and connect to the default local instance of the application. Should be used after running the application.
  * `make grpcui`
* Create a coverage file for the entire project. User of VSCode could view this by searching a command for `Coverage Gutters` through the `ctrl+shift+p` menu.
  * `make coverage`