use sea_orm_migration::prelude::*;
use wh_app_migration::Migrator;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    cli::run_cli(Migrator).await;
}
