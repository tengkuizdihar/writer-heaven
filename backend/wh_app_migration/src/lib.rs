pub use sea_orm_migration::prelude::*;

pub mod entity;

mod m20220101_000001_create_table;
mod m20220101_000002_create_permission;
mod m20220101_000003_create_book;
mod m20220101_000004_create_section;
mod m20220101_000005_create_note;
mod m20220101_000006_create_tag;
mod m20220101_000007_create_section_like;
mod m20220101_000008_create_like_account;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_table::Migration),
            Box::new(m20220101_000002_create_permission::Migration),
            Box::new(m20220101_000003_create_book::Migration),
            Box::new(m20220101_000004_create_section::Migration),
            Box::new(m20220101_000005_create_note::Migration),
            Box::new(m20220101_000006_create_tag::Migration),
            Box::new(m20220101_000007_create_section_like::Migration),
            Box::new(m20220101_000008_create_like_account::Migration),
        ]
    }
}
