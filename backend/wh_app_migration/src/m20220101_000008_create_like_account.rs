use sea_orm_migration::{
    prelude::*,
    sea_orm::{ConnectionTrait, Statement},
};

pub struct Migration;

impl MigrationName for Migration {
    fn name(&self) -> &str {
        "m20220101_000008_create_like_account"
    }
}

const UP: &str = "
CREATE TABLE like_account (
  owner_section_id BIGINT NOT NULL,
  owner_account_id BIGINT NOT NULL,

  FOREIGN KEY(owner_section_id)
    REFERENCES section(id)
    ON DELETE CASCADE,

  FOREIGN KEY(owner_account_id)
    REFERENCES account(id)
    ON DELETE CASCADE,

  PRIMARY KEY (owner_section_id, owner_account_id)
);
";

const DOWN: &str = "
DROP TABLE IF EXISTS like_account;
";

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let sql = UP;
        let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
        manager.get_connection().execute(stmt).await.map(|_| ())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let sql = DOWN;
        let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
        manager.get_connection().execute(stmt).await.map(|_| ())
    }
}
