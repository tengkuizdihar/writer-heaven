let
  d = import ./nix/dependencies.nix { };
  pkgs = d.pkgs;
in
pkgs.rustPlatform.buildRustPackage rec {
  pname = "writer-heaven";
  version = "0.0.1";

  src = ./.;

  nativeBuildInputs = d.nativeBuildInputs;

  cargoSha256 = "0ldb2x7c9690p0fvk5qgcwb94wqfbvpjfs22vn8iqbc7z8d7anr0";

  # Disable because PgSQL service is needed to test properly
  doCheck = false;

  meta = with pkgs.lib; {
    description = "Heaven for writers.";
    homepage = "https://gitlab.com/tengkuizdihar/writer-heaven";
    license = licenses.agpl3Only;
    maintainers = [ maintainers.tengkuizdihar ];
  };
}
