let
  d = import ./nix/dependencies.nix { };
  pkgs = d.pkgs;
in
pkgs.clangStdenv.mkDerivation {
  name = "clang-nix-shell";
  nativeBuildInputs = d.shellInputs ++ d.nativeBuildInputs;
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
