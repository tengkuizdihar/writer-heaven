// use opentelemetry::sdk::propagation::TraceContextPropagator;
// use tracing_subscriber::prelude::*;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use wh_app_lib::{reexport::tracing, ServerConfig};

const ENV_DATABASE_URL: &str = "DATABASE_URL";
const ENV_SECRET_KEY: &str = "SECRET_KEY";
const ENV_SUPERADMIN_PASSWORD: &str = "SUPERADMIN_PASSWORD";
// const APP_NAME: &str = "WRITER_HEAVEN";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();

    let config = ServerConfig {
        address: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 50051),
        tracing_level: tracing::Level::WARN,

        database_uri: std::env::var(ENV_DATABASE_URL)
            .unwrap_or_else(|_| panic!("{} need to be set!", ENV_DATABASE_URL)),

        secret_key: std::env::var(ENV_SECRET_KEY)
            .unwrap_or_else(|_| panic!("{} need to be set!", ENV_SECRET_KEY)),
    };

    // Install an opentelemetry (otel) pipeline with a simple span processor that exports data one at a time when
    // spans end. See the `install_batch` option on each exporter's pipeline builder to see how to
    // export in batches.
    //
    // Commented for now because the terminal tracing subscriber is good for debugging.
    //
    // opentelemetry::global::set_text_map_propagator(TraceContextPropagator::new());
    // let tracer = opentelemetry_jaeger::new_pipeline()
    //     .with_service_name(APP_NAME)
    //     .install_simple()?;

    // let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    // tracing_subscriber::registry()
    //     .with(opentelemetry)
    //     .try_init()?;

    // Setup Tracing
    // Comment this if you want to activate the opentelemetry one.
    // In the future, DEBUG will automatically use this and PRODUCTION will automatically use opentelemetry.
    tracing_subscriber::fmt()
        .with_max_level(config.tracing_level)
        .init();

    // Create and run the server
    let admin_password = std::env::var(ENV_SUPERADMIN_PASSWORD)
        .unwrap_or_else(|_| panic!("{} need to be set!", ENV_SUPERADMIN_PASSWORD));
    let grpc_server = wh_app_lib::create_server(config, admin_password).await;
    grpc_server.await?;

    opentelemetry::global::shutdown_tracer_provider();
    Ok(())
}
