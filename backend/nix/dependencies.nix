{ pkgs ? import (fetchTarball ("https://github.com/NixOS/nixpkgs/archive/f419dc5763c2b3c5580e396dea065b6d8b58ee27.tar.gz")) { } }:
let
  sea-orm-cli = pkgs.callPackage ./sea-orm-cli.nix {
    lib = pkgs.lib;
    rustPlatform = pkgs.rustPlatform;
    fetchCrate = pkgs.fetchCrate;
    installShellFiles = pkgs.installShellFiles;
    pkg-config = pkgs.pkg-config;
    openssl = pkgs.openssl;
    stdenv = pkgs.stdenv;
    Security = pkgs.Security;
    libiconv = pkgs.libiconv;
    sqlite = pkgs.sqlite;
    postgresql = pkgs.postgresql;
    mariadb = pkgs.mariadb;
    zlib = pkgs.zlib;
    postgresqlSupport = true;
  };
in
{
  inherit pkgs;

  nativeBuildInputs = [
    pkgs.cmake
    pkgs.pkgconfig
    pkgs.postgresql
    pkgs.protobuf
  ];

  shellInputs = [
    pkgs.bc
    pkgs.cargo
    pkgs.cargo-tarpaulin
    pkgs.cargo-watch
    pkgs.clippy
    pkgs.grpcui
    pkgs.k6
    pkgs.rustc
    pkgs.rustfmt
    sea-orm-cli
  ];
}
