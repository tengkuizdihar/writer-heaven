{ lib
, sqliteSupport ? false
, postgresqlSupport ? false
, mysqlSupport ? false
, rustPlatform
, fetchCrate
, installShellFiles
, pkg-config
, openssl
, stdenv
, Security
, libiconv
, sqlite
, postgresql
, mariadb
, zlib
}:

let
  inherit (lib) optional optionals optionalString;
in

rustPlatform.buildRustPackage rec {
  pname = "sea-orm-cli";
  version = "0.8.1";

  src = fetchCrate {
    inherit version;
    crateName = "sea-orm-cli";
    sha256 = "07bc2w1xv7n2kncn6yr7afdwrafm5aqgr8pv4kgw1jqvypcyig61";
  };

  cargoSha256 = "0afilgcb7rhz2h1f4l7vw11cwsg6s5r4idvnqdras6l3x8zgk71y";

  nativeBuildInputs = [ installShellFiles pkg-config ];

  buildInputs = [ openssl ]
    ++ optional stdenv.isDarwin Security
    ++ optional (stdenv.isDarwin && mysqlSupport) libiconv;
  # ++ optional sqliteSupport sqlite
  # ++ optional postgresqlSupport postgresql
  # ++ optionals mysqlSupport [ mariadb zlib ];

  postInstall = ''
    installShellCompletion --cmd sea-orm-cli \
      --bash <($out/bin/sea-orm-cli completions bash) \
      --fish <($out/bin/sea-orm-cli completions fish) \
      --zsh <($out/bin/sea-orm-cli completions zsh)
  '';

  # Fix the build with mariadb, which otherwise shows "error adding symbols:
  # DSO missing from command line" errors for libz and libssl.
  NIX_LDFLAGS = optionalString mysqlSupport "-lz -lssl -lcrypto";

  meta = with lib; {
    description = "Command line utility for SeaORM";
    homepage = "https://github.com/SeaQL/sea-orm/tree/master/sea-orm-cli";
    license = with licenses; [ mit ];
    maintainers = with maintainers; [ tengkuizdihar ];
    mainProgram = "sea-orm-cli";
  };
}
