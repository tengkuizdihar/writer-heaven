import { styled, TextField } from "@mui/material";

export const TransparentOnFocusTextField = styled(TextField)`
  & label.Mui-focused {
    color: white;
  }
  & .MuiOutlinedInput-root {
    &.Mui-focused fieldset {
      border-color: rgb(0, 0, 0, 0);
    }
  }
`;
