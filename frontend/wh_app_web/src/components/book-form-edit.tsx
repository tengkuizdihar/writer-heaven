import { Button, Grid, TextField } from "@mui/material";
import { FunctionalComponent, h } from "preact";
import { CSSProperties, useEffect, useState } from "preact/compat";
import { useSnackbar } from "notistack";
import { valueOnUpdate } from "../lib/lib";
import { WHC } from "../proto/global";
import { UpdateBookParam } from "../proto/writer_heaven_pb";
import { getConfiguredHeader } from "../lib/account";

interface BookFormEditProp {
  id: number;
  title: string;
  description: string;
  style?: CSSProperties;
  onUpdateSuccess: () => void;
  onCancelEditing: () => void;
}

const BookFormEdit: FunctionalComponent<BookFormEditProp> = (props) => {
  const { enqueueSnackbar } = useSnackbar();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const clearFormState = () => {
    setTitle("");
    setDescription("");
  };

  useEffect(() => {
    setTitle(props.title);
    setDescription(props.description);
  }, [props.title, props.description]);

  const onSubmit = async () => {
    const param = new UpdateBookParam();

    param.setHeader(getConfiguredHeader());
    param.setId(props.id);
    param.setTitle(title);
    param.setDescription(description);

    try {
      await WHC.updateBook(param, null);

      clearFormState();
      props.onUpdateSuccess();
      enqueueSnackbar("Successfully updated the book!", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  return (
    <Grid>
      <TextField
        label="Title"
        type="search"
        style={{ width: "100%", marginBottom: "0.5em" }}
        value={title}
        onChange={valueOnUpdate(setTitle)}
      />
      <TextField
        label="Description"
        type="search"
        multiline
        style={{ width: "100%", marginBottom: "0.5em" }}
        value={description}
        onChange={valueOnUpdate(setDescription)}
      />
      <Grid container item justifyContent="end">
        <Button color="error" onClick={props.onUpdateSuccess}>
          Cancel
        </Button>
        <Button color="success" onClick={onSubmit}>
          Update
        </Button>
      </Grid>
    </Grid>
  );
};

export { BookFormEdit, BookFormEditProp };
