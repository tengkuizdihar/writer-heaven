import { Stack, TextField } from "@mui/material";
import { useSnackbar } from "notistack";
import { Fragment, FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import { useState } from "preact/hooks";
import { getConfiguredHeader } from "../lib/account";
import { sf, valueOnUpdate } from "../lib/lib";
import { WHC } from "../proto/global";
import { InsertSectionParam } from "../proto/writer_heaven_pb";
import { ROUTE_BOOK } from "./app";
import { SectionFormAction } from "./section-form-action";
import { TransparentOnFocusTextField } from "./text-field";

// Will return section ID that's being saved
const DoSavingSection = async (
  title: string,
  content: string,
  bookId: number
): Promise<number> => {
  const SFDoSavingSectionKey = "DoSavingSection";
  return await sf.do(SFDoSavingSectionKey, async () => {
    const request = new InsertSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setBookId(bookId);
    request.setTitle(title);
    request.setContent(content);

    const lmao = await WHC.insertSection(request, null);

    return lmao.getId();
  });
};

export interface SectionFormCreateProps {
  bookId: number;
  onCancelEditing: () => void;
}

export const SectionFormCreate: FunctionalComponent<SectionFormCreateProps> = (
  props
) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  const handleSavingSection = async () => {
    try {
      const savedSectionId = await DoSavingSection(
        title,
        content,
        props.bookId
      );

      route(`${ROUTE_BOOK}/${props.bookId}/${savedSectionId}`);
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  const handleSaveShortcut = (event: KeyboardEvent) => {
    if ((event.ctrlKey || event.metaKey) && event.key === "s") {
      event.preventDefault();
      handleSavingSection();
    }
  };

  return (
    <Fragment>
      <Stack>
        <TextField
          fullWidth
          onKeyDown={handleSaveShortcut}
          label="Title"
          required
          style={{ marginBottom: "0.5rem" }}
          value={title}
          onChange={valueOnUpdate(setTitle)}
        />
        <TransparentOnFocusTextField
          label="Content"
          onKeyDown={handleSaveShortcut}
          minRows={10}
          multiline
          required
          style={{ marginBottom: "0.5rem" }}
          value={content}
          onChange={valueOnUpdate(setContent)}
        />
      </Stack>

      <SectionFormAction
        saveCallback={handleSavingSection}
        isShowingPublish={false}
        isShowingToggleEditting={true}
        isShowingDelete={false}
        isDraft={false}
        toggleEditting={props.onCancelEditing}
      />
    </Fragment>
  );
};
