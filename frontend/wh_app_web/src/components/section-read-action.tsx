import {
  Delete,
  Edit,
  Favorite,
  FavoriteBorder,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import { Fragment, FunctionalComponent, h } from "preact";
import { FloatingAction } from "./floating-action";

export interface SectionReadActionProps {
  editCallback?: () => void;
  deleteCallback?: () => void;
  publishCallback?: () => void;
  draftCallback?: () => void;
  likeCallback?: () => void;
  isDraft: boolean;
  isLiked: boolean;
}

export const SectionReadAction: FunctionalComponent<SectionReadActionProps> = (
  props
) => {
  return (
    <Fragment>
      <FloatingAction
        collapseButtonTooltipText="Actions"
        otherButton={[
          {
            onClick: props.likeCallback,
            icon: props.isLiked ? <Favorite /> : <FavoriteBorder />,
            tooltipText: props.isLiked ? "Unlove This" : "Clik To Love It",
            isVisible: true,
          },
        ]}
        childrenButtonList={[
          {
            onClick: props.editCallback,
            icon: <Edit />,
            tooltipText: "Edit",
            isVisible: true,
          },
          {
            onClick: props.deleteCallback,
            icon: <Delete />,
            tooltipText: "Delete",
            isVisible: true,
          },
          {
            onClick: props.publishCallback,
            icon: <VisibilityOff />,
            tooltipText: "Publish",
            isVisible: props.isDraft,
          },
          {
            onClick: props.draftCallback,
            icon: <Visibility />,
            tooltipText: "Draft",
            isVisible: !props.isDraft,
          },
        ]}
      />
    </Fragment>
  );
};
