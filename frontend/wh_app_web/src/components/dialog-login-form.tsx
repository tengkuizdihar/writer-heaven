import { Add, Login } from "@mui/icons-material";
import {
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  TextField,
  Tooltip,
} from "@mui/material";
import { useSnackbar } from "notistack";
import { Fragment, FunctionalComponent, h } from "preact";
import { useState } from "preact/hooks";
import { AccountPersistent } from "../lib/account";
import { buildPermissionDetail } from "../lib/permission";
import { WHC } from "../proto/global";
import {
  GetAccountParam,
  Header,
  LoginNativeAccountParam,
} from "../proto/writer_heaven_pb";

async function login(email: string, password: string) {
  const pbParam = new LoginNativeAccountParam();

  pbParam.setEmail(email);
  pbParam.setPassword(password);

  const reply = await WHC.loginNativeAccount(pbParam, null);
  AccountPersistent.set({
    jwt: reply.getJwt(),
    name: "Waiting Info...",
  });

  // Get user info
  const pbParamInfo = new GetAccountParam();
  const pbHeader = new Header();
  pbHeader.setJwt(reply.getJwt());
  pbParamInfo.setHeader(pbHeader);

  const replyInfo = await WHC.getAccount(pbParamInfo, null);
  AccountPersistent.set({
    ...AccountPersistent.get(),
    name: replyInfo.getDisplayName(),
    permissionDetail: buildPermissionDetail(replyInfo.getPermission_asU8()),
  });
}

export interface DialogLoginFormProps {
  setFormRegistration: () => void;
}

export const DialogLoginForm: FunctionalComponent<DialogLoginFormProps> = (
  props
) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  async function usecaseLogin(email: string, password: string) {
    try {
      await login(email, password);

      enqueueSnackbar("Successfully Signed In!", {
        variant: "success",
      });

      window.location.reload();
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  }

  return (
    <>
      <DialogContent>
        <DialogContentText>
          <b>Sign In</b> now and unlock all the features!
        </DialogContentText>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            usecaseLogin(email, password);
          }}
        >
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            label="Email"
            value={email}
            autoComplete="username"
            onChange={(e) => setEmail((e.target as HTMLInputElement)?.value)}
          />
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            label="Password"
            type="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => setPassword((e.target as HTMLInputElement)?.value)}
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Tooltip title="create user">
          <Button onClick={props.setFormRegistration}>
            <Add />
          </Button>
        </Tooltip>
        <Tooltip title="sign in">
          <Button
            variant="contained"
            onClick={() => usecaseLogin(email, password)}
          >
            <Login />
          </Button>
        </Tooltip>
      </DialogActions>
    </>
  );
};
