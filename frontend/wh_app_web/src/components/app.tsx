import { createRef, FunctionalComponent, h } from "preact";
import { Route, Router } from "preact-router";
import { useEffect } from "preact/hooks";
import { SnackbarKey, SnackbarProvider } from "notistack";
import { Button, createTheme, ThemeProvider } from "@mui/material";
import { Container } from "@mui/system";

import Home from "../routes/home";
import NotFoundPage from "../routes/notfound";
import AccountPage from "../routes/account";

import { HelloRequest } from "../proto/writer_heaven_pb";
import { WHC } from "../proto/global";

import { HeaderComponent } from "./header";
import { themeOptions } from "./theme";
import Book from "../routes/book";
import Section from "../routes/section";

const theme = createTheme(themeOptions);

export const ROUTE_ACCOUNT = "/account";
export const ROUTE_BOOK = "/book";
export const ROUTE_HOME = "/";
export const ROUTE_NOTFOUND = "/";

const App: FunctionalComponent = () => {
  useEffect(() => {
    async function effect(): Promise<void> {
      const request = new HelloRequest();
      request.setName("PING");

      const reply = await WHC.sayHello(request, null);
      console.log(reply.getMessage());
    }

    effect();
  });

  // add action to all snackbars
  const notistackRef = createRef();
  const onClickDismiss = (key: SnackbarKey) => () => {
    notistackRef.current.closeSnackbar(key);
  };
  const action = (key: SnackbarKey) => (
    <Button onClick={onClickDismiss(key)}>Dismiss</Button>
  );

  return (
    <div id="preact_root">
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3} ref={notistackRef} action={action}>
          <HeaderComponent />
          <Container sx={{ paddingTop: "25px" }}>
            <Router>
              <Route path={ROUTE_HOME} component={Home} />
              <Route path={`${ROUTE_BOOK}/:bookId`} component={Book} />
              <Route
                path={`${ROUTE_BOOK}/:bookId/:sectionId`}
                component={Section}
              />
              <Route
                path={`${ROUTE_ACCOUNT}/:userId?`}
                component={AccountPage}
              />
              <NotFoundPage default />
            </Router>
          </Container>
        </SnackbarProvider>
      </ThemeProvider>
    </div>
  );
};

export default App;
