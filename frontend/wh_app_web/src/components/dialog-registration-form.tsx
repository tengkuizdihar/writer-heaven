import { Add, Login } from "@mui/icons-material";
import {
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  TextField,
  Tooltip,
} from "@mui/material";
import { useSnackbar } from "notistack";
import { Fragment, FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import { useState } from "preact/hooks";
import { getConfiguredHeader } from "../lib/account";
import { WHC } from "../proto/global";
import { InsertAccountParam } from "../proto/writer_heaven_pb";
import { ROUTE_HOME } from "./app";

async function registration(
  email: string,
  password: string,
  displayName: string
) {
  const pbParam = new InsertAccountParam();

  pbParam.setHeader(getConfiguredHeader());
  pbParam.setEmail(email);
  pbParam.setDisplayName(displayName);
  pbParam.setPassword(password);

  await WHC.insertAccount(pbParam, null);

  route(ROUTE_HOME);
}

export interface DialogRegistrationFormProps {
  setFormLogin: () => void;
}

export const DialogRegistrationForm: FunctionalComponent<
  DialogRegistrationFormProps
> = (props) => {
  const [email, setEmail] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  async function usecaseRegistration() {
    try {
      await registration(email, password, displayName);

      enqueueSnackbar("Successfully Registered!", {
        variant: "success",
      });

      window.location.reload();
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  }

  const passwordConfirmationValidation = password !== confirmPassword;

  return (
    <>
      <DialogContent>
        <DialogContentText>
          <b>Sign In</b> now and unlock all the features!
        </DialogContentText>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            usecaseRegistration();
          }}
        >
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            label="Email"
            value={email}
            autoComplete="username"
            onChange={(e) => setEmail((e.target as HTMLInputElement)?.value)}
          />
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            label="Display Name"
            value={displayName}
            onChange={(e) =>
              setDisplayName((e.target as HTMLInputElement)?.value)
            }
          />
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            label="Password"
            type="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => setPassword((e.target as HTMLInputElement)?.value)}
          />
          <TextField
            autoFocus
            variant="standard"
            fullWidth
            required
            error={passwordConfirmationValidation}
            helperText={
              passwordConfirmationValidation &&
              "This field need to be the same as password"
            }
            label="Confirm Password"
            type="password"
            autoComplete="current-password"
            value={confirmPassword}
            onChange={(e) =>
              setConfirmPassword((e.target as HTMLInputElement)?.value)
            }
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Tooltip title="sign in">
          <Button onClick={props.setFormLogin}>
            <Login />
          </Button>
        </Tooltip>
        <Tooltip title="register">
          <Button variant="contained" onClick={usecaseRegistration}>
            <Add />
          </Button>
        </Tooltip>
      </DialogActions>
    </>
  );
};
