import {
  Cancel,
  Delete,
  Save,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import { FunctionalComponent, h } from "preact";
import { FloatingAction } from "./floating-action";

export interface SectionFormActionProps {
  saveCallback?: () => void;
  publishCallback?: () => void;
  draftCallback?: () => void;
  deleteCallback?: () => void;
  toggleEditting?: () => void;
  isDraft: boolean;
  isShowingPublish: boolean;
  isShowingToggleEditting: boolean;
  isShowingDelete: boolean;
}

export const SectionFormAction: FunctionalComponent<SectionFormActionProps> = (
  props
) => {
  return (
    <FloatingAction
      collapseButtonTooltipText="Actions"
      childrenButtonList={[
        {
          onClick: props.toggleEditting,
          icon: <Cancel />,
          tooltipText: "Cancel Editting",
          isVisible: props.isShowingToggleEditting,
        },
        {
          onClick: props.deleteCallback,
          icon: <Delete />,
          tooltipText: "Delete",
          isVisible: props.isShowingDelete,
        },
        {
          onClick: props.publishCallback,
          icon: <VisibilityOff />,
          tooltipText: "Publish",
          isVisible: props.isShowingPublish && props.isDraft,
        },
        {
          onClick: props.draftCallback,
          icon: <Visibility />,
          tooltipText: "Draft",
          isVisible: props.isShowingPublish && !props.isDraft,
        },
        {
          onClick: props.saveCallback,
          icon: <Save />,
          tooltipText: "Save",
          isVisible: true,
        },
      ]}
    />
  );
};
