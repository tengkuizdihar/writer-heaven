import { Button, Grid, Paper, TextField, Typography } from "@mui/material";
import { FunctionalComponent, h } from "preact";
import { CSSProperties, useState } from "preact/compat";
import { useSnackbar } from "notistack";
import { valueOnUpdate } from "../lib/lib";
import { WHC } from "../proto/global";
import { InsertBookParam } from "../proto/writer_heaven_pb";
import { getConfiguredHeader } from "../lib/account";

interface BookFormCreateProp {
  style?: CSSProperties;
  loadBook: () => void;
}

const BookFormCreate: FunctionalComponent<BookFormCreateProp> = (props) => {
  const { enqueueSnackbar } = useSnackbar();
  const [isDisplayingForm, setIsDisplayingForm] = useState(false);

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const toggleIsDisplayingForm = () => {
    setIsDisplayingForm(!isDisplayingForm);
  };

  const clearFormState = () => {
    setTitle("");
    setDescription("");
  };

  const onSubmit = async () => {
    const param = new InsertBookParam();

    param.setHeader(getConfiguredHeader());
    param.setTitle(title);
    param.setDescription(description);

    try {
      toggleIsDisplayingForm();

      await WHC.insertBook(param, null);

      clearFormState();
      props.loadBook();
      enqueueSnackbar("Successfully Created A New Book!", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  return isDisplayingForm ? (
    <Grid item xs={12} md={8} lg={6} style={props.style}>
      <Paper elevation={3} sx={{ height: "100%", padding: "1rem" }}>
        <Typography variant="h5" gutterBottom>
          New Book
        </Typography>
        <TextField
          label="Title"
          type="search"
          style={{ width: "100%", marginBottom: "0.5em" }}
          value={title}
          onChange={valueOnUpdate(setTitle)}
        />
        <TextField
          label="Description"
          type="search"
          multiline
          style={{ width: "100%", marginBottom: "0.5em" }}
          value={description}
          onChange={valueOnUpdate(setDescription)}
        />
        <Grid container item justifyContent="end">
          <Button color="error" onClick={toggleIsDisplayingForm}>
            cancel
          </Button>
          <Button color="success" onClick={onSubmit}>
            submit
          </Button>
        </Grid>
      </Paper>
    </Grid>
  ) : (
    <Grid item xs={6} md={4} lg={3} style={props.style}>
      <Paper elevation={3} sx={{ height: "100%" }}>
        <Button
          variant="text"
          style={{ height: "100%", width: "100%" }}
          onClick={toggleIsDisplayingForm}
        >
          CREATE NEW BOOK
        </Button>
      </Paper>
    </Grid>
  );
};

export { BookFormCreate, BookFormCreateProp };
