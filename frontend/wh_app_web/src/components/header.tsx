import {
  AccountBox,
  AccountCircle,
  DriveFileRenameOutline,
  Logout,
} from "@mui/icons-material";
import {
  AppBar,
  Dialog,
  DialogContent,
  IconButton,
  Toolbar,
} from "@mui/material";
import { useStore } from "@nanostores/preact";
import { useSnackbar } from "notistack";
import { h, Fragment } from "preact";
import { route } from "preact-router";
import { StateUpdater, useState } from "preact/hooks";
import { AccountPersistent } from "../lib/account";
import { ROUTE_ACCOUNT, ROUTE_HOME } from "../components/app";
import { DialogLoginForm } from "./dialog-login-form";
import { DialogRegistrationForm } from "./dialog-registration-form";

async function logout() {
  AccountPersistent.set(undefined);
}

enum FormMode {
  Login,
  Registration,
}

interface LoginDialogProps {
  isOpen: boolean;
  setOpen: StateUpdater<boolean>;
}

function AccountDialog(props: LoginDialogProps) {
  const account = useStore(AccountPersistent);
  const [formMode, setFormMode] = useState(FormMode.Login);
  const { enqueueSnackbar } = useSnackbar();

  async function usecaseLogout() {
    try {
      await logout();

      enqueueSnackbar("Successfully Logged Out!", {
        variant: "success",
      });

      props.setOpen(false);

      route(ROUTE_HOME);
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  }

  async function usecaseHome() {
    props.setOpen(false);
    route(ROUTE_ACCOUNT);
  }

  async function setFormRegistration() {
    setFormMode(FormMode.Registration);
  }

  async function setFormLogin() {
    setFormMode(FormMode.Login);
  }

  return (
    <Dialog open={props.isOpen} onClose={() => props.setOpen(false)}>
      {account ? (
        <>
          <DialogContent>
            <IconButton onClick={usecaseHome}>
              <AccountBox />
            </IconButton>
            <IconButton onClick={usecaseLogout}>
              <Logout />
            </IconButton>
          </DialogContent>
        </>
      ) : (
        <div style={{ maxWidth: "25rem", width: "25rem" }}>
          {(formMode === FormMode.Login && (
            <DialogLoginForm setFormRegistration={setFormRegistration} />
          )) ||
            (formMode === FormMode.Registration && (
              <DialogRegistrationForm setFormLogin={setFormLogin} />
            ))}
        </div>
      )}
    </Dialog>
  );
}

export function HeaderComponent() {
  const [loginOpen, setLoginOpen] = useState(false);

  const handleHomeClick = () => {
    route(ROUTE_HOME);
  };

  const handleUserButton = () => {
    setLoginOpen(true);
  };

  return (
    <>
      <AccountDialog isOpen={loginOpen} setOpen={setLoginOpen} />
      <AppBar position="static">
        <Toolbar>
          <IconButton variant="text" onClick={handleHomeClick}>
            <DriveFileRenameOutline />
          </IconButton>

          <div style={{ flexGrow: 1 }} />

          <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleUserButton}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </Toolbar>
      </AppBar>
    </>
  );
}
