import { Chip, Grid, Stack, Tooltip, Typography } from "@mui/material";
import { useStore } from "@nanostores/preact";
import _ from "lodash";
import { FunctionalComponent, h } from "preact";
import { AccountPersistent } from "../lib/account";

export const AccountConfigurationTab: FunctionalComponent = () => {
  const account = useStore(AccountPersistent);
  return (
    <Stack>
      <Typography variant="h4" marginBottom={2}>
        Permissions
      </Typography>

      <Grid container gap={1}>
        {_.sortBy(account?.permissionDetail, ["description"]).map((v) => (
          <Grid item container key={v.permission} xs={2}>
            <Tooltip
              title={`${v.description} (${
                v.enabled ? v.permission : "disabled"
              })`}
            >
              <Chip
                label={`${v.description}`}
                variant={v.enabled ? "filled" : "outlined"}
              />
            </Tooltip>
          </Grid>
        ))}
      </Grid>
    </Stack>
  );
};
