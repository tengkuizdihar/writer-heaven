import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Skeleton,
  Typography,
} from "@mui/material";
import { FunctionalComponent, h, Fragment } from "preact";
import { route } from "preact-router";
import { ROUTE_BOOK } from "./app";
import { ReadMore } from "./read-more";

export interface BookCardProps {
  book_id?: number;
  title?: string;
  description?: string;
  headerImageUrl?: string;
  ownerName?: string;
}

export const BookCard: FunctionalComponent<BookCardProps> = (props) => {
  const redirect = () => {
    route(`${ROUTE_BOOK}/${props.book_id ?? ""}`);
  };

  return (
    <Card sx={{ height: "100%" }}>
      {props.book_id ? (
        <CardActionArea onClick={redirect}>
          <CardMedia
            component="img"
            height="140"
            image={
              props.headerImageUrl ??
              "https://c.tenor.com/RPccFu6jCeoAAAAC/inori-minase-minase-inori.gif" // Replace with anime GIF, to reduce stress.
            }
            alt="book header image"
          />
        </CardActionArea>
      ) : (
        <Skeleton variant="rectangular" height={140} />
      )}

      <CardContent>
        {props.ownerName ? (
          <Typography variant="body2" color="text.secondary" gutterBottom>
            Made By {props.ownerName}
          </Typography>
        ) : (
          <Skeleton width="10rem" />
        )}

        {props.title ? (
          <Typography gutterBottom variant="h5" component="div">
            {props.title}
          </Typography>
        ) : (
          <Skeleton variant="rectangular" height="3rem" />
        )}

        {props.description ? (
          <Typography variant="body2" color="text.secondary">
            <ReadMore text={props.description} />
          </Typography>
        ) : (
          <>
            <Skeleton />
            <Skeleton />
            <Skeleton />
            <Skeleton width="10rem" />
          </>
        )}
      </CardContent>
    </Card>
  );
};
