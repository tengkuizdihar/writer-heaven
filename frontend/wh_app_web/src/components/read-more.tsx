import { FunctionalComponent, h } from "preact";
import { useState } from "preact/hooks";

export interface ReadMoreProps {
  text: string;
  limit?: number;
}

export const ReadMore: FunctionalComponent<ReadMoreProps> = (props) => {
  const text = props.text;

  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  const limit = props.limit ?? 150;

  return (
    <span onClick={toggleReadMore}>
      {isReadMore ? text.slice(0, limit) : text}
      {isReadMore && text.length > limit ? "..." : ""}
    </span>
  );
};
