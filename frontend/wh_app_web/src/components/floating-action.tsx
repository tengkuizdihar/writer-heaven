import { ArrowUpward, Favorite } from "@mui/icons-material";
import { Fab, Grid, Tooltip, Zoom } from "@mui/material";
import _ from "lodash";
import { FunctionalComponent, h } from "preact";
import { useState } from "preact/hooks";

const fabStyle = {
  position: "fixed",
  bottom: 16,
  right: 16,
};

export interface ChildrenButton {
  tooltipText: string;
  isVisible: boolean;
  icon: JSX.Element;
  onClick?: () => void;
}

export interface FloatingActionProps {
  collapseButtonTooltipText: string;
  childrenButtonList: ChildrenButton[];
  otherButton?: ChildrenButton[];
}

export const FloatingAction: FunctionalComponent<FloatingActionProps> = (
  props
) => {
  const [isShowingChildren, setIsShowingChildren] = useState(false);
  const toggleIsShowingChildren = () => {
    setIsShowingChildren(!isShowingChildren);
  };

  return (
    <Grid
      container
      style={fabStyle}
      spacing={1}
      alignContent="end"
      direction="column-reverse"
    >
      {_.map(
        props.otherButton,
        (c) =>
          c.isVisible && (
            <Grid item>
              <Tooltip placement="left" title={c.tooltipText}>
                <Fab color="primary" aria-label="publish" onClick={c.onClick}>
                  {c.icon}
                </Fab>
              </Tooltip>
            </Grid>
          )
      )}
      <Grid item>
        <Tooltip placement="left" title={props.collapseButtonTooltipText}>
          <Fab
            color="primary"
            aria-label="collapse"
            onClick={toggleIsShowingChildren}
          >
            <ArrowUpward />
          </Fab>
        </Tooltip>
      </Grid>

      {isShowingChildren &&
        _.map(
          props.childrenButtonList,
          (c) =>
            c.isVisible && (
              <Grid item>
                <Zoom in={isShowingChildren} mountOnEnter unmountOnExit>
                  <Tooltip placement="left" title={c.tooltipText}>
                    <Fab
                      color="primary"
                      aria-label="publish"
                      onClick={() => {
                        if (c.onClick) {
                          c.onClick();
                        }
                        setIsShowingChildren(false);
                      }}
                    >
                      {c.icon}
                    </Fab>
                  </Tooltip>
                </Zoom>
              </Grid>
            )
        )}
    </Grid>
  );
};
