import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Grid, Paper, Skeleton, Tooltip, Typography } from "@mui/material";
import moment from "moment";
import { FunctionalComponent, h, Fragment } from "preact";
import { route } from "preact-router";
import { ROUTE_BOOK } from "./app";

export interface SectionCardProps {
  bookId?: number;
  sectionId?: number;
  title?: string;
  createdAt?: number;
  isDraft?: boolean;
}

export const SectionCard: FunctionalComponent<SectionCardProps> = (props) => {
  const datetime = moment.unix(props.createdAt ?? 0);
  const timeAccurate = datetime.format("DD-MM-YYYY HH:mm:ss");
  const timeAgo = datetime.fromNow();
  const redirect = () => {
    route(`${ROUTE_BOOK}/${props.bookId}/${props.sectionId}`);
  };

  return (
    <Paper style={{ padding: "0.5rem" }}>
      <Grid container justifyContent="space-around">
        <Grid item xs={6}>
          {props.title ? (
            <Typography onClick={redirect} style={{ cursor: "pointer" }}>
              {props.title}
            </Typography>
          ) : (
            <Skeleton variant="text" />
          )}
        </Grid>
        <Grid
          container
          item
          xs={6}
          justifyContent="flex-end"
          alignItems="center"
          gap={2}
        >
          {props.createdAt ? (
            <>
              <Grid item>
                <Tooltip title={timeAccurate}>
                  <Typography>{timeAgo}</Typography>
                </Tooltip>
              </Grid>
              {props.isDraft ? (
                <Tooltip title={"draft"}>
                  <VisibilityOff />
                </Tooltip>
              ) : (
                <Tooltip title={"published"}>
                  <Visibility />
                </Tooltip>
              )}
            </>
          ) : (
            <Skeleton variant="text" width="14em" />
          )}
        </Grid>
      </Grid>
    </Paper>
  );
};
