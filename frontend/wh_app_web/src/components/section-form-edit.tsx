import { Stack, TextField } from "@mui/material";
import { useSnackbar } from "notistack";
import { Fragment, FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import { useEffect, useState } from "preact/hooks";
import { getConfiguredHeader } from "../lib/account";
import { sf, valueOnUpdate } from "../lib/lib";
import { WHC } from "../proto/global";
import {
  DeleteSectionParam,
  UpdateSectionParam,
} from "../proto/writer_heaven_pb";
import { ROUTE_BOOK } from "./app";
import { SectionFormAction } from "./section-form-action";
import { TransparentOnFocusTextField } from "./text-field";

// Will return section ID that's being saved
const DoSavingSection = async (
  title: string,
  content: string,
  bookId: number,
  sectionId: number
): Promise<void> => {
  const SFDoSavingSectionKey = "DoSavingSection";
  await sf.do(SFDoSavingSectionKey, async () => {
    const request = new UpdateSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setBookId(bookId);
    request.setSectionId(sectionId);
    request.setTitle(title);
    request.setContent(content);
    request.setIsDraft(true);

    await WHC.updateSection(request, null);
  });
};

const DoSetIsDraftSection = async (
  bookId: number,
  sectionId: number,
  isDraft: boolean
) => {
  const SFDoPublishSectionKey = "DoSetIsDraftSection";
  await sf.do(SFDoPublishSectionKey, async () => {
    const request = new UpdateSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setBookId(bookId);
    request.setSectionId(sectionId);
    request.setIsDraft(isDraft);

    await WHC.updateSection(request, null);
  });
};

const DoDeleteSection = async (sectionId: number) => {
  const SFDoDeleteSectionKey = "DoDeleteSection";
  await sf.do(SFDoDeleteSectionKey, async () => {
    const request = new DeleteSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setSectionId(sectionId);

    await WHC.deleteSection(request, null);
  });
};

export interface SectionFormEditProps {
  bookId: number;
  sectionId: number;
  title: string;
  content: string;
  isDraft: boolean;
  loadSection: () => void;
  toggleEditting: () => void;
}

export const SectionFormEdit: FunctionalComponent<SectionFormEditProps> = (
  props
) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    setTitle(props.title);
    setContent(props.content);
  }, [props.title, props.content]);

  const handleSavingSection = async () => {
    try {
      await DoSavingSection(title, content, props.bookId, props.sectionId);
      props.loadSection();
      enqueueSnackbar("Section is Saved and Turn To Draft!", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  const handleSaveShortcut = (event: KeyboardEvent) => {
    if ((event.ctrlKey || event.metaKey) && event.key === "s") {
      event.preventDefault();
      handleSavingSection();
    }
  };

  const handlePublishSection = async () => {
    if (props.sectionId) {
      try {
        await DoSavingSection(title, content, props.bookId, props.sectionId);
        await DoSetIsDraftSection(props.bookId, props.sectionId, false);

        props.loadSection();

        enqueueSnackbar("Section is Saved and Published Successfully!", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar((error as Error).message, {
          variant: "error",
        });
      }
    }
  };

  const handleDraftSection = async () => {
    if (props.sectionId) {
      try {
        await DoSavingSection(title, content, props.bookId, props.sectionId);
        await DoSetIsDraftSection(props.bookId, props.sectionId, true);

        props.loadSection();

        enqueueSnackbar("Section is Saved and Drafted Successfully!", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar((error as Error).message, {
          variant: "error",
        });
      }
    }
  };

  const handleDelete = async () => {
    try {
      await DoDeleteSection(props.sectionId);
      route(`${ROUTE_BOOK}/${props.bookId}`);
      enqueueSnackbar("Section is Deleted Successfully!", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  return (
    <Fragment>
      <Stack>
        <TextField
          fullWidth
          onKeyDown={handleSaveShortcut}
          label="Title"
          required
          style={{ marginBottom: "0.5rem" }}
          value={title}
          onChange={valueOnUpdate(setTitle)}
        />
        <TransparentOnFocusTextField
          label="Content"
          onKeyDown={handleSaveShortcut}
          minRows={10}
          multiline
          required
          style={{ marginBottom: "0.5rem" }}
          value={content}
          onChange={valueOnUpdate(setContent)}
        />
      </Stack>

      <SectionFormAction
        saveCallback={handleSavingSection}
        publishCallback={handlePublishSection}
        draftCallback={handleDraftSection}
        toggleEditting={props.toggleEditting}
        deleteCallback={handleDelete}
        isDraft={props.isDraft}
        isShowingPublish={true}
        isShowingToggleEditting={true}
        isShowingDelete={true}
      />
    </Fragment>
  );
};
