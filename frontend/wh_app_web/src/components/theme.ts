import { ThemeOptions } from "@mui/material";
import { CSSProperties } from "preact/compat";

// https://bareynol.github.io/mui-theme-creator/#Badge
export const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: "#EEEEEE",
    },
    secondary: {
      main: "#00ADB5",
    },
    mode: "dark",
  },
  shape: {
    borderRadius: 0,
  },
};

export const modalPaperStyle: CSSProperties = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
