/**
 * @fileoverview gRPC-Web generated client stub for wh
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!

/* eslint-disable */
// @ts-nocheck

import * as grpcWeb from "grpc-web";

import * as writer_heaven_pb from "./writer_heaven_pb";

export class WriterHeavenClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string };
  options_: null | { [index: string]: any };

  constructor(
    hostname: string,
    credentials?: null | { [index: string]: string },
    options?: null | { [index: string]: any }
  ) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options["format"] = "binary";

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodDescriptorSayHello = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/SayHello",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.HelloRequest,
    writer_heaven_pb.HelloReply,
    (request: writer_heaven_pb.HelloRequest) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.HelloReply.deserializeBinary
  );

  sayHello(
    request: writer_heaven_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.HelloReply>;

  sayHello(
    request: writer_heaven_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.HelloReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.HelloReply>;

  sayHello(
    request: writer_heaven_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.HelloReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/SayHello",
        request,
        metadata || {},
        this.methodDescriptorSayHello,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/SayHello",
      request,
      metadata || {},
      this.methodDescriptorSayHello
    );
  }

  methodDescriptorGetAccount = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/GetAccount",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.GetAccountParam,
    writer_heaven_pb.GetAccountReply,
    (request: writer_heaven_pb.GetAccountParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.GetAccountReply.deserializeBinary
  );

  getAccount(
    request: writer_heaven_pb.GetAccountParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.GetAccountReply>;

  getAccount(
    request: writer_heaven_pb.GetAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetAccountReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.GetAccountReply>;

  getAccount(
    request: writer_heaven_pb.GetAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetAccountReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/GetAccount",
        request,
        metadata || {},
        this.methodDescriptorGetAccount,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/GetAccount",
      request,
      metadata || {},
      this.methodDescriptorGetAccount
    );
  }

  methodDescriptorInsertAccount = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/InsertAccount",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.InsertAccountParam,
    writer_heaven_pb.InsertAccountReply,
    (request: writer_heaven_pb.InsertAccountParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.InsertAccountReply.deserializeBinary
  );

  insertAccount(
    request: writer_heaven_pb.InsertAccountParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.InsertAccountReply>;

  insertAccount(
    request: writer_heaven_pb.InsertAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertAccountReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.InsertAccountReply>;

  insertAccount(
    request: writer_heaven_pb.InsertAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertAccountReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/InsertAccount",
        request,
        metadata || {},
        this.methodDescriptorInsertAccount,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/InsertAccount",
      request,
      metadata || {},
      this.methodDescriptorInsertAccount
    );
  }

  methodDescriptorLoginNativeAccount = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/LoginNativeAccount",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.LoginNativeAccountParam,
    writer_heaven_pb.LoginNativeAccountReply,
    (request: writer_heaven_pb.LoginNativeAccountParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.LoginNativeAccountReply.deserializeBinary
  );

  loginNativeAccount(
    request: writer_heaven_pb.LoginNativeAccountParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.LoginNativeAccountReply>;

  loginNativeAccount(
    request: writer_heaven_pb.LoginNativeAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.LoginNativeAccountReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.LoginNativeAccountReply>;

  loginNativeAccount(
    request: writer_heaven_pb.LoginNativeAccountParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.LoginNativeAccountReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/LoginNativeAccount",
        request,
        metadata || {},
        this.methodDescriptorLoginNativeAccount,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/LoginNativeAccount",
      request,
      metadata || {},
      this.methodDescriptorLoginNativeAccount
    );
  }

  methodDescriptorInsertBook = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/InsertBook",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.InsertBookParam,
    writer_heaven_pb.InsertBookReply,
    (request: writer_heaven_pb.InsertBookParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.InsertBookReply.deserializeBinary
  );

  insertBook(
    request: writer_heaven_pb.InsertBookParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.InsertBookReply>;

  insertBook(
    request: writer_heaven_pb.InsertBookParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertBookReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.InsertBookReply>;

  insertBook(
    request: writer_heaven_pb.InsertBookParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertBookReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/InsertBook",
        request,
        metadata || {},
        this.methodDescriptorInsertBook,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/InsertBook",
      request,
      metadata || {},
      this.methodDescriptorInsertBook
    );
  }

  methodDescriptorGetBook = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/GetBook",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.GetBookParam,
    writer_heaven_pb.GetBookReply,
    (request: writer_heaven_pb.GetBookParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.GetBookReply.deserializeBinary
  );

  getBook(
    request: writer_heaven_pb.GetBookParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.GetBookReply>;

  getBook(
    request: writer_heaven_pb.GetBookParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetBookReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.GetBookReply>;

  getBook(
    request: writer_heaven_pb.GetBookParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetBookReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/GetBook",
        request,
        metadata || {},
        this.methodDescriptorGetBook,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/GetBook",
      request,
      metadata || {},
      this.methodDescriptorGetBook
    );
  }

  methodDescriptorUpdateBook = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/UpdateBook",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.UpdateBookParam,
    writer_heaven_pb.UpdateBookReply,
    (request: writer_heaven_pb.UpdateBookParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.UpdateBookReply.deserializeBinary
  );

  updateBook(
    request: writer_heaven_pb.UpdateBookParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.UpdateBookReply>;

  updateBook(
    request: writer_heaven_pb.UpdateBookParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateBookReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.UpdateBookReply>;

  updateBook(
    request: writer_heaven_pb.UpdateBookParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateBookReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/UpdateBook",
        request,
        metadata || {},
        this.methodDescriptorUpdateBook,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/UpdateBook",
      request,
      metadata || {},
      this.methodDescriptorUpdateBook
    );
  }

  methodDescriptorInsertSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/InsertSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.InsertSectionParam,
    writer_heaven_pb.InsertSectionReply,
    (request: writer_heaven_pb.InsertSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.InsertSectionReply.deserializeBinary
  );

  insertSection(
    request: writer_heaven_pb.InsertSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.InsertSectionReply>;

  insertSection(
    request: writer_heaven_pb.InsertSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.InsertSectionReply>;

  insertSection(
    request: writer_heaven_pb.InsertSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/InsertSection",
        request,
        metadata || {},
        this.methodDescriptorInsertSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/InsertSection",
      request,
      metadata || {},
      this.methodDescriptorInsertSection
    );
  }

  methodDescriptorGetSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/GetSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.GetSectionParam,
    writer_heaven_pb.GetSectionReply,
    (request: writer_heaven_pb.GetSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.GetSectionReply.deserializeBinary
  );

  getSection(
    request: writer_heaven_pb.GetSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.GetSectionReply>;

  getSection(
    request: writer_heaven_pb.GetSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.GetSectionReply>;

  getSection(
    request: writer_heaven_pb.GetSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/GetSection",
        request,
        metadata || {},
        this.methodDescriptorGetSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/GetSection",
      request,
      metadata || {},
      this.methodDescriptorGetSection
    );
  }

  methodDescriptorUpdateSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/UpdateSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.UpdateSectionParam,
    writer_heaven_pb.UpdateSectionReply,
    (request: writer_heaven_pb.UpdateSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.UpdateSectionReply.deserializeBinary
  );

  updateSection(
    request: writer_heaven_pb.UpdateSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.UpdateSectionReply>;

  updateSection(
    request: writer_heaven_pb.UpdateSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.UpdateSectionReply>;

  updateSection(
    request: writer_heaven_pb.UpdateSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/UpdateSection",
        request,
        metadata || {},
        this.methodDescriptorUpdateSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/UpdateSection",
      request,
      metadata || {},
      this.methodDescriptorUpdateSection
    );
  }

  methodDescriptorDeleteSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/DeleteSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.DeleteSectionParam,
    writer_heaven_pb.DeleteSectionReply,
    (request: writer_heaven_pb.DeleteSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.DeleteSectionReply.deserializeBinary
  );

  deleteSection(
    request: writer_heaven_pb.DeleteSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.DeleteSectionReply>;

  deleteSection(
    request: writer_heaven_pb.DeleteSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.DeleteSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.DeleteSectionReply>;

  deleteSection(
    request: writer_heaven_pb.DeleteSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.DeleteSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/DeleteSection",
        request,
        metadata || {},
        this.methodDescriptorDeleteSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/DeleteSection",
      request,
      metadata || {},
      this.methodDescriptorDeleteSection
    );
  }

  methodDescriptorGetNote = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/GetNote",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.GetNoteParam,
    writer_heaven_pb.GetNoteReply,
    (request: writer_heaven_pb.GetNoteParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.GetNoteReply.deserializeBinary
  );

  getNote(
    request: writer_heaven_pb.GetNoteParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.GetNoteReply>;

  getNote(
    request: writer_heaven_pb.GetNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetNoteReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.GetNoteReply>;

  getNote(
    request: writer_heaven_pb.GetNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetNoteReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/GetNote",
        request,
        metadata || {},
        this.methodDescriptorGetNote,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/GetNote",
      request,
      metadata || {},
      this.methodDescriptorGetNote
    );
  }

  methodDescriptorInsertNote = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/InsertNote",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.InsertNoteParam,
    writer_heaven_pb.InsertNoteReply,
    (request: writer_heaven_pb.InsertNoteParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.InsertNoteReply.deserializeBinary
  );

  insertNote(
    request: writer_heaven_pb.InsertNoteParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.InsertNoteReply>;

  insertNote(
    request: writer_heaven_pb.InsertNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertNoteReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.InsertNoteReply>;

  insertNote(
    request: writer_heaven_pb.InsertNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.InsertNoteReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/InsertNote",
        request,
        metadata || {},
        this.methodDescriptorInsertNote,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/InsertNote",
      request,
      metadata || {},
      this.methodDescriptorInsertNote
    );
  }

  methodDescriptorUpdateNote = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/UpdateNote",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.UpdateNoteParam,
    writer_heaven_pb.UpdateNoteReply,
    (request: writer_heaven_pb.UpdateNoteParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.UpdateNoteReply.deserializeBinary
  );

  updateNote(
    request: writer_heaven_pb.UpdateNoteParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.UpdateNoteReply>;

  updateNote(
    request: writer_heaven_pb.UpdateNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateNoteReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.UpdateNoteReply>;

  updateNote(
    request: writer_heaven_pb.UpdateNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.UpdateNoteReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/UpdateNote",
        request,
        metadata || {},
        this.methodDescriptorUpdateNote,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/UpdateNote",
      request,
      metadata || {},
      this.methodDescriptorUpdateNote
    );
  }

  methodDescriptorDeleteNote = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/DeleteNote",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.DeleteNoteParam,
    writer_heaven_pb.DeleteNoteReply,
    (request: writer_heaven_pb.DeleteNoteParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.DeleteNoteReply.deserializeBinary
  );

  deleteNote(
    request: writer_heaven_pb.DeleteNoteParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.DeleteNoteReply>;

  deleteNote(
    request: writer_heaven_pb.DeleteNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.DeleteNoteReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.DeleteNoteReply>;

  deleteNote(
    request: writer_heaven_pb.DeleteNoteParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.DeleteNoteReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/DeleteNote",
        request,
        metadata || {},
        this.methodDescriptorDeleteNote,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/DeleteNote",
      request,
      metadata || {},
      this.methodDescriptorDeleteNote
    );
  }

  methodDescriptorLikeSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/LikeSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.LikeSectionParam,
    writer_heaven_pb.LikeSectionReply,
    (request: writer_heaven_pb.LikeSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.LikeSectionReply.deserializeBinary
  );

  likeSection(
    request: writer_heaven_pb.LikeSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.LikeSectionReply>;

  likeSection(
    request: writer_heaven_pb.LikeSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.LikeSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.LikeSectionReply>;

  likeSection(
    request: writer_heaven_pb.LikeSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.LikeSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/LikeSection",
        request,
        metadata || {},
        this.methodDescriptorLikeSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/LikeSection",
      request,
      metadata || {},
      this.methodDescriptorLikeSection
    );
  }

  methodDescriptorGetLikeSection = new grpcWeb.MethodDescriptor(
    "/wh.WriterHeaven/GetLikeSection",
    grpcWeb.MethodType.UNARY,
    writer_heaven_pb.GetLikeSectionParam,
    writer_heaven_pb.GetLikeSectionReply,
    (request: writer_heaven_pb.GetLikeSectionParam) => {
      return request.serializeBinary();
    },
    writer_heaven_pb.GetLikeSectionReply.deserializeBinary
  );

  getLikeSection(
    request: writer_heaven_pb.GetLikeSectionParam,
    metadata: grpcWeb.Metadata | null
  ): Promise<writer_heaven_pb.GetLikeSectionReply>;

  getLikeSection(
    request: writer_heaven_pb.GetLikeSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetLikeSectionReply
    ) => void
  ): grpcWeb.ClientReadableStream<writer_heaven_pb.GetLikeSectionReply>;

  getLikeSection(
    request: writer_heaven_pb.GetLikeSectionParam,
    metadata: grpcWeb.Metadata | null,
    callback?: (
      err: grpcWeb.RpcError,
      response: writer_heaven_pb.GetLikeSectionReply
    ) => void
  ) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ + "/wh.WriterHeaven/GetLikeSection",
        request,
        metadata || {},
        this.methodDescriptorGetLikeSection,
        callback
      );
    }
    return this.client_.unaryCall(
      this.hostname_ + "/wh.WriterHeaven/GetLikeSection",
      request,
      metadata || {},
      this.methodDescriptorGetLikeSection
    );
  }
}
