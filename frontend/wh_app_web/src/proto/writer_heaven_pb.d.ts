import * as jspb from "google-protobuf";

export class Header extends jspb.Message {
  getJwt(): string;
  setJwt(value: string): Header;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Header.AsObject;
  static toObject(includeInstance: boolean, msg: Header): Header.AsObject;
  static serializeBinaryToWriter(
    message: Header,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): Header;
  static deserializeBinaryFromReader(
    message: Header,
    reader: jspb.BinaryReader
  ): Header;
}

export namespace Header {
  export type AsObject = {
    jwt: string;
  };
}

export class PaginationRequest extends jspb.Message {
  getSize(): number;
  setSize(value: number): PaginationRequest;

  getNumber(): number;
  setNumber(value: number): PaginationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationRequest.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: PaginationRequest
  ): PaginationRequest.AsObject;
  static serializeBinaryToWriter(
    message: PaginationRequest,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): PaginationRequest;
  static deserializeBinaryFromReader(
    message: PaginationRequest,
    reader: jspb.BinaryReader
  ): PaginationRequest;
}

export namespace PaginationRequest {
  export type AsObject = {
    size: number;
    number: number;
  };
}

export class PaginationReply extends jspb.Message {
  getTotal(): number;
  setTotal(value: number): PaginationReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: PaginationReply
  ): PaginationReply.AsObject;
  static serializeBinaryToWriter(
    message: PaginationReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): PaginationReply;
  static deserializeBinaryFromReader(
    message: PaginationReply,
    reader: jspb.BinaryReader
  ): PaginationReply;
}

export namespace PaginationReply {
  export type AsObject = {
    total: number;
  };
}

export class HelloRequest extends jspb.Message {
  getName(): string;
  setName(value: string): HelloRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HelloRequest.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: HelloRequest
  ): HelloRequest.AsObject;
  static serializeBinaryToWriter(
    message: HelloRequest,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): HelloRequest;
  static deserializeBinaryFromReader(
    message: HelloRequest,
    reader: jspb.BinaryReader
  ): HelloRequest;
}

export namespace HelloRequest {
  export type AsObject = {
    name: string;
  };
}

export class HelloReply extends jspb.Message {
  getMessage(): string;
  setMessage(value: string): HelloReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HelloReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: HelloReply
  ): HelloReply.AsObject;
  static serializeBinaryToWriter(
    message: HelloReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): HelloReply;
  static deserializeBinaryFromReader(
    message: HelloReply,
    reader: jspb.BinaryReader
  ): HelloReply;
}

export namespace HelloReply {
  export type AsObject = {
    message: string;
  };
}

export class GetAccountParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): GetAccountParam;
  hasHeader(): boolean;
  clearHeader(): GetAccountParam;

  getId(): number;
  setId(value: number): GetAccountParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAccountParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetAccountParam
  ): GetAccountParam.AsObject;
  static serializeBinaryToWriter(
    message: GetAccountParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetAccountParam;
  static deserializeBinaryFromReader(
    message: GetAccountParam,
    reader: jspb.BinaryReader
  ): GetAccountParam;
}

export namespace GetAccountParam {
  export type AsObject = {
    header?: Header.AsObject;
    id: number;
  };
}

export class GetAccountReply extends jspb.Message {
  getId(): number;
  setId(value: number): GetAccountReply;

  getEmail(): string;
  setEmail(value: string): GetAccountReply;

  getDisplayName(): string;
  setDisplayName(value: string): GetAccountReply;

  getPermission(): Uint8Array | string;
  getPermission_asU8(): Uint8Array;
  getPermission_asB64(): string;
  setPermission(value: Uint8Array | string): GetAccountReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAccountReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetAccountReply
  ): GetAccountReply.AsObject;
  static serializeBinaryToWriter(
    message: GetAccountReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetAccountReply;
  static deserializeBinaryFromReader(
    message: GetAccountReply,
    reader: jspb.BinaryReader
  ): GetAccountReply;
}

export namespace GetAccountReply {
  export type AsObject = {
    id: number;
    email: string;
    displayName: string;
    permission: Uint8Array | string;
  };
}

export class InsertAccountParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): InsertAccountParam;
  hasHeader(): boolean;
  clearHeader(): InsertAccountParam;

  getEmail(): string;
  setEmail(value: string): InsertAccountParam;

  getDisplayName(): string;
  setDisplayName(value: string): InsertAccountParam;

  getPassword(): string;
  setPassword(value: string): InsertAccountParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertAccountParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertAccountParam
  ): InsertAccountParam.AsObject;
  static serializeBinaryToWriter(
    message: InsertAccountParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertAccountParam;
  static deserializeBinaryFromReader(
    message: InsertAccountParam,
    reader: jspb.BinaryReader
  ): InsertAccountParam;
}

export namespace InsertAccountParam {
  export type AsObject = {
    header?: Header.AsObject;
    email: string;
    displayName: string;
    password: string;
  };
}

export class InsertAccountReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertAccountReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertAccountReply
  ): InsertAccountReply.AsObject;
  static serializeBinaryToWriter(
    message: InsertAccountReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertAccountReply;
  static deserializeBinaryFromReader(
    message: InsertAccountReply,
    reader: jspb.BinaryReader
  ): InsertAccountReply;
}

export namespace InsertAccountReply {
  export type AsObject = {};
}

export class LoginNativeAccountParam extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): LoginNativeAccountParam;

  getPassword(): string;
  setPassword(value: string): LoginNativeAccountParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginNativeAccountParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: LoginNativeAccountParam
  ): LoginNativeAccountParam.AsObject;
  static serializeBinaryToWriter(
    message: LoginNativeAccountParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): LoginNativeAccountParam;
  static deserializeBinaryFromReader(
    message: LoginNativeAccountParam,
    reader: jspb.BinaryReader
  ): LoginNativeAccountParam;
}

export namespace LoginNativeAccountParam {
  export type AsObject = {
    email: string;
    password: string;
  };
}

export class LoginNativeAccountReply extends jspb.Message {
  getJwt(): string;
  setJwt(value: string): LoginNativeAccountReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginNativeAccountReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: LoginNativeAccountReply
  ): LoginNativeAccountReply.AsObject;
  static serializeBinaryToWriter(
    message: LoginNativeAccountReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): LoginNativeAccountReply;
  static deserializeBinaryFromReader(
    message: LoginNativeAccountReply,
    reader: jspb.BinaryReader
  ): LoginNativeAccountReply;
}

export namespace LoginNativeAccountReply {
  export type AsObject = {
    jwt: string;
  };
}

export class GetBookParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): GetBookParam;
  hasHeader(): boolean;
  clearHeader(): GetBookParam;

  getPagination(): PaginationRequest | undefined;
  setPagination(value?: PaginationRequest): GetBookParam;
  hasPagination(): boolean;
  clearPagination(): GetBookParam;

  getId(): number;
  setId(value: number): GetBookParam;

  getOwnerId(): number;
  setOwnerId(value: number): GetBookParam;

  getTitle(): string;
  setTitle(value: string): GetBookParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetBookParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetBookParam
  ): GetBookParam.AsObject;
  static serializeBinaryToWriter(
    message: GetBookParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetBookParam;
  static deserializeBinaryFromReader(
    message: GetBookParam,
    reader: jspb.BinaryReader
  ): GetBookParam;
}

export namespace GetBookParam {
  export type AsObject = {
    header?: Header.AsObject;
    pagination?: PaginationRequest.AsObject;
    id: number;
    ownerId: number;
    title: string;
  };
}

export class GetBookReply extends jspb.Message {
  getPagination(): PaginationReply | undefined;
  setPagination(value?: PaginationReply): GetBookReply;
  hasPagination(): boolean;
  clearPagination(): GetBookReply;

  getBookListList(): Array<GetBookReply.Book>;
  setBookListList(value: Array<GetBookReply.Book>): GetBookReply;
  clearBookListList(): GetBookReply;
  addBookList(value?: GetBookReply.Book, index?: number): GetBookReply.Book;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetBookReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetBookReply
  ): GetBookReply.AsObject;
  static serializeBinaryToWriter(
    message: GetBookReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetBookReply;
  static deserializeBinaryFromReader(
    message: GetBookReply,
    reader: jspb.BinaryReader
  ): GetBookReply;
}

export namespace GetBookReply {
  export type AsObject = {
    pagination?: PaginationReply.AsObject;
    bookListList: Array<GetBookReply.Book.AsObject>;
  };

  export class Book extends jspb.Message {
    getId(): number;
    setId(value: number): Book;

    getTitle(): string;
    setTitle(value: string): Book;

    getDescription(): string;
    setDescription(value: string): Book;

    getOwnerName(): string;
    setOwnerName(value: string): Book;

    getOwnerId(): number;
    setOwnerId(value: number): Book;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Book.AsObject;
    static toObject(includeInstance: boolean, msg: Book): Book.AsObject;
    static serializeBinaryToWriter(
      message: Book,
      writer: jspb.BinaryWriter
    ): void;
    static deserializeBinary(bytes: Uint8Array): Book;
    static deserializeBinaryFromReader(
      message: Book,
      reader: jspb.BinaryReader
    ): Book;
  }

  export namespace Book {
    export type AsObject = {
      id: number;
      title: string;
      description: string;
      ownerName: string;
      ownerId: number;
    };
  }
}

export class InsertBookParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): InsertBookParam;
  hasHeader(): boolean;
  clearHeader(): InsertBookParam;

  getTitle(): string;
  setTitle(value: string): InsertBookParam;

  getDescription(): string;
  setDescription(value: string): InsertBookParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertBookParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertBookParam
  ): InsertBookParam.AsObject;
  static serializeBinaryToWriter(
    message: InsertBookParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertBookParam;
  static deserializeBinaryFromReader(
    message: InsertBookParam,
    reader: jspb.BinaryReader
  ): InsertBookParam;
}

export namespace InsertBookParam {
  export type AsObject = {
    header?: Header.AsObject;
    title: string;
    description: string;
  };
}

export class InsertBookReply extends jspb.Message {
  getId(): number;
  setId(value: number): InsertBookReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertBookReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertBookReply
  ): InsertBookReply.AsObject;
  static serializeBinaryToWriter(
    message: InsertBookReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertBookReply;
  static deserializeBinaryFromReader(
    message: InsertBookReply,
    reader: jspb.BinaryReader
  ): InsertBookReply;
}

export namespace InsertBookReply {
  export type AsObject = {
    id: number;
  };
}

export class GetSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): GetSectionParam;
  hasHeader(): boolean;
  clearHeader(): GetSectionParam;

  getPagination(): PaginationRequest | undefined;
  setPagination(value?: PaginationRequest): GetSectionParam;
  hasPagination(): boolean;
  clearPagination(): GetSectionParam;

  getBookId(): number;
  setBookId(value: number): GetSectionParam;

  getSectionId(): number;
  setSectionId(value: number): GetSectionParam;

  getTitle(): string;
  setTitle(value: string): GetSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetSectionParam
  ): GetSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: GetSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetSectionParam;
  static deserializeBinaryFromReader(
    message: GetSectionParam,
    reader: jspb.BinaryReader
  ): GetSectionParam;
}

export namespace GetSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    pagination?: PaginationRequest.AsObject;
    bookId: number;
    sectionId: number;
    title: string;
  };
}

export class GetSectionReply extends jspb.Message {
  getPagination(): PaginationReply | undefined;
  setPagination(value?: PaginationReply): GetSectionReply;
  hasPagination(): boolean;
  clearPagination(): GetSectionReply;

  getSectionListList(): Array<GetSectionReply.Section>;
  setSectionListList(value: Array<GetSectionReply.Section>): GetSectionReply;
  clearSectionListList(): GetSectionReply;
  addSectionList(
    value?: GetSectionReply.Section,
    index?: number
  ): GetSectionReply.Section;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetSectionReply
  ): GetSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: GetSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetSectionReply;
  static deserializeBinaryFromReader(
    message: GetSectionReply,
    reader: jspb.BinaryReader
  ): GetSectionReply;
}

export namespace GetSectionReply {
  export type AsObject = {
    pagination?: PaginationReply.AsObject;
    sectionListList: Array<GetSectionReply.Section.AsObject>;
  };

  export class Section extends jspb.Message {
    getId(): number;
    setId(value: number): Section;

    getOwnerBookId(): number;
    setOwnerBookId(value: number): Section;

    getTitle(): string;
    setTitle(value: string): Section;

    getContent(): string;
    setContent(value: string): Section;

    getIsDraft(): boolean;
    setIsDraft(value: boolean): Section;

    getCreatedAt(): number;
    setCreatedAt(value: number): Section;

    getUpdatedAt(): number;
    setUpdatedAt(value: number): Section;

    getLikeCount(): number;
    setLikeCount(value: number): Section;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Section.AsObject;
    static toObject(includeInstance: boolean, msg: Section): Section.AsObject;
    static serializeBinaryToWriter(
      message: Section,
      writer: jspb.BinaryWriter
    ): void;
    static deserializeBinary(bytes: Uint8Array): Section;
    static deserializeBinaryFromReader(
      message: Section,
      reader: jspb.BinaryReader
    ): Section;
  }

  export namespace Section {
    export type AsObject = {
      id: number;
      ownerBookId: number;
      title: string;
      content: string;
      isDraft: boolean;
      createdAt: number;
      updatedAt: number;
      likeCount: number;
    };
  }
}

export class InsertSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): InsertSectionParam;
  hasHeader(): boolean;
  clearHeader(): InsertSectionParam;

  getBookId(): number;
  setBookId(value: number): InsertSectionParam;

  getTitle(): string;
  setTitle(value: string): InsertSectionParam;

  getContent(): string;
  setContent(value: string): InsertSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertSectionParam
  ): InsertSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: InsertSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertSectionParam;
  static deserializeBinaryFromReader(
    message: InsertSectionParam,
    reader: jspb.BinaryReader
  ): InsertSectionParam;
}

export namespace InsertSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    bookId: number;
    title: string;
    content: string;
  };
}

export class InsertSectionReply extends jspb.Message {
  getId(): number;
  setId(value: number): InsertSectionReply;

  getOwnerBookId(): number;
  setOwnerBookId(value: number): InsertSectionReply;

  getTitle(): string;
  setTitle(value: string): InsertSectionReply;

  getContent(): string;
  setContent(value: string): InsertSectionReply;

  getCreatedAt(): number;
  setCreatedAt(value: number): InsertSectionReply;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): InsertSectionReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertSectionReply
  ): InsertSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: InsertSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertSectionReply;
  static deserializeBinaryFromReader(
    message: InsertSectionReply,
    reader: jspb.BinaryReader
  ): InsertSectionReply;
}

export namespace InsertSectionReply {
  export type AsObject = {
    id: number;
    ownerBookId: number;
    title: string;
    content: string;
    createdAt: number;
    updatedAt: number;
  };
}

export class UpdateBookParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): UpdateBookParam;
  hasHeader(): boolean;
  clearHeader(): UpdateBookParam;

  getId(): number;
  setId(value: number): UpdateBookParam;

  getTitle(): string;
  setTitle(value: string): UpdateBookParam;

  getDescription(): string;
  setDescription(value: string): UpdateBookParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateBookParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateBookParam
  ): UpdateBookParam.AsObject;
  static serializeBinaryToWriter(
    message: UpdateBookParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateBookParam;
  static deserializeBinaryFromReader(
    message: UpdateBookParam,
    reader: jspb.BinaryReader
  ): UpdateBookParam;
}

export namespace UpdateBookParam {
  export type AsObject = {
    header?: Header.AsObject;
    id: number;
    title: string;
    description: string;
  };
}

export class UpdateBookReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateBookReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateBookReply
  ): UpdateBookReply.AsObject;
  static serializeBinaryToWriter(
    message: UpdateBookReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateBookReply;
  static deserializeBinaryFromReader(
    message: UpdateBookReply,
    reader: jspb.BinaryReader
  ): UpdateBookReply;
}

export namespace UpdateBookReply {
  export type AsObject = {};
}

export class UpdateSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): UpdateSectionParam;
  hasHeader(): boolean;
  clearHeader(): UpdateSectionParam;

  getBookId(): number;
  setBookId(value: number): UpdateSectionParam;

  getSectionId(): number;
  setSectionId(value: number): UpdateSectionParam;

  getTitle(): string;
  setTitle(value: string): UpdateSectionParam;

  getContent(): string;
  setContent(value: string): UpdateSectionParam;

  getIsDraft(): boolean;
  setIsDraft(value: boolean): UpdateSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateSectionParam
  ): UpdateSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: UpdateSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateSectionParam;
  static deserializeBinaryFromReader(
    message: UpdateSectionParam,
    reader: jspb.BinaryReader
  ): UpdateSectionParam;
}

export namespace UpdateSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    bookId: number;
    sectionId: number;
    title: string;
    content: string;
    isDraft: boolean;
  };
}

export class UpdateSectionReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateSectionReply
  ): UpdateSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: UpdateSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateSectionReply;
  static deserializeBinaryFromReader(
    message: UpdateSectionReply,
    reader: jspb.BinaryReader
  ): UpdateSectionReply;
}

export namespace UpdateSectionReply {
  export type AsObject = {};
}

export class DeleteSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): DeleteSectionParam;
  hasHeader(): boolean;
  clearHeader(): DeleteSectionParam;

  getSectionId(): number;
  setSectionId(value: number): DeleteSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: DeleteSectionParam
  ): DeleteSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: DeleteSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSectionParam;
  static deserializeBinaryFromReader(
    message: DeleteSectionParam,
    reader: jspb.BinaryReader
  ): DeleteSectionParam;
}

export namespace DeleteSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    sectionId: number;
  };
}

export class DeleteSectionReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: DeleteSectionReply
  ): DeleteSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: DeleteSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSectionReply;
  static deserializeBinaryFromReader(
    message: DeleteSectionReply,
    reader: jspb.BinaryReader
  ): DeleteSectionReply;
}

export namespace DeleteSectionReply {
  export type AsObject = {};
}

export class GetNoteParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): GetNoteParam;
  hasHeader(): boolean;
  clearHeader(): GetNoteParam;

  getId(): number;
  setId(value: number): GetNoteParam;

  getKeyword(): string;
  setKeyword(value: string): GetNoteParam;

  getTagListList(): Array<string>;
  setTagListList(value: Array<string>): GetNoteParam;
  clearTagListList(): GetNoteParam;
  addTagList(value: string, index?: number): GetNoteParam;

  getPagination(): PaginationRequest | undefined;
  setPagination(value?: PaginationRequest): GetNoteParam;
  hasPagination(): boolean;
  clearPagination(): GetNoteParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNoteParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetNoteParam
  ): GetNoteParam.AsObject;
  static serializeBinaryToWriter(
    message: GetNoteParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetNoteParam;
  static deserializeBinaryFromReader(
    message: GetNoteParam,
    reader: jspb.BinaryReader
  ): GetNoteParam;
}

export namespace GetNoteParam {
  export type AsObject = {
    header?: Header.AsObject;
    id: number;
    keyword: string;
    tagListList: Array<string>;
    pagination?: PaginationRequest.AsObject;
  };
}

export class GetNoteReply extends jspb.Message {
  getPagination(): PaginationReply | undefined;
  setPagination(value?: PaginationReply): GetNoteReply;
  hasPagination(): boolean;
  clearPagination(): GetNoteReply;

  getNoteListList(): Array<GetNoteReply.Note>;
  setNoteListList(value: Array<GetNoteReply.Note>): GetNoteReply;
  clearNoteListList(): GetNoteReply;
  addNoteList(value?: GetNoteReply.Note, index?: number): GetNoteReply.Note;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNoteReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetNoteReply
  ): GetNoteReply.AsObject;
  static serializeBinaryToWriter(
    message: GetNoteReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetNoteReply;
  static deserializeBinaryFromReader(
    message: GetNoteReply,
    reader: jspb.BinaryReader
  ): GetNoteReply;
}

export namespace GetNoteReply {
  export type AsObject = {
    pagination?: PaginationReply.AsObject;
    noteListList: Array<GetNoteReply.Note.AsObject>;
  };

  export class Note extends jspb.Message {
    getId(): number;
    setId(value: number): Note;

    getContent(): string;
    setContent(value: string): Note;

    getTagListList(): Array<string>;
    setTagListList(value: Array<string>): Note;
    clearTagListList(): Note;
    addTagList(value: string, index?: number): Note;

    getCreatedAt(): number;
    setCreatedAt(value: number): Note;

    getUpdatedAt(): number;
    setUpdatedAt(value: number): Note;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Note.AsObject;
    static toObject(includeInstance: boolean, msg: Note): Note.AsObject;
    static serializeBinaryToWriter(
      message: Note,
      writer: jspb.BinaryWriter
    ): void;
    static deserializeBinary(bytes: Uint8Array): Note;
    static deserializeBinaryFromReader(
      message: Note,
      reader: jspb.BinaryReader
    ): Note;
  }

  export namespace Note {
    export type AsObject = {
      id: number;
      content: string;
      tagListList: Array<string>;
      createdAt: number;
      updatedAt: number;
    };
  }
}

export class InsertNoteParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): InsertNoteParam;
  hasHeader(): boolean;
  clearHeader(): InsertNoteParam;

  getContent(): string;
  setContent(value: string): InsertNoteParam;

  getTagListList(): Array<string>;
  setTagListList(value: Array<string>): InsertNoteParam;
  clearTagListList(): InsertNoteParam;
  addTagList(value: string, index?: number): InsertNoteParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertNoteParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertNoteParam
  ): InsertNoteParam.AsObject;
  static serializeBinaryToWriter(
    message: InsertNoteParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertNoteParam;
  static deserializeBinaryFromReader(
    message: InsertNoteParam,
    reader: jspb.BinaryReader
  ): InsertNoteParam;
}

export namespace InsertNoteParam {
  export type AsObject = {
    header?: Header.AsObject;
    content: string;
    tagListList: Array<string>;
  };
}

export class InsertNoteReply extends jspb.Message {
  getId(): number;
  setId(value: number): InsertNoteReply;

  getContent(): string;
  setContent(value: string): InsertNoteReply;

  getTagListList(): Array<string>;
  setTagListList(value: Array<string>): InsertNoteReply;
  clearTagListList(): InsertNoteReply;
  addTagList(value: string, index?: number): InsertNoteReply;

  getCreatedAt(): number;
  setCreatedAt(value: number): InsertNoteReply;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): InsertNoteReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsertNoteReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: InsertNoteReply
  ): InsertNoteReply.AsObject;
  static serializeBinaryToWriter(
    message: InsertNoteReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): InsertNoteReply;
  static deserializeBinaryFromReader(
    message: InsertNoteReply,
    reader: jspb.BinaryReader
  ): InsertNoteReply;
}

export namespace InsertNoteReply {
  export type AsObject = {
    id: number;
    content: string;
    tagListList: Array<string>;
    createdAt: number;
    updatedAt: number;
  };
}

export class UpdateNoteParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): UpdateNoteParam;
  hasHeader(): boolean;
  clearHeader(): UpdateNoteParam;

  getId(): number;
  setId(value: number): UpdateNoteParam;

  getContent(): string;
  setContent(value: string): UpdateNoteParam;

  getTagListList(): Array<string>;
  setTagListList(value: Array<string>): UpdateNoteParam;
  clearTagListList(): UpdateNoteParam;
  addTagList(value: string, index?: number): UpdateNoteParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateNoteParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateNoteParam
  ): UpdateNoteParam.AsObject;
  static serializeBinaryToWriter(
    message: UpdateNoteParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateNoteParam;
  static deserializeBinaryFromReader(
    message: UpdateNoteParam,
    reader: jspb.BinaryReader
  ): UpdateNoteParam;
}

export namespace UpdateNoteParam {
  export type AsObject = {
    header?: Header.AsObject;
    id: number;
    content: string;
    tagListList: Array<string>;
  };
}

export class UpdateNoteReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateNoteReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: UpdateNoteReply
  ): UpdateNoteReply.AsObject;
  static serializeBinaryToWriter(
    message: UpdateNoteReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): UpdateNoteReply;
  static deserializeBinaryFromReader(
    message: UpdateNoteReply,
    reader: jspb.BinaryReader
  ): UpdateNoteReply;
}

export namespace UpdateNoteReply {
  export type AsObject = {};
}

export class DeleteNoteParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): DeleteNoteParam;
  hasHeader(): boolean;
  clearHeader(): DeleteNoteParam;

  getId(): number;
  setId(value: number): DeleteNoteParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteNoteParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: DeleteNoteParam
  ): DeleteNoteParam.AsObject;
  static serializeBinaryToWriter(
    message: DeleteNoteParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): DeleteNoteParam;
  static deserializeBinaryFromReader(
    message: DeleteNoteParam,
    reader: jspb.BinaryReader
  ): DeleteNoteParam;
}

export namespace DeleteNoteParam {
  export type AsObject = {
    header?: Header.AsObject;
    id: number;
  };
}

export class DeleteNoteReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteNoteReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: DeleteNoteReply
  ): DeleteNoteReply.AsObject;
  static serializeBinaryToWriter(
    message: DeleteNoteReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): DeleteNoteReply;
  static deserializeBinaryFromReader(
    message: DeleteNoteReply,
    reader: jspb.BinaryReader
  ): DeleteNoteReply;
}

export namespace DeleteNoteReply {
  export type AsObject = {};
}

export class LikeSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): LikeSectionParam;
  hasHeader(): boolean;
  clearHeader(): LikeSectionParam;

  getSectionId(): number;
  setSectionId(value: number): LikeSectionParam;

  getIsLike(): boolean;
  setIsLike(value: boolean): LikeSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LikeSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: LikeSectionParam
  ): LikeSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: LikeSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): LikeSectionParam;
  static deserializeBinaryFromReader(
    message: LikeSectionParam,
    reader: jspb.BinaryReader
  ): LikeSectionParam;
}

export namespace LikeSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    sectionId: number;
    isLike: boolean;
  };
}

export class LikeSectionReply extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LikeSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: LikeSectionReply
  ): LikeSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: LikeSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): LikeSectionReply;
  static deserializeBinaryFromReader(
    message: LikeSectionReply,
    reader: jspb.BinaryReader
  ): LikeSectionReply;
}

export namespace LikeSectionReply {
  export type AsObject = {};
}

export class GetLikeSectionParam extends jspb.Message {
  getHeader(): Header | undefined;
  setHeader(value?: Header): GetLikeSectionParam;
  hasHeader(): boolean;
  clearHeader(): GetLikeSectionParam;

  getSectionId(): number;
  setSectionId(value: number): GetLikeSectionParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetLikeSectionParam.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetLikeSectionParam
  ): GetLikeSectionParam.AsObject;
  static serializeBinaryToWriter(
    message: GetLikeSectionParam,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetLikeSectionParam;
  static deserializeBinaryFromReader(
    message: GetLikeSectionParam,
    reader: jspb.BinaryReader
  ): GetLikeSectionParam;
}

export namespace GetLikeSectionParam {
  export type AsObject = {
    header?: Header.AsObject;
    sectionId: number;
  };
}

export class GetLikeSectionReply extends jspb.Message {
  getIsLike(): boolean;
  setIsLike(value: boolean): GetLikeSectionReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetLikeSectionReply.AsObject;
  static toObject(
    includeInstance: boolean,
    msg: GetLikeSectionReply
  ): GetLikeSectionReply.AsObject;
  static serializeBinaryToWriter(
    message: GetLikeSectionReply,
    writer: jspb.BinaryWriter
  ): void;
  static deserializeBinary(bytes: Uint8Array): GetLikeSectionReply;
  static deserializeBinaryFromReader(
    message: GetLikeSectionReply,
    reader: jspb.BinaryReader
  ): GetLikeSectionReply;
}

export namespace GetLikeSectionReply {
  export type AsObject = {
    isLike: boolean;
  };
}
