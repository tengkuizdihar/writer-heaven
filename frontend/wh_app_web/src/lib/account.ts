import { persistentAtom } from "@nanostores/persistent";
import { Header } from "../proto/writer_heaven_pb";
import { Buffer } from "buffer";
import { PermissionDetail } from "./permission";

// For more information https://preactjs.com/guide/v10/context
export const AccountPersistent = persistentAtom<AccountModel | undefined>(
  "account",
  undefined,
  {
    encode: JSON.stringify,
    decode: JSON.parse,
  }
);

export interface AccountModel {
  jwt?: string;
  name?: string;
  permissionDetail?: Array<PermissionDetail>;
}

export function getConfiguredHeader(): Header {
  const header = new Header();

  const account = AccountPersistent.get();
  header.setJwt(account?.jwt ?? "");

  return header;
}

export interface JwtClaim {
  exp: number;
  sub: number;
  slt: string;
}

export function parseJwt(token: string): JwtClaim | undefined {
  try {
    return JSON.parse(
      Buffer.from(token.split(".")[1], "base64").toString("utf8")
    );
  } catch (e) {
    return undefined;
  }
}
