export enum Permission {
  /// Used for usecases that has the ability to get Personally Identified Information for an account.
  /// For example, an admin will have this permission but regular user will not, and could only get their own account.
  GetAccountOtherMember = 1,

  /// Granting the ability for the user to create a user with other roles such as Admin.
  CreateUserWithAnyRole = 2,
}

export interface PermissionDetail {
  permission: Permission;
  enabled: boolean;
  description: string;
}

export function checkPermissionByIndex(
  permissionArray: Uint8Array,
  bitIndex: Permission
): boolean {
  const u8Index = Math.floor(bitIndex / 8);

  if (permissionArray.length - 1 < u8Index) {
    return false;
  }

  const u8Permission = permissionArray[u8Index];
  const u8LocalIndex = bitIndex % 8;
  return (u8Permission & (0b1 << u8LocalIndex)) > 0;
}

export function mapPermissionToDescription(permission: Permission) {
  switch (permission) {
    case Permission.GetAccountOtherMember:
      return "Get Account Other Member";
    case Permission.CreateUserWithAnyRole:
      return "Create User With Any Role";
    default:
      return "Unnamed Permission";
  }
}

export function buildPermissionDetail(
  permissionArray: Uint8Array
): Array<PermissionDetail> {
  const sorted = Object.keys(Permission)
    .filter((item) => {
      return !isNaN(Number(item));
    })
    .map((v) => Number(v))
    .map((v) => ({
      permission: v,
      enabled: checkPermissionByIndex(permissionArray, v as Permission),
      description: mapPermissionToDescription(v),
    }))
    .sort((a, b) => a.permission - b.permission);

  return sorted;
}
