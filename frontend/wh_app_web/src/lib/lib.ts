import { Singleflight } from "@zcong/singleflight";
import { fn } from "moment";
import { StateUpdater } from "preact/hooks";

export const DEFAULT_PAGE_SIZE = 25;

export function getTotalPages(itemCounts: number): number {
  if (itemCounts == 0) {
    return 1;
  }

  const pages = itemCounts / (DEFAULT_PAGE_SIZE * 1.0);
  return Math.ceil(pages);
}

/**
 * Use this function to simulate delay in your async functions.
 * This code should not be anywhere in your code and should never be committed if called.
 *
 * @param time
 * @returns
 */
export async function asyncSleep(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export function valueOnUpdate<Y extends EventTarget>(
  stateUpdaterFunc: StateUpdater<string>
): (e: React.ChangeEvent<Y>) => void {
  return (e) => {
    const value = (e.target as HTMLInputElement).value;
    stateUpdaterFunc(value);
  };
}

export const sf = new Singleflight();

export function emptyCallback() {
  return;
}
