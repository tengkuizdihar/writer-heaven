import _ from "lodash";
import { Box } from "@mui/system";
import { Grid, Pagination, Tab, Tabs, Typography } from "@mui/material";
import { useStore } from "@nanostores/preact";
import { useEffect, useState } from "preact/hooks";
import { FunctionalComponent, h, Fragment } from "preact";

import { BookCard } from "../../components/book-card";
import { BookFormCreate } from "../../components/book-form-create";
import { TabPanel } from "../../components/tab-panel";
import {
  AccountPersistent,
  getConfiguredHeader,
  parseJwt,
} from "../../lib/account";
import { DEFAULT_PAGE_SIZE, getTotalPages } from "../../lib/lib";
import { WHC } from "../../proto/global";
import {
  GetBookParam,
  GetBookReply,
  PaginationRequest,
} from "../../proto/writer_heaven_pb";
import { AccountConfigurationTab } from "../../components/account-configuration-tab";

interface AccountProps {
  userId?: number;
}

async function getBooks(
  accountId: number,
  pageNumber: number
): Promise<GetBookReply.AsObject> {
  const request = new GetBookParam();

  const pagination = new PaginationRequest();
  pagination.setNumber(pageNumber);
  pagination.setSize(DEFAULT_PAGE_SIZE);

  request.setPagination(pagination);
  request.setHeader(getConfiguredHeader());
  request.setOwnerId(accountId);

  return (await WHC.getBook(request, null)).toObject();
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const AccountPage: FunctionalComponent<AccountProps> = () => {
  const account = useStore(AccountPersistent);

  const [page, setPage] = useState<number>();
  const [books, setBooks] = useState<GetBookReply.AsObject>();
  const [tab, setTab] = useState<number>(0);

  useEffect(() => {
    setBooks(undefined);
    const accountId = parseJwt(account?.jwt ?? "")?.sub ?? 0;
    getBooks(accountId, page ?? 0).then((v) => {
      setBooks(v);
    });
  }, [page, account]);

  const loadBook = () => {
    setBooks(undefined);
    const accountId = parseJwt(account?.jwt ?? "")?.sub ?? 0;
    getBooks(accountId, page ?? 0).then((v) => {
      setBooks(v);
    });
  };

  const handlePageChange = (_event: unknown, value: number) => {
    setPage(value - 1);
  };

  const handleTabChange = (_event: unknown, value: number) => {
    setTab(value);
  };

  return (
    <>
      <Grid container justifyContent="center" sx={{ marginBottom: "2em" }}>
        <Typography variant="h4">{account?.name ?? "Guest"}</Typography>
      </Grid>

      <Box sx={{ width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={tab}
            onChange={handleTabChange}
            aria-label="basic tabs example"
          >
            <Tab label="Books" {...a11yProps(0)} />
            <Tab label="Configuration" {...a11yProps(1)} />
          </Tabs>
        </Box>

        {/* Tab For Books */}
        <TabPanel value={tab} index={0}>
          <Grid container spacing={2}>
            {/* Will only render book form create card in the first page */}
            {(page ?? 0) < 1 && (
              <BookFormCreate
                loadBook={loadBook}
                style={{ minHeight: "20rem" }}
              />
            )}

            {books
              ? books?.bookListList.map((b) => (
                  <Grid key={b.id} item xs={6} md={4} lg={3}>
                    <BookCard
                      book_id={b.id}
                      title={b.title}
                      description={b.description}
                      ownerName={b.ownerName}
                    />
                  </Grid>
                ))
              : _.map(_.range(0, 15), () => (
                  <Grid item xs={6} md={4} lg={3}>
                    <BookCard />
                  </Grid>
                ))}
          </Grid>
          <Grid
            container
            justifyContent="center"
            sx={{ marginTop: "2em", height: "50px" }}
          >
            <Pagination
              value={page}
              count={getTotalPages(books?.pagination?.total ?? 0)}
              onChange={handlePageChange}
            />
          </Grid>
        </TabPanel>

        {/* Tab for configurations */}
        <TabPanel value={tab} index={1}>
          <AccountConfigurationTab />
        </TabPanel>
      </Box>
    </>
  );
};

export default AccountPage;
