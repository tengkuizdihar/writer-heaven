import _ from "lodash";
import moment from "moment";
import { route } from "preact-router";
import Markdown from "preact-markdown";
import { Fragment, FunctionalComponent, h } from "preact";
import { useEffect, useState } from "preact/hooks";
import { Box, Stack, Tooltip, Typography } from "@mui/material";
import { ROUTE_BOOK, ROUTE_NOTFOUND } from "../../components/app";
import {
  AccountPersistent,
  getConfiguredHeader,
  parseJwt,
} from "../../lib/account";
import { WHC } from "../../proto/global";
import {
  DeleteSectionParam,
  GetBookParam,
  GetBookReply,
  GetLikeSectionParam,
  GetLikeSectionReply,
  GetSectionParam,
  GetSectionReply,
  LikeSectionParam,
  PaginationRequest,
  UpdateSectionParam,
} from "../../proto/writer_heaven_pb";
import { SectionReadAction } from "../../components/section-read-action";
import { SectionFormEdit } from "../../components/section-form-edit";
import { emptyCallback, sf } from "../../lib/lib";
import { useSnackbar } from "notistack";
import { useStore } from "@nanostores/preact";
import "./style.css";

export interface SectionProps {
  bookId: number;
  sectionId: number;
}

async function getSection(
  bookId: number,
  sectionId: number
): Promise<GetSectionReply.Section.AsObject | undefined> {
  const param = new GetSectionParam();
  const pagination = new PaginationRequest();

  pagination.setSize(1);

  param.setPagination(pagination);
  param.setHeader(getConfiguredHeader());
  param.setBookId(bookId);
  param.setSectionId(sectionId);

  const reply = await WHC.getSection(param, null);
  const sections = _.first(reply.getSectionListList());

  return sections?.toObject();
}

async function getBook(
  bookId: number
): Promise<GetBookReply.Book.AsObject | undefined> {
  const request = new GetBookParam();

  const pagination = new PaginationRequest();
  pagination.setNumber(0);
  pagination.setSize(1);

  request.setPagination(pagination);
  request.setHeader(getConfiguredHeader());
  request.setId(bookId);

  const books = await WHC.getBook(request, null);
  const book = _.first(books.getBookListList());

  return await book?.toObject();
}

const DoSetDraftSection = async (
  bookId: number,
  sectionId: number,
  isDraft: boolean
) => {
  const SFDoSetDraftSectionKey = "DoSetDraftSection";
  await sf.do(SFDoSetDraftSectionKey, async () => {
    const request = new UpdateSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setBookId(bookId);
    request.setSectionId(sectionId);
    request.setIsDraft(isDraft);

    await WHC.updateSection(request, null);
  });
};

const DoDeleteSection = async (sectionId: number) => {
  const SFDoDeleteSectionKey = "DoDeleteSection";
  await sf.do(SFDoDeleteSectionKey, async () => {
    const request = new DeleteSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setSectionId(sectionId);

    await WHC.deleteSection(request, null);
  });
};

const DoLikeSection = async (sectionId: number, isLike: boolean) => {
  const SFDoLikeSection = `DoLikeSection|${sectionId}`;
  await sf.do(SFDoLikeSection, async () => {
    const request = new LikeSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setSectionId(sectionId);
    request.setIsLike(isLike);

    await WHC.likeSection(request, null);
  });
};

const GetLikeSection = async (
  sectionId: number
): Promise<GetLikeSectionReply.AsObject | undefined> => {
  const SFDoLikeSection = `DoLikeSection|${sectionId}`;
  return await sf.do(SFDoLikeSection, async () => {
    const request = new GetLikeSectionParam();

    request.setHeader(getConfiguredHeader());
    request.setSectionId(sectionId);

    return await (await WHC.getLikeSection(request, null)).toObject();
  });
};

const Section: FunctionalComponent<SectionProps> = (props) => {
  const account = useStore(AccountPersistent);
  const { enqueueSnackbar } = useSnackbar();
  const [book, setBook] = useState<GetBookReply.Book.AsObject>();
  const [section, setSection] = useState<GetSectionReply.Section.AsObject>();
  const [isEditting, setIsEditting] = useState(false);
  const [isLiked, setIsLiked] = useState(false);
  const isOwnedByCurrentUser =
    parseJwt(account?.jwt ?? "")?.sub === book?.ownerId;

  const datetime = moment.unix(section?.createdAt ?? 0);
  const timeAccurate = datetime.format("DD-MM-YYYY HH:mm:ss");
  const timeAgo = datetime.fromNow();

  useEffect(() => {
    getSection(props.bookId, props.sectionId).then((v) => {
      if (v) {
        setSection(v);
      } else {
        route(ROUTE_NOTFOUND);
      }
    });

    getBook(props.bookId).then((v) => {
      if (v) {
        setBook(v);
      } else {
        route(ROUTE_NOTFOUND);
      }
    });

    GetLikeSection(props.sectionId).then((v) => {
      setIsLiked(v?.isLike ?? false);
    });
  }, [props.bookId, props.sectionId]);

  const handlePublishSection = async () => {
    if (props.sectionId) {
      try {
        await DoSetDraftSection(props.bookId, props.sectionId, false);

        enqueueSnackbar("Section is Published Successfully!", {
          variant: "success",
        });

        const updatedSection = await getSection(props.bookId, props.sectionId);
        if (updatedSection) {
          setSection(updatedSection);
        } else {
          route(ROUTE_NOTFOUND);
        }
      } catch (error) {
        enqueueSnackbar((error as Error).message, {
          variant: "error",
        });
      }
    }
  };

  const handleDraftSection = async () => {
    if (props.sectionId) {
      try {
        await DoSetDraftSection(props.bookId, props.sectionId, true);

        enqueueSnackbar("Section is Drafted Successfully!", {
          variant: "success",
        });

        const updatedSection = await getSection(props.bookId, props.sectionId);
        if (updatedSection) {
          setSection(updatedSection);
        } else {
          route(ROUTE_NOTFOUND);
        }
      } catch (error) {
        enqueueSnackbar((error as Error).message, {
          variant: "error",
        });
      }
    }
  };

  const handleLike = async () => {
    if (props.sectionId) {
      try {
        await DoLikeSection(props.sectionId, !isLiked);
        setIsLiked(!isLiked);
      } catch (error) {
        enqueueSnackbar((error as Error).message, {
          variant: "error",
        });
      }
    }
  };

  const toggleEditting = () => {
    setIsEditting(!isEditting);
  };

  const handleDelete = async () => {
    try {
      await DoDeleteSection(props.sectionId);
      route(`${ROUTE_BOOK}/${props.bookId}`);
      enqueueSnackbar("Section is Deleted Successfully!", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar((error as Error).message, {
        variant: "error",
      });
    }
  };

  return (
    <Stack>
      {isEditting ? (
        section !== undefined && (
          <SectionFormEdit
            bookId={props.bookId}
            sectionId={props.sectionId}
            title={section.title}
            content={section.content}
            isDraft={section.isDraft}
            toggleEditting={toggleEditting}
            loadSection={() => {
              getSection(props.bookId, props.sectionId).then((v) => {
                if (v) {
                  setSection(v);
                } else {
                  route(ROUTE_NOTFOUND);
                }
              });
            }}
          />
        )
      ) : (
        <Fragment>
          <Typography variant="h3">{section?.title}</Typography>
          <Tooltip
            title={timeAccurate}
            width="fit-content"
            placement="right"
            arrow
          >
            <Typography>{timeAgo}</Typography>
          </Tooltip>
          <Box height="min(5vh,50px)" />
          {h(Markdown, { markdown: section?.content ?? "" })}
          {section !== undefined &&
            book !== undefined &&
            isOwnedByCurrentUser && (
              <SectionReadAction
                editCallback={toggleEditting}
                deleteCallback={handleDelete}
                isDraft={section.isDraft}
                publishCallback={handlePublishSection}
                draftCallback={handleDraftSection}
                isLiked={isLiked}
                likeCallback={handleLike}
              />
            )}
        </Fragment>
      )}
    </Stack>
  );
};

export default Section;
