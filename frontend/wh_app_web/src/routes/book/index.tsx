import { Add, Edit } from "@mui/icons-material";
import { Grid, Pagination, Stack, Typography } from "@mui/material";
import { useStore } from "@nanostores/preact";
import _ from "lodash";
import { Fragment, FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import { useEffect, useState } from "preact/hooks";
import { ROUTE_NOTFOUND } from "../../components/app";
import { BookFormEdit } from "../../components/book-form-edit";
import { FloatingAction } from "../../components/floating-action";
import { ReadMore } from "../../components/read-more";
import { SectionCard } from "../../components/section-card";
import { SectionFormCreate } from "../../components/section-form-create";
import {
  AccountPersistent,
  getConfiguredHeader,
  parseJwt,
} from "../../lib/account";
import { DEFAULT_PAGE_SIZE, getTotalPages } from "../../lib/lib";
import { WHC } from "../../proto/global";
import {
  GetBookParam,
  GetBookReply,
  GetSectionParam,
  GetSectionReply,
  PaginationRequest,
} from "../../proto/writer_heaven_pb";

export interface BookProps {
  bookId: number;
}

async function getBook(
  bookId: number
): Promise<GetBookReply.Book.AsObject | undefined> {
  const request = new GetBookParam();

  const pagination = new PaginationRequest();
  pagination.setNumber(0);
  pagination.setSize(1);

  request.setPagination(pagination);
  request.setHeader(getConfiguredHeader());
  request.setId(bookId);

  const books = await WHC.getBook(request, null);
  const book = _.first(books.getBookListList());

  return await book?.toObject();
}

async function getSections(
  bookId: number,
  pageNumber: number
): Promise<GetSectionReply.AsObject | undefined> {
  const request = new GetSectionParam();

  const pagination = new PaginationRequest();
  pagination.setNumber(pageNumber);
  pagination.setSize(DEFAULT_PAGE_SIZE);

  request.setPagination(pagination);
  request.setHeader(getConfiguredHeader());
  request.setBookId(bookId);

  const sections = await WHC.getSection(request, null);

  return sections.toObject();
}

const Book: FunctionalComponent<BookProps> = (props) => {
  const account = useStore(AccountPersistent);
  const [book, setBook] = useState<GetBookReply.Book.AsObject>();
  const [sections, setSections] = useState<GetSectionReply.AsObject>();
  const [page, setPage] = useState<number>();
  const [isShowingSectionCreateForm, setIsEditting] = useState(false);
  const [isShowingBookEditForm, setIsShowingBookEditForm] = useState(false);
  const isOwnedByCurrentUser =
    parseJwt(account?.jwt ?? "")?.sub === book?.ownerId;

  useEffect(() => {
    getBook(props.bookId)
      .then((v) => {
        if (v) {
          setBook(v);
        } else {
          route(ROUTE_NOTFOUND);
        }
      })
      .then(() => {
        getSections(props.bookId, page ?? 0).then((v) => {
          setSections(v);
        });
      });
  }, [props.bookId, page]);

  const toggleIsEditting = () => {
    setIsEditting(!isShowingSectionCreateForm);
  };

  const toggleIsShowingBookEditForm = () => {
    setIsShowingBookEditForm(!isShowingBookEditForm);
  };

  const handlePageChange = (_event: unknown, value: number) => {
    setPage(value - 1);
  };

  const onUpdateSuccess = () => {
    getBook(props.bookId).then((v) => {
      if (v) {
        setBook(v);
        setIsShowingBookEditForm(false);
      } else {
        route(ROUTE_NOTFOUND);
      }
    });
  };

  return (
    <Stack>
      {isShowingBookEditForm ? (
        <BookFormEdit
          id={props.bookId}
          title={book?.title ?? ""}
          description={book?.description ?? ""}
          onUpdateSuccess={onUpdateSuccess}
          onCancelEditing={() => setIsShowingBookEditForm(false)}
        />
      ) : (
        <>
          <Typography
            align="center"
            variant="h2"
            style={{ marginTop: 0, marginBottom: "1rem" }}
          >
            <ReadMore limit={38} text={book?.title ?? "Untitled Book"} />
          </Typography>
          <Typography variant="h5" align="center">
            Made By {book?.ownerName ?? "An Unnamed Owner"}
          </Typography>
          <Typography variant="subtitle1" align="center">
            <ReadMore
              limit={160}
              text={book?.description ?? "Unfilled description"}
            />
          </Typography>
        </>
      )}

      <Stack gap={1} style={{ marginTop: "3em" }}>
        <Grid container>
          <Grid container item xs={6}>
            <Typography variant="h5">Sections</Typography>
          </Grid>
        </Grid>

        {isShowingSectionCreateForm ? (
          <SectionFormCreate
            bookId={props.bookId}
            onCancelEditing={() => setIsEditting(false)}
          />
        ) : (
          <Fragment>
            {sections
              ? _.map(sections.sectionListList, (s) => (
                  <SectionCard
                    bookId={props.bookId}
                    sectionId={s.id}
                    title={s.title}
                    createdAt={s.createdAt}
                    isDraft={s.isDraft}
                  />
                ))
              : _.map(_.range(0, 10), () => <SectionCard />)}
            <Grid container justifyContent="center" sx={{ marginTop: "2em" }}>
              <Pagination
                value={page}
                count={getTotalPages(sections?.pagination?.total ?? 0)}
                onChange={handlePageChange}
              />
            </Grid>
          </Fragment>
        )}
      </Stack>

      {isOwnedByCurrentUser && !isShowingSectionCreateForm && (
        <FloatingAction
          collapseButtonTooltipText="Action"
          childrenButtonList={[
            {
              tooltipText: "Create New Section",
              isVisible: true,
              icon: <Add />,
              onClick: toggleIsEditting,
            },
            {
              tooltipText: "Edit Book",
              isVisible: true,
              icon: <Edit />,
              onClick: toggleIsShowingBookEditForm,
            },
          ]}
        />
      )}
    </Stack>
  );
};

export default Book;
