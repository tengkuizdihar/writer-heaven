import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";
import { ROUTE_HOME } from "../../components/app";
import style from "./style.css";

const Notfound: FunctionalComponent = () => {
  return (
    <div class={style.notfound}>
      <h1>Error 404</h1>
      <p>That page doesn&apos;t exist.</p>
      <Link href={ROUTE_HOME}>
        <h4>Back to Home</h4>
      </Link>
    </div>
  );
};

export default Notfound;
