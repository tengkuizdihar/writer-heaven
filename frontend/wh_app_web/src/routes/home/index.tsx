import { Grid, Pagination } from "@mui/material";
import _ from "lodash";
import { FunctionalComponent, h, Fragment } from "preact";
import { useEffect, useState } from "preact/hooks";
import { BookCard } from "../../components/book-card";
import { getConfiguredHeader } from "../../lib/account";
import { DEFAULT_PAGE_SIZE, getTotalPages } from "../../lib/lib";
import { WHC } from "../../proto/global";
import {
  GetBookParam,
  GetBookReply,
  PaginationRequest,
} from "../../proto/writer_heaven_pb";

async function getBooks(pageNumber: number): Promise<GetBookReply.AsObject> {
  const request = new GetBookParam();

  const pagination = new PaginationRequest();
  pagination.setNumber(pageNumber);
  pagination.setSize(DEFAULT_PAGE_SIZE);

  request.setPagination(pagination);
  request.setHeader(getConfiguredHeader());

  return (await WHC.getBook(request, null)).toObject();
}

const Home: FunctionalComponent = () => {
  const [page, setPage] = useState<number>();
  const [books, setBooks] = useState<GetBookReply.AsObject>();

  useEffect(() => {
    getBooks(page ?? 0).then((v) => {
      setBooks(v);
    });
  }, [page]);

  const handlePageChange = (_event: unknown, value: number) => {
    setPage(value - 1);
  };

  return (
    <>
      <Grid container spacing={2}>
        {books
          ? books?.bookListList.map((b) => (
              <Grid key={b.id} item xs={6} md={4} lg={3}>
                <BookCard
                  book_id={b.id}
                  title={b.title}
                  description={b.description}
                  ownerName={b.ownerName}
                />
              </Grid>
            ))
          : _.map(_.range(0, 15), () => (
              <Grid item xs={6} md={4} lg={3}>
                <BookCard />
              </Grid>
            ))}
      </Grid>
      <Grid
        container
        justifyContent="center"
        sx={{ marginTop: "2em", height: "50px" }}
      >
        <Pagination
          value={page}
          count={getTotalPages(books?.pagination?.total ?? 0)}
          onChange={handlePageChange}
        />
      </Grid>
    </>
  );
};

export default Home;
