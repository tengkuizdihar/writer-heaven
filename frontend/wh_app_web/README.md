# wh_app_web

## Developer Gotchas

If you encounter any error about connection, you might want to turn off the adblock or if you're on Brave Browser, allow ad and tracker to run. No, it doesn't have anything to do with ads, but it does have something to do with how GRPC Web request resources.

## CLI Commands
*   `npm install`: Installs dependencies

*   `npm run dev`: Run a development, HMR server

*   `npm run serve`: Run a production-like server

*   `npm run build`: Production-ready build

*   `npm run lint`: Pass TypeScript files using ESLint

*   `npm run test`: Run Jest and Enzyme with
    [`enzyme-adapter-preact-pure`](https://github.com/preactjs/enzyme-adapter-preact-pure) for
    your tests


For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
