{ pkgs ? import (fetchTarball ("https://github.com/NixOS/nixpkgs/archive/f419dc5763c2b3c5580e396dea065b6d8b58ee27.tar.gz")) { } }:
{
  inherit pkgs;

  buildInputs = [
    pkgs.gnumake
  ];

  shellInputs = [
    pkgs.protobuf
    pkgs.protoc-gen-grpc-web
    pkgs.nodejs-16_x
  ];
}
