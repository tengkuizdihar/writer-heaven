let
  d = import ./nix/dependencies.nix { };
  pkgs = d.pkgs;
in
pkgs.clangStdenv.mkDerivation {
  name = "wh_app_web";
  buildInputs = d.shellInputs ++ d.buildInputs;
}
